var loadingScreenDiv = window.document.getElementById("loadingScreen");
var btnEnter = document.getElementById("btnEnter");
let eagleView = false; // 카메라 이글뷰 여부
let eagleCamera; // 이글뷰용 카메라 인스턴스
let cameraChanging = false; // 카메라 변경중
var camera;
var ground;
var vid1;
var vid2;
var deg;
var rotationValue = 0;
var eagleViewDirection = "down";
var stage;
var commercial;
var eventZone;
var campingZone;
var tripZone;
var descriptionTarget;
var buttonList = [];
// Loading still in progress.
// To wait for it to complete, add "DOMContentLoaded" or "load" listeners.
window.addEventListener("DOMContentLoaded", () => {
  // DOM ready! Images, frames, and other subresources are still downloading.
  document.getElementById("gauge").classList.add("inter");
});

window.addEventListener("load", () => {
  // Fully loaded!
  setTimeout(function () {
    document.getElementById("gauge").classList.add("done");
    document.getElementsByClassName("box-guage")[0].style.display = "none";
    document.getElementById("status").style.display = "none";
    document.getElementById("btnEnter").style.display = "block";
  }, 8000)

  // setTimeout(function () {
  //   loadingScreenDiv.classList.add("disapear");
  // }, 15000);

  var swiper = new Swiper(".mySwiper", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
});


btnEnter.addEventListener("click", () => {
  loadingScreenDiv.classList.add("disapear");
  openingAnimation();
  $("#fog").addClass("disapear");
})

var btnServiceInfo = document.getElementById("serviceInfo");
var serviceInfoCont = document.getElementById("serviceInfoContainer");
var canvas = document.getElementById("renderCanvas");

btnServiceInfo.addEventListener("click", function () {
  dimLayer.classList.add("show");
  serviceInfoCont.classList.add("show");
});

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function () {
  return new BABYLON.Engine(canvas, true, {
    preserveDrawingBuffer: true,
    stencil: true,
    disableWebGL2Support: false
  });
};

var createScene = function () {
  BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(function (loader) {
    if (loader.name === "gltf") {
      loader.useRangeRequests = true;
    }
  });

  // Scene and Camera
  var scene = new BABYLON.Scene(engine);
  camera = new BABYLON.ArcRotateCamera("main camera", 1.6, 1.13, 12.5, new BABYLON.Vector3(0, 0, 0), scene);
  scene.activeCamera = camera;
  scene.activeCamera.attachControl(canvas, true);
  // scene.debugLayer.show();
  // define degree
  deg = Math.PI ;
  // var deg1 = deg / 90;

  // camera
  camera.minZ = 1;
  camera.maxZ = 30000;
  camera.lowerBetaLimit = 0.9;
  camera.upperBetaLimit = 1.3;
  camera.angularSensibilityX = 5000;
  camera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
  // camera.upperRadiusLimit = window.innerWidth > 640 ? 410 : 410;
  camera.upperRadiusLimit = 1000;
  camera.panningDistanceLimit = 1;
  camera.wheelPrecision = 1;
  camera.pinchPrecision = 0.5;
  camera.pinchZoom = true;
  camera.setPosition(new BABYLON.Vector3(800, window.innerWidth > 640 ? 300 : 500, 0));
  camera.attachControl(canvas, true, false, 3);
  camera.setTarget(BABYLON.Vector3.Zero());
  camera.inputs.attached.mousewheel.wheelPrecisionY = 100;

  // Lights
  var light = new BABYLON.DirectionalLight("dirlight", new BABYLON.Vector3(-1, -1, 0), scene);
  light.position = new BABYLON.Vector3(0, 200, 0);
  light.intensity = 1.5;

  var light2 = new BABYLON.HemisphericLight("hemislight", new BABYLON.Vector3(0, 1, 0), scene);
  light2.intensity = 1.3;
  light2.specular = BABYLON.Color3.Black();

  var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

  // description model   
  var descSrc = [
    "stores_modal.png",
    "open_modal.png",
    "heal_modal.png",
    "igloo_modal.png",
    "hot_modal.png"
  ]

  // 모바일 버전 설명팝업
  var descSrcMobile = [
    "stores_pop.png",
    "openstage_pop.png",
    "healing_pop.png",
    "lgloo_pop.png",
    "hotair_pop.png"
  ]

  var mediaSrc = [
    "vid1.mp4",
    "vid2.mp4"
  ]

  var descText = [
    "It’s now that cherished time of year where all across our great blue sphere we celebrate with joy and cheer that special bond that brings us here."
  ]

  // function attact model name   
  var attachLabel = function attachLabel(modelName, modelText, modelPositionY, modelPositionX, modelPositionZ) {
     var button = BABYLON.GUI.Button.CreateSimpleButton("but", modelText);
  
    button.parent = modelName;
    button.color = "white";
    button.background = "green";
    button.width = modelText == "Hot Air Balloon Station" ? "200px" : "140px";
    button.height = "30px";
    button.thickness = 0;
    button.background = "white";
    button.alpha = 0;
    button.cornerRadius = 60;
    button.color = "#183993";
    button.fontSize = "16px";
    button.fontWeight = "bold";
    button.linkOffsetY = modelPositionY;
    button.linkOffsetX = modelPositionX;
    button.linkOffsetZ = modelPositionZ;
    advancedTexture.addControl(button); 
    button.linkWithMesh(modelName);
    button.pointerEnterAnimation = () => {
      button.color = "white";
      button.background = "#183993";
    }

    button.pointerOutAnimation = () => {
      button.color = "#183993";
      button.background = "white";
    }

    button.actionManager = new BABYLON.ActionManager(scene);
    var ease = new BABYLON.CubicEase();
    var speed = 60;
    var frameCount = 180;
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);

    // camera move
    var animateCameraTargetToPosition = function (cam, speed, frameCount, newPos) {
      var aable1 = BABYLON.Animation.CreateAndStartAnimation('at5', cam, 'target', speed, frameCount, cam.target, newPos, 0, ease);
      aable1.disposeOnEnd = true;
    }
    var animateCameraToPosition = function (cam, speed, frameCount, newPos) {
      var aable2 = BABYLON.Animation.CreateAndStartAnimation('at4', cam, 'position', speed, frameCount, cam.position, newPos, 0, ease);
      aable2.disposeOnEnd = true;
    }
    buttonList.push(button);

    button.onPointerUpObservable.add(function(){
      descriptionTarget = modelName;
      var clickedMesh = modelName;
      var xVal, zVal, yVal;

      switch (modelText){
        case "Open Stage":
          xVal = -100;
          zVal =  -20; 
          yVal = 0;
          break;
        case "Stores":
          xVal = -150;
          zVal = -150; 
          yVal = 0;
          break;
        case "Igloo":
          xVal = -50;
          zVal = -80; 
          yVal = 0;
          break;
        case "Healing Zone":
          xVal = 20;
          zVal = 60; 
          yVal = 0;
          break;
        case "Hot Air Balloon Station":
          xVal = -160;
          zVal =  -80; 
          yVal = 0;
          break;
      }

      if (!eagleView) {
        cameraTargetChangeWithAnimation(camera, new BABYLON.Vector3(clickedMesh._absolutePosition._x, yVal, clickedMesh._absolutePosition._z), new BABYLON.Vector3(clickedMesh._absolutePosition._x - xVal, 50, clickedMesh._absolutePosition._z + zVal));
      } else {
        cameraTargetChangeWithAnimation(eagleCamera, new BABYLON.Vector3(clickedMesh._absolutePosition._x, yVal, clickedMesh._absolutePosition._z), new BABYLON.Vector3(clickedMesh._absolutePosition._x - xVal, 50, clickedMesh._absolutePosition._z + zVal));
      }

      camera.detachControl();
      if (eagleCamera != null) {
        eagleCamera.detachControl();
      } 
      gameMoney.style.display = "none";
      $("#contentModal").show();

      if (window.innerWidth > 640) {
        // attach modal contents
        switch (modelText) {
          case "Stores":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[0])
            break;
          case "Open Stage":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[1])
            break;
          case "Healing Zone":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[2])
            break;
          case "Igloo":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[3])
            break;
          case "Hot Air Balloon Station":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[4])
            break;
        }
      } else {
        // attach modal contents
        switch (modelText) {
          case "Stores":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[0])
            break;
          case "Open Stage":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[1])
            break;
          case "Healing Zone":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[2])
            break;
          case "Igloo":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[3])
            break;
          case "Hot Air Balloon Station":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[4])
            break;
        }
      }
    });    
  }

  // rotate camera animation
  // function rotateCamera() {
  //   scene.registerBeforeRender(function () {
  //       camera.alpha += 0.0001;
  //   });
  // }
  // rotateCamera();

  // function hover mesh event
  function makeDescription(targetMesh, target, rectWidth, rectHeight, offsetY, descriptions) {

    // description for model
    let descWrap = new BABYLON.GUI.Rectangle();
    advancedTexture.addControl(descWrap);
    descWrap.width = rectWidth;
    descWrap.height = rectHeight;
    descWrap.thickness = 2;
    descWrap.background = "black";
    descWrap.scaleX = 0;
    descWrap.scaleY = 0;
    descWrap.alpha = 0.6;
    descWrap.cornerRadius = 30;
    descWrap.linkWithMesh(target);
    descWrap.linkOffsetX = 0;
    descWrap.linkOffsetY = offsetY;
    descWrap.dispose();

    let desc = new BABYLON.GUI.TextBlock();
    desc.text = descriptions;
    desc.color = "White";
    desc.fontSize = 14;
    desc.textWrapping = true;
    desc.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
    desc.paddingTop = "10px";
    desc.paddingBottom = "10px";
    desc.paddingLeft = "10px";
    desc.paddingRight = "10px";
    descWrap.addControl(desc);
    desc.dispose();

    targetMesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOverTrigger, function (ev) {
      document.body.style.cursor = 'pointer';
      descWrap.scaleX = 1;
      descWrap.scaleY = 1;
    }));
    targetMesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOutTrigger, function (ev) {
      document.body.style.cursor = ''
      descWrap.scaleX = 0;
      descWrap.scaleY = 0;
    }));
  }

  var contentModal = document.getElementById("contentModal");
  var descImg = document.querySelector("#contentModal img");
  var closeContentModal = document.getElementById("closeContentModal");
  var gameMoney = document.querySelector("#gameMoney");
  var dimLayer = document.querySelector("#dimLayer");
  // var penguinSwap = document.querySelector("#penguinSwap");
  var mediaModal = document.querySelector("#mediaModal");
  var videoCont = document.querySelector("#mediaWrap video");
  var videoClose = document.getElementById("mediaClose");

  // function click mesh event   
  var clickMeshEvent = function clickMeshEvent(target, xVal, zVal, yVal) {
    // console.log("clickMeshEvent");
    var ease = new BABYLON.CubicEase();
    var speed = 60;
    var frameCount = 180;
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);

    // camera move
    var animateCameraTargetToPosition = function (cam, speed, frameCount, newPos) {
      var aable1 = BABYLON.Animation.CreateAndStartAnimation('at5', cam, 'target', speed, frameCount, cam.target, newPos, 0, ease);
      aable1.disposeOnEnd = true;
    }
    var animateCameraToPosition = function (cam, speed, frameCount, newPos) {
      var aable2 = BABYLON.Animation.CreateAndStartAnimation('at4', cam, 'position', speed, frameCount, cam.position, newPos, 0, ease);
      aable2.disposeOnEnd = true;
    }

    var clickStation = function clickStation(targetMesh) {
      descriptionTarget = target;
      var clickedMesh = targetMesh;
      if (!eagleView) {
        cameraTargetChangeWithAnimation(camera, new BABYLON.Vector3(clickedMesh._absolutePosition._x, yVal, clickedMesh._absolutePosition._z), new BABYLON.Vector3(clickedMesh._absolutePosition._x - xVal, 50, clickedMesh._absolutePosition._z + zVal));
      } else {
        cameraTargetChangeWithAnimation(eagleCamera, new BABYLON.Vector3(clickedMesh._absolutePosition._x, yVal, clickedMesh._absolutePosition._z), new BABYLON.Vector3(clickedMesh._absolutePosition._x - xVal, 50, clickedMesh._absolutePosition._z + zVal));
      }
    }

    target.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function (ev) {
      // console.log(ev.source.name);
      clickStation(ev.source, xVal, zVal, yVal);
      camera.detachControl();
      if (eagleCamera != null) {
        eagleCamera.detachControl();
      } 
      gameMoney.style.display = "none";
      $("#contentModal").show();


      if (window.innerWidth > 640) {
        // attach modal contents
        switch (ev.source.name) {
          case "commercial_area":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[0])
            break;
          case "concert_zone":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[1])
            break;
          case "campong_zone":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[2])
            break;
          case "eventzone":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[3])
            break;
          case "trip_zone":
            descImg.setAttribute("src", "./assets/img/20220321/" + descSrc[4])
            break;
        }
      } else {
        // attach modal contents
        switch (ev.source.name) {
          case "commercial_area":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[0])
            break;
          case "concert_zone":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[1])
            break;
          case "campong_zone":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[2])
            break;
          case "eventzone":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[3])
            break;
          case "trip_zone":
            descImg.setAttribute("src", "./assets/img/20220318/" + descSrcMobile[4])
            break;
        }
      }
      
    }));
  }


  var clickCoinEvent = function clickCoinEvent(target) {
    target.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function (ev) {
      // console.log(ev.source.name);
      camera.detachControl();
      dimLayer.classList.add("show");
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        iglooSwap.style.display = "block";
      } else {
        $(".iglooSwapMobile").show();
      }
      // penguinSwap.classList.add("show");
    }));
  }

  var moveTo = function moveTo(cam, target, alpha, beta, radius, endCallBack = () => {}) {
    camera.lowerRadiusLimit = 0;
    var ease = new BABYLON.PowerEase();
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    BABYLON.Animation.CreateAndStartAnimation('targetMove', cam, 'target', 50, 80, cam.target, target, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('alphaMove', cam, 'alpha', 50, 80, cam.alpha, alpha, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('betaMove', cam, '', 50, 80, cam.beta, beta, 0, ease);
    BABYLON.Animation.CreateAndStartAnimation('cameraMove', cam, 'radius', 50, 80, cam.radius, radius, 0, ease, () => {
      camera.lowerRadiusLimit = radius;
      if (endCallBack) endCallBack();
    });
  }

  // Zone 별 모달 해제시 카메라 원상복구
  // closeContentModal.addEventListener("click", function () {
  //   descriptionTarget = null;
  //   $("#contentModal").removeClass("open");
  //   for (var i = 0; i < buttonList.length; i++){ 
  //     buttonList[i].alpha = 0;
  //   }

  //   toggleEagleView(true);
  //   // gameMoney.style.display = "block";
  //   // contentModal.classList.remove("open");
  //   // mediaModal.style.display = "none"
  //   // videoCont.setAttribute("src", "");
  //   // moveTo(camera, new BABYLON.Vector3(0, 0, 0), 0.055, 0.648, 410, () => {
  //   //   camera.lowerRadiusLimit = 150;
  //   //   camera.upperRadiusLimit = 410;
  //   //   camera.attachControl();
  //   // })
  //   // descriptionTarget = null;

  //   // // 이전에 이글뷰였던 경우
  //   // if (eagleView) {
  //   //   var ease = new BABYLON.CubicEase();
  //   //   var speed = 60;
  //   //   var frameCount = 180;
  //   //   ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
  //   //   BABYLON.Animation.CreateAndStartAnimation('at5', eagleCamera, 'target', speed, frameCount, eagleCamera.target, new BABYLON.Vector3(0, -30, 0), 0, ease);
  //   //   BABYLON.Animation.CreateAndStartAnimation('at4', eagleCamera, 'position', speed, frameCount, eagleCamera.position, new BABYLON.Vector3(-400, 400, 0), 0, ease);
  //   // }
  // })

  // close swap screen
  dimLayer.addEventListener("click", function () {
    this.classList.remove("show");
    // penguinSwap.classList.remove("show");
    serviceInfoCont.classList.remove("show");
    camera.attachControl();
  })

  // load mesh
  BABYLON.SceneLoader.ImportMesh("", "./assets/model/", "metakongworld_0315.glb", scene, function (newMeshes) {
    document.getElementById("gauge").classList.add("done");
    document.getElementsByClassName("box-guage")[0].style.display = "none";
    document.getElementById("status").style.display = "none";
    document.getElementById("btnEnter").style.display = "block";

    // for (var i = 0; i < newMeshes.length; i++) {
    //   console.log("---------"+i+"---------");
    //   console.log(newMeshes[i].name);
    // }

    ground = newMeshes[0];
    ground.scaling = new BABYLON.Vector3(-11, 11, 11);
    ground.rotation = new BABYLON.Vector3(0, deg, 0);
    ground.position = new BABYLON.Vector3(0, -11, 0);

    stage = scene.getMeshByName('concert_zone');
    stage.actionManager = new BABYLON.ActionManager(scene);
    stage.actionManager.isRecursive = true;
    stage.isPickable = true;

    var dataMap = {
      "Open Stage": {
        "xVal": -100,
        "zVal": -20,
        "yVal": 0,
      },
      "Stores": {
        "xVal": -150,
        "zVal": -150,
        "yVal": 0,
      },
      "Igloo": {
        "xVal": -50,
        "zVal": -80,
        "yVal": 0,
      },
      "Healing Zone": {
        "xVal": 20,
        "zVal": 60,
        "yVal": 0,
      },
      "Hot Air Balloon Station": {
        "xVal": -160,
        "zVal": -80,
        "yVal": 0,
      },
    }

    attachLabel(stage, "Open Stage", -30, 30, 200);
    clickMeshEvent(stage, dataMap["Open Stage"]["xVal"], dataMap["Open Stage"]["zVal"], dataMap["Open Stage"]["yVal"]);
    makeDescription(stage, stage, "330px", "60px", "-120px", descText[0]);

    commercial = scene.getMeshByName("commercial_area");
    commercial.actionManager = new BABYLON.ActionManager(scene);
    commercial.actionManager.isRecursive = true;
    commercial.isPickable = true;
    attachLabel(commercial, "Stores", -60, -60, 0);
    clickMeshEvent(commercial, dataMap["Stores"]["xVal"], dataMap["Stores"]["zVal"], dataMap["Stores"]["yVal"]);
    makeDescription(commercial, commercial, "330px", "60px", "-120px", descText[0]);

    eventZone = scene.getMeshByName('eventzone');
    eventZone.actionManager = new BABYLON.ActionManager(scene);
    eventZone.actionManager.isRecursive = true;
    eventZone.isPickable = true;
    attachLabel(eventZone, "Igloo", -30, 0, 0);
    clickMeshEvent(eventZone, dataMap["Igloo"]["xVal"], dataMap["Igloo"]["zVal"], dataMap["Igloo"]["yVal"]);
    makeDescription(eventZone, eventZone, "330px", "60px", "-120px", descText[0]);

    campingZone = scene.getMeshByName('campong_zone');
    campingZone.actionManager = new BABYLON.ActionManager(scene);
    campingZone.actionManager.isRecursive = true;
    campingZone.isPickable = true;
    attachLabel(campingZone, "Healing Zone", -30, 0, 0);
    clickMeshEvent(campingZone, dataMap["Healing Zone"]["xVal"], dataMap["Healing Zone"]["zVal"], dataMap["Healing Zone"]["yVal"]);
    makeDescription(campingZone, campingZone, "330px", "60px", "-120px", descText[0]);

    tripZone = scene.getMeshByName('trip_zone');
    tripZone.actionManager = new BABYLON.ActionManager(scene);
    tripZone.actionManager.isRecursive = true;
    tripZone.isPickable = true;
    attachLabel(tripZone, "Hot Air Balloon Station", -30, 0, 0);
    clickMeshEvent(tripZone, dataMap["Hot Air Balloon Station"]["xVal"], dataMap["Hot Air Balloon Station"]["zVal"], dataMap["Hot Air Balloon Station"]["yVal"]);
    makeDescription(tripZone, tripZone, "330px", "60px", "-120px", descText[0]);

    var coin = scene.getMeshByName('coin');
    coin.actionManager = new BABYLON.ActionManager(scene);
    coin.actionManager.isRecursive = true;
    coin.isPickable = true;
    var alpha1 = 0;
    clickCoinEvent(coin);

    var cloud1 = scene.getMeshByName('cloud1');

    var cloud2 = scene.getMeshByName('cloud2');

    var cloud3 = scene.getMeshByName('cloud3');

    var cloud4 = scene.getMeshByName('cloud4');
    
    var hotballoon = scene.getMeshByName('firepoongsun');
    hotballoon.position = new BABYLON.Vector3(-15.1, 5, 6.25);

    var land = scene.getMeshByName('land');
    var sea0 = scene.getMeshByName('sea_primitive0');
    var sea1 = scene.getMeshByName('sea_primitive1');

    // var direction = true;

    var direction = "right";

    // Animations
    scene.registerBeforeRender(function () {

      // 방향등 오른쪽 지시
      if (cloud1.position.x < -35 && direction == "left") {
        direction = "right";
      }

      // 방향등 왼쪽 지시
      if (cloud1.position.x > -20 && direction == "right") {
        direction = "left";
      }

      if (direction == "right") {
        cloud1.position.x += 0.015;
        cloud2.position.x += 0.015;
        cloud3.position.z += 0.015;
        cloud4.position.z += 0.015;
        hotballoon.position.y += 0.007;
      } else {
        cloud1.position.x -= 0.015;
        cloud2.position.x -= 0.015;
        cloud3.position.z -= 0.015;
        cloud4.position.z -= 0.015;
        hotballoon.position.y -= 0.007;
      } 

      coin.rotation = new BABYLON.Vector3(deg, alpha1, 0);
      alpha1 += 0.025;
      
      if (eagleView && descriptionTarget == null) {
        eagleCamera.alpha += 0.0007;
      } 
    });
  });

  var planeOpts = {
    height: 5.4762,
    width: 7.3967,
    sideOrientation: BABYLON.Mesh.DOUBLESIDE
  };

  vid1 = BABYLON.MeshBuilder.CreatePlane("myUniverse", planeOpts, scene);
  vid1.parent = ground;
  vid1.position = new BABYLON.Vector3(110, 4, -6.6);
  vid1.scaling = new BABYLON.Vector3(5.1, 4, 1);
  vid1.rotation = new BABYLON.Vector3(0, BABYLON.Tools.ToRadians(90), 0);
  var vid1Mat = new BABYLON.StandardMaterial("m", scene);
  var vid1VidTex = new BABYLON.VideoTexture("concert", "./assets/video/vid1.mp4", scene);
  vid1Mat.diffuseTexture = vid1VidTex;
  vid1Mat.roughness = 1;
  vid1Mat.emissiveColor = new BABYLON.Color3.White();
  vid1.material = vid1Mat;
  vid1VidTex.video.muted = true;
  vid1VidTex.video.play();
  scene.onPointerObservable.add(function (evt) {
    if (evt.pickInfo.pickedMesh === vid1) {
      mediaModal.style.display = "block";
      videoCont.setAttribute("src", "./assets/video/" + mediaSrc[0]);
    }
  }, BABYLON.PointerEventTypes.POINTERPICK);

  vid2 = BABYLON.MeshBuilder.CreatePlane("lalaLand", planeOpts, scene);
  vid2.parent = ground;
  vid2.position = new BABYLON.Vector3(145.5, 5, 118);
  vid2.scaling = new BABYLON.Vector3(5.3, 4.2, 1);
  vid2.rotation = new BABYLON.Vector3(0, BABYLON.Tools.ToRadians(36), 0);
  var vid2Mat = new BABYLON.StandardMaterial("m", scene);
  var vid2VidTex = new BABYLON.VideoTexture("movie", "./assets/video/vid2.mp4", scene);
  vid2Mat.diffuseTexture = vid2VidTex;
  vid2Mat.roughness = 1;
  vid2Mat.emissiveColor = new BABYLON.Color3.White();
  vid2.material = vid2Mat;
  vid2VidTex.video.muted = true;
  vid2VidTex.video.play();
  scene.onPointerObservable.add(function (evt) {
    if (evt.pickInfo.pickedMesh === vid2) {
      mediaModal.style.display = "block";
      videoCont.setAttribute("src", "./assets/video/" + mediaSrc[1]);
    }
  }, BABYLON.PointerEventTypes.POINTERPICK);

  videoClose.addEventListener("click", function () {
    mediaModal.style.display = "none";
    videoCont.setAttribute("src", "");
  })

  return scene;
}

window.initFunction = async function () {


  var asyncEngineCreation = async function () {
    try {
      return createDefaultEngine();
    } catch (e) {
      console.log("the available createEngine function failed. Creating the default engine instead");
      return createDefaultEngine();
    }
  }

  window.engine = await asyncEngineCreation();
  if (!engine) throw 'engine should not be null.';
  window.scene = createScene();
};

// render scene
initFunction().then(() => {
  sceneToRender = scene
  engine.runRenderLoop(function () {
    if (sceneToRender && sceneToRender.activeCamera) {
      sceneToRender.render();
    }
  });
});

// Resize
window.addEventListener("resize", function () {
  engine.resize();
});


$(document).ready(function () {
    // 모달 처리
    const openIglooSwap = document.getElementById("openIglooSwap");
    const iglooSwap = document.getElementById("iglooSwap");
    const btnCloseIgloo = document.getElementById("btnCloseIgloo");
    const btnQuest = document.getElementById("quest");
    const btnShop = document.getElementById("shop");
    const btnGovernance = document.getElementById("governance");
    const btnNews = document.getElementById("news");
    const btnRecruit = document.getElementById("recruit");
    const btnUpdateNick = document.getElementById("btnUpdateNick");
    const confirmUpdateNick = document.getElementById("confirmUpdateNick");
    let iglooTabs = document.querySelectorAll("#iglooSwap .tab-btn");
    let iglooContent = document.querySelectorAll("#iglooSwap .tab-content");

    for (i = 0; i < iglooTabs.length; i++) {
        iglooTabs[i].addEventListener("click", function (e) {
            var num = Array.from(iglooTabs).indexOf(e.currentTarget);
            // console.log(num);
            for (j = 0; j < iglooContent.length; j++) {
                iglooContent[j].style.display = "none";
            }
            iglooContent[num].style.display = "block";
        })
    }

    // 메타콩소개
    btnNews.addEventListener("click", function () {
        window.open("https://fascinated-carol-305.notion.site/f43263b387ee46e2b39e4653b037cf31");
    })

    // 채용
    btnRecruit.addEventListener("click", function () {
        window.open("https://fascinated-carol-305.notion.site/973e1546f43d46988d2aadb28b730900");
    })

    // 이글루스왑 메뉴 열기,닫기
    openIglooSwap.addEventListener("click", function () {
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        iglooSwap.style.display = "block";
      } else {
        $(".iglooSwapMobile").show();
      }
    })
    btnCloseIgloo.addEventListener("click", function () {
        $("#dimLayer").hide();
        iglooSwap.style.display = "none";
    })

    // 퀘스트
    btnQuest.addEventListener("click", function () {
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        $("#questBox").show();
      } else {
        $("#questBoxMobile").show();
      }
    })

    // 모바일 이글루 스왑 모달 조작
    $(".iglooSwapMobile .tab-btn").click(function() {
      $(".iglooSwapMobile .tab-btn img.on").hide();
      $(".iglooSwapMobile .tab-btn img.off").show();

      if ($(this).hasClass("btn1")) {    
        $(".iglooSwapMobile .btn1 img.off").hide();
        $(".iglooSwapMobile .btn1 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section1").show();
      } else if ($(this).hasClass("btn2")){
        $(".iglooSwapMobile .btn2 img.off").hide();
        $(".iglooSwapMobile .btn2 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section2").show();
      } else {
        $(".iglooSwapMobile .btn3 img.off").hide();
        $(".iglooSwapMobile .btn3 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section3").show();
      }

      return false;
    });

    // 모바일 이글루 스왑 모달 닫기
    $(".iglooSwapMobile #btnCloseIgloo").on("click", function () {
      $("#dimLayer").hide();
      $(".iglooSwapMobile").hide();
    })

    // 퀘스트 수락,창닫기
    $("#btnCloseQuest, #btnAgree").on("click", function () {
        $("#dimLayer").hide();
        $("#questBox").hide();
        $("#questBoxMobile").hide();
    })

    // 준비중 메뉴
    btnShop.addEventListener("click", function () {
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        $("#nftShop").show();
      } else {
        $("#nftShopMobile").show();
      }  
    })

    // 준비중 메뉴
    btnGovernance.addEventListener("click", function () {
        $("#dimLayer").show();
        if (window.innerWidth > 640) {
          $("#governanceModal").show();
        } else {
          $("#governanceMobileModal").show();
        }
        
    })

    // NFT 모달 닫기
    $("#btnCloseShop").on("click", function () {
        $("#dimLayer").hide();
        $("#nftShop").hide();
    })

    // 모바일 NFT 모달 닫기
    $("#nftShopMobile #btnCloseShop").on("click", function () {
      $("#dimLayer").hide();
      $("#nftShopMobile").hide();
    })

    $(".btn-modal1").on("click", function () {
      $(".btn-modal2-bg").show();
      $(".btn-modal2").show();
      return false;
    })

    $(".btn-modal2, .btn-modal2-bg").on("click", function () {
      $(".btn-modal2-bg").hide();
      $(".btn-modal2").hide();
      return false;
    })

    $("#btnCloseGovernance").on("click", function () {
        $("#dimLayer").hide();
        $("#governanceModal").hide();
    })

    $("#btnCloseKeyGuideModal").on("click", function () {
        $("#dimLayer").hide();
        $("#keyGuideModal").hide();
    })

    $("#mobileMenu").on("click", function () {
        $("#contentsMenu li").toggle();
        $("#news").toggle();
        $("#recruit").toggle();        
        // $("#btn-map").toggle();
    })

    $(".btn-quest-list").on("click", function () {
        $(".box-quest-detail").show();
    })

    $("#btnCloseQuestMobile").on("click", function () {
      $("#dimLayer").hide();
      $("#questBoxMobile").hide();
    });

    $("#btnCloseQuestDetailMobile").on("click", function () {
      $(".box-quest-detail").hide();
    });

    $("#btnCloseQuestMobile").on("click", function () {
      $("#questBoxMobile").hide();
    });

    $("#btnCloseGovernanceMobile").on("click", function () {
      $("#dimLayer").hide();
      $("#governanceMobileModal").hide();
    });

    // 카메라 이글뷰로 토글
    $('#btn-map').click(function() {
        toggleEagleView();
        return false;
    });
});

// 카메라 토글
function toggleEagleView(forceEagleView = false) {
  if (cameraChanging) return false;

  cameraChanging = true;
  $("#contentModal").hide();

  if (!eagleView || forceEagleView) {
      if (forceEagleView && eagleCamera != null) {
        var ease = new BABYLON.CubicEase();
        var speed = 60;
        var frameCount = 180;
        ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        BABYLON.Animation.CreateAndStartAnimation('at5', eagleCamera, 'target', speed, frameCount, eagleCamera.target, new BABYLON.Vector3(0, -30, 0), 0, ease);
        BABYLON.Animation.CreateAndStartAnimation('at4', eagleCamera, 'position', speed, frameCount, eagleCamera.position, window.innerWidth > 640 ?  new BABYLON.Vector3(200, 500, 0) : new BABYLON.Vector3(400, 700, 0), 0, ease);
        cameraChanging = false;
      } else {
        // 이글뷰로 변경
        // 현재 카메라 포지션 그대로 기본 카메라 정의
        eagleCamera = new BABYLON.ArcRotateCamera("UniversalCamera", 1.6, 1.13, 12.5, camera.position, scene);

        // Step1 : 아바타 중심 카메라에서 기본 카메라로 타겟은 아바타 그대로 유형만 변경
        scene.activeCamera = eagleCamera;
        scene.activeCamera.attachControl(canvas, false);
        eagleCamera.lowerBetaLimit = 0.9;
        eagleCamera.upperBetaLimit = 1.3;
        eagleCamera.target = ground.position;
        eagleCamera.inputs.clear();

        // 맵 중심으로 타겟 정의
        let newTarget = new BABYLON.Vector3(0, -30, 0);

        // 이글뷰로 포지션 변경
        let newPosition = window.innerWidth > 640 ?  new BABYLON.Vector3(200, 500, 0) : new BABYLON.Vector3(400, 700, 0);

        // Step2 : 아바타에서 맵 중심으로 카메라 타겟 변경, 이글뷰로 포지션 이동
        cameraTargetChangeWithAnimation(eagleCamera, newTarget, newPosition);
      }

      vid1.visibility = false; 
      vid2.visibility = false; 

      for (var i = 0; i < buttonList.length; i++){ 
        buttonList[i].alpha = 1;
      }

      eagleView = true;

  } else {
    if (descriptionTarget != null) {
      descriptionTarget = null;
      cameraChanging = false;
      toggleEagleView(true); 
    } else {
      // Step1 : 아바타 중심으로 포지션 및 타겟 변경
      cameraTargetResetWithAnimation(eagleCamera, ground.position, camera.position);
      eagleView = false;
      ground.rotation = new BABYLON.Vector3(0, deg, 0);
      vid1.visibility = true; 
      vid2.visibility = true; 
      for (var i = 0; i < buttonList.length; i++){ 
        buttonList[i].alpha = 0;
      }
    }
  }
}

// 카메라 애니메이션 함수
function cameraTargetChangeWithAnimation(cam, target, position) {
    cam.animations = [];
    
    var callBack = function(test) {
        cameraChanging = false;
    }

    // 카메라 타겟 변경 애니메이션
    var targetAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "target", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

     var keys = []; 

     keys.push({
         frame: 0,
         value: camera.target,
         outTangent: new BABYLON.Vector3(0, 0, 0)
     });

     keys.push({
         frame: 100,
         value: target,
         inTangent: new BABYLON.Vector3(.2, 0, 0),
     });

     targetAnimation.setKeys(keys);
     cam.animations.push(targetAnimation);
    
    // 카메라 포지션 변경 애니메이션
    var positionAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "position", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
        frame: 0,
        value: camera.position,
        outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    keys.push({
        frame: 100,
        value: position,
        inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    positionAnimation.setKeys(keys);
    cam.animations.push(positionAnimation);

    scene.beginAnimation(cam, 0, 100, false, 1, callBack);
}

// 카메라 리셋 애니메이션 함수
function cameraTargetResetWithAnimation(cam, target, position) {
    cam.animations = [];
    
    var callBack = function(test) {
        camera.target = ground.position;
        scene.activeCamera = camera;
        scene.activeCamera.attachControl(canvas, true);
        cameraChanging = false;
    }

    // 카메라 타겟 변경 애니메이션
    var targetAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "target", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

     var keys = []; 

     keys.push({
         frame: 0,
         value: eagleCamera.target,
         outTangent: new BABYLON.Vector3(0, 0, 0)
     });

     keys.push({
         frame: 100,
         inTangent: new BABYLON.Vector3(.2, 0, 0),
         value: target,
     });

     targetAnimation.setKeys(keys);
     cam.animations.push(targetAnimation);
    
    // 카메라 포지션 변경 애니메이션
    var positionAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "position", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
        frame: 0,
        value: eagleCamera.position,
        outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    keys.push({
        frame: 100,
        inTangent: new BABYLON.Vector3(.2, 0, 0),
        value: position,
    });

    positionAnimation.setKeys(keys);
    cam.animations.push(positionAnimation);

    scene.beginAnimation(cam, 0, 100, false, 1, callBack);
}

// 월드접속 애니메이션
function openingAnimation() {
    var frameRate = 30;
    var radiusAnimation = new BABYLON.Animation("radiusAnimation", "radius", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    var radiusKeyFrames = []
    radiusKeyFrames.push({
        value: 1000,
        frame: 0
    })

    radiusKeyFrames.push({
        value: 410,
        frame: 1 * frameRate,
    })
    radiusAnimation.setKeys(radiusKeyFrames)

    scene.beginDirectAnimation(camera, [radiusAnimation], 0, 1 * frameRate, false, 1,() => {
      camera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
      camera.upperRadiusLimit = window.innerWidth > 640 ? 410 : 410;
    });
}