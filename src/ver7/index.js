var camera; // 기본 카메라
var ground; // 월드맵 메시
let eagleView = false; // 카메라 이글뷰 여부
let eagleCamera; // 이글뷰용 카메라 인스턴스
let cameraChanging = false; // 기본 카메라 <=> 이글뷰 카메라 변경중 여부
var videoMesh;
var deg = Math.PI ;
var rotationValue = 0;
var eagleViewDirection = "down";
var clickedMesh; // 라벨을 눌러 설명을 보고 있는 메시
var buttonList = []; // 라벨 버튼 리스트
var ease = new BABYLON.CubicEase(); // 애니메이션 이징
var speed = 60; // 애니메이션 스피드
var frameCount = 180; // 애니메이션 프레임 수
var symbolAlpha = 0; // 중앙 심볼 회전 값
var logosAlpha = 0; // 로고 회전 값
var coinAlpha = 0; // 코인 회전 값
var advancedTexture; // 텍스처
var _brandZone; // 브랜드 존 메시
var _nftZone; // NFT 존 메시
var _rocketZone; // 로켓 존 메시
var _swapZone; // 스왑 존 메시
var _videoZone; // 비디오 존 메시
var _BRAND_ZONE_SELECT; // 브랜드 존 보더 메시
var _NFT_ZONE_SELECT; // NFT 존 보더 메시
var _ROKET_ZONE_SELECT; // 로켓 존 보더 메시
var _SWAP_ZONE_SELECT; // 스왑 존 보더 메시
var _VIDEO_ZONE_SELECT; // 비디오 존 보더 메시
ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);

// 파트너사 건물 
var shop_256;

// 파트너사 건물 
var shop_VKC;

// 파트너사 건물 
var shop_PB;

// 파트너사 건물 
var shop_tencent;

// 파트너사 건물 
var shop_Apple;

// 파트너사 포인터 
var pointer_256;

// 파트너사 포인터 
var pointer_paris;

// 파트너사 포인터 
var pointer_tencent;

// 파트너사 포인터 
var pointer_apple;

// 파트너사 포인터 
var pointer_vkc;


// 인트로 로딩 게이지바 채우기 애니메이션
$("#gauge").addClass("inter");


// 8초 후 자동 입장
window.addEventListener("load", () => {
  setTimeout(function () {
    $("#gauge").addClass("done");
    $(".box-guage").hide();
    $("#status").hide();
    $("#btnEnter").show();
  }, 8000)
});


// 입장하기 버튼 클릭시 인트로 화면 제거
$("#btnEnter").click(function() {
  $("#loadingScreen").addClass("disapear");
  openingAnimation();
  $("#fog").addClass("disapear");
});


// 3D 모델링할 캔버스
var canvas = document.getElementById("renderCanvas");
var createDefaultEngine = function () {
  return new BABYLON.Engine(canvas, true, {
    preserveDrawingBuffer: true,
    stencil: true,
    disableWebGL2Support: false
  });
};


var createScene = function () {
  BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(function (loader) {
    if (loader.name === "gltf") {
      loader.useRangeRequests = true;
    }
  });


  // Scene and Camera
  var scene = new BABYLON.Scene(engine);
  camera = new BABYLON.ArcRotateCamera("main camera", 1.6, 1.13, 12.5, new BABYLON.Vector3(0, 0, 0), scene);
  scene.activeCamera = camera;
  scene.activeCamera.attachControl(canvas, true);
  // scene.debugLayer.show();
  


  // 카메라 설정
  camera.minZ = 1;
  camera.maxZ = 30000;
  camera.lowerBetaLimit = 0.9;
  camera.upperBetaLimit = 1.3;
  camera.angularSensibilityX = 5000;
  camera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
  camera.upperRadiusLimit = 1000;
  camera.panningDistanceLimit = 1;
  camera.wheelPrecision = 1;
  camera.pinchPrecision = 0.5;
  camera.pinchZoom = true;
  camera.setPosition(new BABYLON.Vector3(800, window.innerWidth > 640 ? 300 : 500, 0));
  camera.attachControl(canvas, true, false, 3);
  camera.setTarget(BABYLON.Vector3.Zero());
  camera.inputs.attached.mousewheel.wheelPrecisionY = 100;


  // 조명1
  var light = new BABYLON.DirectionalLight("dirlight", new BABYLON.Vector3(-1, -1, 0), scene);
  light.position = new BABYLON.Vector3(0, 200, 0);
  light.intensity = 1.5;


  // 조명2
  var light2 = new BABYLON.HemisphericLight("hemislight", new BABYLON.Vector3(0, 1, 0), scene);
  light2.intensity = 1.3;
  light2.specular = BABYLON.Color3.Black();


  advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

  
  // 메시에 라벨 붙이기 함수
  function attachLabel(mesh, label) {
    var button = BABYLON.GUI.Button.CreateSimpleButton("but", label);
    button.parent = mesh;
    button.width = "150px";
    button.height = "30px";
    button.thickness = 0;
    button.background = "white";
    button.alpha = 0;
    button.cornerRadius = 60;
    button.color = "#183993";
    button.fontSize = "16px";
    button.fontWeight = "bold";
    button.linkOffsetX = 0;
    button.linkOffsetY = -100;
    button.linkOffsetZ = 0;
    advancedTexture.addControl(button); 
    button.linkWithMesh(mesh);

    // 호버 마우스 인시 컬러 변경
    button.pointerEnterAnimation = () => {
      button.color = "white";
      button.background = "#183993";
      document.body.style.cursor = 'pointer';
    }

    // 호버 마우스 아웃시 컬러 변경
    button.pointerOutAnimation = () => {
      button.color = "#183993";
      button.background = "white";
      document.body.style.cursor = ''
    }

    button.actionManager = new BABYLON.ActionManager(scene);
    buttonList.push(button);

    // ]-----] 메시 클릭시 이벤트 [-----[
    button.onPointerUpObservable.add(function(){
      zoomInSection(mesh);
      _BRAND_ZONE_SELECT.setEnabled(false);
      _NFT_ZONE_SELECT.setEnabled(false);
      _ROKET_ZONE_SELECT.setEnabled(false);
      _SWAP_ZONE_SELECT.setEnabled(false);
      _VIDEO_ZONE_SELECT.setEnabled(false);
    });    
  }

  // ]-----] 건물 포인터 로드 [-----[
  BABYLON.SceneLoader.ImportMesh("", "./assets/model/", "point.glb", scene, function (newMeshes) {
    // for (var i = 0; i < newMeshes.length; i++) {
    //   console.log("---------"+i+"---------");
    //   console.log(newMeshes[i].name);
    // }
    newMeshes[0].scaling = new BABYLON.Vector3(10, 10, 10);

    // 포인터 할당
    pointer_256 = scene.getMeshByName('256');
    pointer_paris = scene.getMeshByName('paris');
    pointer_tencent = scene.getMeshByName('tencent');
    pointer_apple = scene.getMeshByName('apple');
    pointer_vkc = scene.getMeshByName('vkc');
  });

  // ]-----] 디파이 간판 로드 [-----[
  BABYLON.SceneLoader.ImportMesh("", "./assets/model/", "defi.glb", scene, function (newMeshes) {
    newMeshes[0].scaling = new BABYLON.Vector3(10, 10, 10);
    newMeshes[0].position = new BABYLON.Vector3(93, -8, 78);
    newMeshes[0].rotation = new BABYLON.Vector3(0, deg/1.6, 0);
  });


  // ]-----] 월드맵 로드 시작 [-----[
  BABYLON.SceneLoader.ImportMesh("", "./assets/model/", "metakongworld_0414.glb", scene, function (newMeshes) {
    // for (var i = 0; i < newMeshes.length; i++) {
    //   console.log("---------"+i+"---------");
    //   console.log(newMeshes[i].name);
    // }

    // ]-----] 인트로 화면 로딩 시작 [-----[
    document.getElementById("gauge").classList.add("done");
    document.getElementsByClassName("box-guage")[0].style.display = "none";
    document.getElementById("status").style.display = "none";
    document.getElementById("btnEnter").style.display = "block";
    // ]-----] 인트로 화면 로딩 끝 [-----[


    // ]-----] 월드맵 포지션, 크기, 회전 조정 시작 [-----[
    ground = newMeshes[0];
    ground.scaling = new BABYLON.Vector3(-11, 11, 11);
    ground.rotation = new BABYLON.Vector3(0, deg, 0);
    ground.position = new BABYLON.Vector3(0, -11, 0);
    // ]-----] 월드맵 포지션, 크기, 회전 조정 끝 [-----[


    // ]-----] 섹션 벽 부분 숨기기 시작 [-----[
    _BRAND_ZONE_SELECT = scene.getMeshByName('BRAND_ZONE_SELECT');
    _NFT_ZONE_SELECT = scene.getMeshByName('NFT_ZONE_SELECT');
    _ROKET_ZONE_SELECT = scene.getMeshByName('ROKET_ZONE_SELECT');
    _SWAP_ZONE_SELECT = scene.getMeshByName('SWAP_ZONE_SELECT');
    _VIDEO_ZONE_SELECT = scene.getMeshByName('VIDEO_ZONE_SELECT');

    _BRAND_ZONE_SELECT.setEnabled(false);
    _NFT_ZONE_SELECT.setEnabled(false);
    _ROKET_ZONE_SELECT.setEnabled(false);
    _SWAP_ZONE_SELECT.setEnabled(false);
    _VIDEO_ZONE_SELECT.setEnabled(false);
    // ]-----] 섹션 벽 부분 숨기기 끝 [-----[

    // 기존 바다 숨기기
    var sea = scene.getMeshByName('sea');
    sea.setEnabled(false);

    // ]-----] 섹션 별 라벨 + 이벤트 추가 시작 [-----[
    _brandZone = scene.getMeshByName('BRAND_ZONE');
    _nftZone = scene.getMeshByName('NFT_ZONE');
    _rocketZone = scene.getMeshByName('ROKET_ZONE');
    _swapZone = scene.getMeshByName('SWAP_ZONE');
    _videoZone = scene.getMeshByName('VIDEO_ZONE');

    // 라벨 추가
    attachLabel(_brandZone, "Brand Zone");
    attachLabel(_nftZone, "NFT Zone");
    attachLabel(_rocketZone, "Rocket Zone");
    attachLabel(_swapZone, "De-Fi Zone");
    attachLabel(_videoZone, "Launchpad Zone");

    // 호버시 섹션 보더 추가
    hoverSection(_brandZone, _BRAND_ZONE_SELECT);
    hoverSection(_nftZone, _NFT_ZONE_SELECT);
    hoverSection(_rocketZone, _ROKET_ZONE_SELECT);
    hoverSection(_swapZone, _SWAP_ZONE_SELECT);
    hoverSection(_videoZone, _VIDEO_ZONE_SELECT);
    // ]-----] 섹션 별 이벤트 추가 끝 [-----[

    // 파트너사 건물 할당
    shop_256 = scene.getMeshByName('256_shop');
    shop_VKC = scene.getMeshByName('VKC_shop');
    shop_PB = scene.getMeshByName('PB_shop');
    shop_tencent = scene.getMeshByName('tencent_shop');
    shop_Apple = scene.getMeshByName('Apple_shop');

    // ]-----] 포인터 부모 메시 지정 및 위치 조절 시작 [-----[
    pointer_256.parent = shop_256;
    pointer_256.scaling = new BABYLON.Vector3(1, 1, 1);
    pointer_256.rotation = new BABYLON.Vector3(0, 0, deg/2);
    pointer_256.position.x = 50;
    pointer_256.position.z = -500;
    pointer_256.setEnabled(false);

    pointer_paris.parent = shop_PB;
    pointer_paris.scaling = new BABYLON.Vector3(1, 1, 1);
    pointer_paris.rotation = new BABYLON.Vector3(0, 0, deg/2);
    pointer_paris.position.x = 50;
    pointer_paris.position.z = -400;
    pointer_paris.setEnabled(false);

    pointer_tencent.parent = shop_tencent;
    pointer_tencent.scaling = new BABYLON.Vector3(1, 1, 1);
    pointer_tencent.rotation = new BABYLON.Vector3(0, 0, deg/2);
    pointer_tencent.position.x = 0;
    pointer_tencent.position.z = -600;
    pointer_tencent.setEnabled(false);
    
    pointer_apple.parent = shop_Apple;
    pointer_apple.scaling = new BABYLON.Vector3(1, 1, 1);
    pointer_apple.rotation = new BABYLON.Vector3(0, 0, deg/2);
    pointer_apple.position.x = 0;
    pointer_apple.position.z = -500;
    pointer_apple.setEnabled(false);

    pointer_vkc.parent = shop_VKC;
    pointer_vkc.scaling = new BABYLON.Vector3(1, 1, 1);
    pointer_vkc.rotation = new BABYLON.Vector3(0, 0, deg/2);
    pointer_vkc.position.x = 0;
    pointer_vkc.position.z = -400;
    pointer_vkc.setEnabled(false);
    // ]-----] 포인터 부모 메시 지정 및 위치 조절 끝 [-----[
    

    var logos = scene.getMeshByName('moving_partner_logo');
    var symbol = scene.getMeshByName('moving_center_logo');
    var coin = scene.getMeshByName('moving_swap_coin');
    
    // 메타콩 로고 심볼 메시 위치 조정
    symbol.position.z = .01295;

    // 프레임 별 렌더링 변경 값
    scene.registerBeforeRender(function () {
      // 협력사 로고 전광판 회전
      logos.rotation = new BABYLON.Vector3(deg/2, 0, logosAlpha);
      logosAlpha += 0.005;

      // 메타콩 로고 심볼 회전
      symbol.rotation = new BABYLON.Vector3(deg/2, 0, symbolAlpha);
      symbolAlpha += 0.004;

      // 이글루 스왑 코인 회전
      coin.rotation = new BABYLON.Vector3(deg/2, 0, coinAlpha);
      coinAlpha += 0.007;

      if (eagleView && clickedMesh == null) {
        eagleCamera.alpha += 0.0007;
      } 

      // 포인터 회전
      pointer_paris.rotation = new BABYLON.Vector3(0, 0, deg/2+coinAlpha);
      pointer_tencent.rotation = new BABYLON.Vector3(0, 0, deg/2+coinAlpha);
      pointer_apple.rotation = new BABYLON.Vector3(0, 0, deg/2+coinAlpha);
      pointer_vkc.rotation = new BABYLON.Vector3(0, 0, deg/2+coinAlpha);
      pointer_256.rotation = new BABYLON.Vector3(0, 0, deg/2+coinAlpha);
    });

    // 비디오 메시 생성
    videoMesh = BABYLON.MeshBuilder.CreatePlane("lalaLand", {
      height: 5.4762,
      width: 7.3967,
      sideOrientation: BABYLON.Mesh.DOUBLESIDE
    }, scene);

    videoMesh.parent = ground;
    videoMesh.position = new BABYLON.Vector3(11.9, 1.1, -12.7);
    videoMesh.scaling = new BABYLON.Vector3(.455, .455, .455);
    videoMesh.rotation = new BABYLON.Vector3(
      ground.rotation.x, 
      ground.rotation.y-BABYLON.Tools.ToRadians(40), 
      ground.rotation.z
    );

    var videoMeshMat = new BABYLON.StandardMaterial("m", scene);
    var videoMeshVidTex = new BABYLON.VideoTexture("movie", "./assets/video/vid1.mp4", scene);
    videoMeshMat.diffuseTexture = videoMeshVidTex;
    videoMesh.material = videoMeshMat;
    videoMeshVidTex.video.muted = true;
    videoMeshVidTex.video.play();

    scene.onPointerObservable.add(function (evt) {
      // 비디오 메시 클릭시 비디오 모달 보이기
      if (evt.pickInfo.pickedMesh === videoMesh) {
        $("#mediaModal").show();
        $("#mediaWrap video").attr("src", "./assets/video/vid1.mp4");
      } else if (evt.pickInfo.pickedMesh === _brandZone) {
        // 섹션 클릭 이벤트
        zoomInSection(_brandZone);
      } else if (evt.pickInfo.pickedMesh === _nftZone) {
        // 섹션 클릭 이벤트
        zoomInSection(_nftZone);
      } else if (evt.pickInfo.pickedMesh === _rocketZone) {
        // 섹션 클릭 이벤트
        zoomInSection(_rocketZone);
      } else if (evt.pickInfo.pickedMesh === _swapZone) {
        // 섹션 클릭 이벤트
        zoomInSection(_swapZone);
      } else if (evt.pickInfo.pickedMesh === _videoZone) {
        // 섹션 클릭 이벤트
        zoomInSection(_videoZone);
      } else if (evt.pickInfo.pickedMesh === shop_256 || 
                evt.pickInfo.pickedMesh === pointer_256) {
        // 빌딩 클릭 이벤트
        zoomInBuilding(shop_256);
      } else if (evt.pickInfo.pickedMesh === shop_VKC || 
                evt.pickInfo.pickedMesh === pointer_vkc) {
        // 빌딩 클릭 이벤트
        zoomInBuilding(shop_VKC);
      } else if (evt.pickInfo.pickedMesh === shop_PB || 
                evt.pickInfo.pickedMesh === pointer_paris) {
        // 빌딩 클릭 이벤트
        zoomInBuilding(shop_PB);
      } else if (evt.pickInfo.pickedMesh === shop_tencent || 
                evt.pickInfo.pickedMesh === pointer_tencent) {
        // 빌딩 클릭 이벤트
        zoomInBuilding(shop_tencent);
      } else if (evt.pickInfo.pickedMesh === shop_Apple || 
                evt.pickInfo.pickedMesh === pointer_apple) {
        // 빌딩 클릭 이벤트
        zoomInBuilding(shop_Apple);
      } 
      _BRAND_ZONE_SELECT.setEnabled(false);
      _NFT_ZONE_SELECT.setEnabled(false);
      _ROKET_ZONE_SELECT.setEnabled(false);
      _SWAP_ZONE_SELECT.setEnabled(false);
      _VIDEO_ZONE_SELECT.setEnabled(false);
    }, BABYLON.PointerEventTypes.POINTERPICK);

    // 비디오 모달 닫기
    $("#mediaClose").click(function () {
      $("#mediaModal").hide();
      $("#mediaWrap video").attr("src", "");
    })
  });

  // 주변 지형지물 로드
  BABYLON.SceneLoader.ImportMesh("", "./assets/model/", "metakongworld_0408.glb", scene, function (newMeshes) {
    // for (var i = 0; i < newMeshes.length; i++) {
    //   console.log("---------"+i+"---------");
    //   console.log(newMeshes[i].name);
    // }

    // 전체 메시 사이즈 및 위치 변경
    newMeshes[0].position.y = -10;
    newMeshes[0].scaling = new BABYLON.Vector3(10, 10, 10);

    // 애니메이션 방향 전환 여부
    var reverse = true;
    var reverse2 = true;

    var cloud1 = scene.getMeshByName('cloud1');
    var cloud2 = scene.getMeshByName('cloud2');
    var cloud3 = scene.getMeshByName('cloud3');
    var cloud4 = scene.getMeshByName('cloud4');
    var ship = scene.getMeshByName('ship');

    // 프레임 별 렌더링 변경 값
    scene.registerBeforeRender(function () {
          
      // 방향등 오른쪽 지시
      if (cloud1.position.x < -25 && !reverse) {
        reverse = true;
      }

      // 방향등 왼쪽 지시
      if (cloud1.position.x > -10 && reverse) {
        reverse = false;
      }

      if (reverse) {
        cloud1.position.x += 0.015;
        cloud2.position.x += 0.015;
        cloud3.position.z += 0.015;
        cloud4.position.z += 0.015;
        ship.position.x -= 0.015;  
      } else {
        cloud1.position.x -= 0.015;
        cloud2.position.x -= 0.015;
        cloud3.position.z -= 0.015;
        cloud4.position.z -= 0.015;
        ship.position.x += 0.015;  
      } 

      // 배 물에 떠있는 움직임 방향 변경
      if (ship.position.y < -1 && !reverse2) {
        reverse2 = true;
      }

      // 배 물에 떠있는 움직임 방향 변경
      if (ship.position.y > -0.5 && reverse2) {
        reverse2 = false;
      }

      if (reverse2) {
        ship.position.y += 0.0025;  
      } else {
        ship.position.y -= 0.0025;
      }
    });
  });

  return scene;
}

// 3D 엔진 초기화
window.initFunction = async function () {
  var asyncEngineCreation = async function () {
    try {
      return createDefaultEngine();
    } catch (e) {
      console.log("the available createEngine function failed. Creating the default engine instead");
      return createDefaultEngine();
    }
  }

  window.engine = await asyncEngineCreation();
  if (!engine) throw 'engine should not be null.';
  window.scene = createScene();
};

// 3D 렌더링 시작
initFunction().then(() => {
  engine.runRenderLoop(function () {
    if (scene && scene.activeCamera) {
      scene.render();
    }
  });
});

// 윈도우 리사이즈 될 때 렌더링 화면도 같이 리사이즈
window.addEventListener("resize", function () {
  engine.resize();
});


$(document).ready(function () {
    // 모달 처리
    const btnShop = document.getElementById("shop");
    const btnGovernance = document.getElementById("governance");
    let iglooTabs = document.querySelectorAll("#iglooSwap .tab-btn");
    let iglooContent = document.querySelectorAll("#iglooSwap .tab-content");

    // 이글루 모달 탭 클릭시 탭에 맞는 컨텐츠 보여주기 이벤트 추가
    for (i = 0; i < iglooTabs.length; i++) {
        iglooTabs[i].addEventListener("click", function (e) {
            var num = Array.from(iglooTabs).indexOf(e.currentTarget);
            for (j = 0; j < iglooContent.length; j++) {
                iglooContent[j].style.display = "none";
            }
            iglooContent[num].style.display = "block";
        })
    }

    // 메타콩 클릭시 소개 새 창 띄우기
    $("#news").click( function () {
        window.open("https://fascinated-carol-305.notion.site/ABOUT-METACONG-f43263b387ee46e2b39e4653b037cf31");
        // $("#dimLayer").fadeIn();
        // $("#preparingModal").fadeIn();
    })

    // 채용 클릭시 새 창 띄우기
    $("#recruit").click( function () {
        // window.open("https://fascinated-carol-305.notion.site/973e1546f43d46988d2aadb28b730900");
        window.open(
          './assets/presentation.pdf',
          '_blank' // <- This is what makes it open in a new window.
        );
        // $("#dimLayer").fadeIn();
        // $("#preparingModal").fadeIn();
    })

    // 이글루스왑 모달 열기
    $("#openIglooSwap").click( function () {
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        $("#iglooSwap").show();
      } else {
        $(".iglooSwapMobile").show();
      }
    })

    // 이글루스왑 모달 닫기
    $("#btnCloseIgloo").click( function () {
        $("#dimLayer").hide();
        $("#iglooSwap").hide();
    })

    // 퀘스트 모달 열기
    $("#quest").click(function () {
      $("#dimLayer").fadeIn();
      $(".box-intro-modal").fadeIn();
      $(".box-intro-modal").hide();
      $(".box-luniverse-modal").show();
    })

    // 모바일 이글루 스왑 모달 조작
    $(".iglooSwapMobile .tab-btn").click(function() {
      $(".iglooSwapMobile .tab-btn img.on").hide();
      $(".iglooSwapMobile .tab-btn img.off").show();

      if ($(this).hasClass("btn1")) {    
        $(".iglooSwapMobile .btn1 img.off").hide();
        $(".iglooSwapMobile .btn1 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section1").show();
      } else if ($(this).hasClass("btn2")){
        $(".iglooSwapMobile .btn2 img.off").hide();
        $(".iglooSwapMobile .btn2 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section2").show();
      } else {
        $(".iglooSwapMobile .btn3 img.off").hide();
        $(".iglooSwapMobile .btn3 img.on").show();
        $(".box-section").hide();
        $(".box-section.box-section3").show();
      }

      return false;
    });

    // 모바일 이글루 스왑 모달 닫기
    $(".iglooSwapMobile #btnCloseIgloo").click( function () {
      $("#dimLayer").hide();
      $(".iglooSwapMobile").hide();
    })

    // 퀘스트 모달 닫기
    $("#btnCloseQuest, #btnAgree").click( function () {
        $("#dimLayer").hide();
        $("#questBox").hide();
        $("#questBoxMobile").hide();
    })

    // NFT 모달 열기
    $("#shop").click(function () {
      $("#dimLayer").show();
      if (window.innerWidth > 640) {
        $("#nftShop").show();
      } else {
        $("#nftShopMobile").show();
      }  
    })

    // 거버넌스 모달 열기
    $("#governance").click( function () {
        $("#dimLayer").show();
        if (window.innerWidth > 640) {
          $("#governanceModal").show();
        } else {
          $("#governanceMobileModal").show();
        }
    })

    // NFT 모달 닫기
    $("#btnCloseShop").on("click", function () {
        $("#dimLayer").hide();
        $("#nftShop").hide();
    })

    // 모바일 NFT 모달 닫기
    $("#nftShopMobile #btnCloseShop").on("click", function () {
      $("#dimLayer").hide();
      $("#nftShopMobile").hide();
    })

    // NFT 모바일 모달 열기
    $(".btn-modal1").on("click", function () {
      $(".btn-modal2-bg").show();
      $(".btn-modal2").show();
      return false;
    })

    // NFT 모바일 모달 닫기
    $(".btn-modal2, .btn-modal2-bg").on("click", function () {
      $(".btn-modal2-bg").hide();
      $(".btn-modal2").hide();
      return false;
    })

    // 거버넌스 모달 닫기
    $("#btnCloseGovernance").on("click", function () {
        $("#dimLayer").hide();
        $("#governanceModal").hide();
    })

    // 모바일 햄버거 메뉴 클릭시 하위 메뉴 토글 이벤트
    $("#mobileMenu").on("click", function () {
        $("#contentsMenu li").toggle();
        $("#news").toggle();
        $("#contactus").toggle();
        $("#recruit").toggle();        
    })

    // 퀘스트 모달 클릭시 퀘스트 상세 열기 이벤트
    $(".btn-quest-list").on("click", function () {
        $(".box-quest-detail").show();
    })

    // 퀘스트 모바일 모달 닫기
    $("#btnCloseQuestMobile").on("click", function () {
      $("#dimLayer").hide();
      $("#questBoxMobile").hide();
    });

    // 퀘스트 상세 모바일 모달 닫기
    $("#btnCloseQuestDetailMobile").on("click", function () {
      $(".box-quest-detail").hide();
    });

    // 퀘스트 모바일 모달 닫기
    $("#btnCloseQuestMobile").on("click", function () {
      $("#questBoxMobile").hide();
    });

    // 거버넌스 모바일 모달 닫기
    $("#btnCloseGovernanceMobile").on("click", function () {
      $("#dimLayer").hide();
      $("#governanceMobileModal").hide();
    });

    // 카메라 이글뷰로 토글
    $('#btn-map').click(function() {
        toggleEagleView();
        return false;
    });
});

// 카메라 토글
function toggleEagleView(forceEagleView = false) {
  if (cameraChanging) return false;
  closeAllButton();

  // 카메라 변경 중 처리
  cameraChanging = true;

  if (!eagleView || forceEagleView) {
      if (forceEagleView && eagleCamera != null) {
        // 애니메이션 이징 설정 시작
        var ease = new BABYLON.CubicEase();
        var speed = 60;
        var frameCount = 180;
        ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        // 애니메이션 이징 설정 끝

        // 카메라 애니메이션 실행
        BABYLON.Animation.CreateAndStartAnimation('at5', eagleCamera, 'target', speed, frameCount, eagleCamera.target, new BABYLON.Vector3(0, -30, 0), 0, ease);
        BABYLON.Animation.CreateAndStartAnimation('at4', eagleCamera, 'position', speed, frameCount, eagleCamera.position, window.innerWidth > 640 ?  new BABYLON.Vector3(200, 500, 0) : new BABYLON.Vector3(400, 700, 0), 0, ease);

        // 카메라 변경중 아님 처리
        cameraChanging = false;
      } else {
        // 이글뷰로 변경
        // 현재 카메라 포지션 그대로 기본 카메라 정의
        eagleCamera = new BABYLON.ArcRotateCamera("UniversalCamera", 1.6, 1.13, 12.5, camera.position, scene);

        // 건물 포인터 미노출 
        pointer_256.setEnabled(false);
        pointer_vkc.setEnabled(false);
        pointer_paris.setEnabled(false);
        pointer_tencent.setEnabled(false);
        pointer_apple.setEnabled(false);        

        // Step1 : 아바타 중심 카메라에서 기본 카메라로 타겟은 아바타 그대로 유형만 변경
        scene.activeCamera = eagleCamera;
        eagleCamera.lowerBetaLimit = 0.9;
        eagleCamera.upperBetaLimit = 1.3;
        eagleCamera.target = ground.position;
        scene.activeCamera.detachControl();
        
        // 맵 중심으로 타겟 정의
        let newTarget = new BABYLON.Vector3(0, -30, 0);

        // 이글뷰로 포지션 변경
        let newPosition = window.innerWidth > 640 ?  new BABYLON.Vector3(200, 500, 0) : new BABYLON.Vector3(400, 700, 0);

        // Step2 : 아바타에서 맵 중심으로 카메라 타겟 변경, 이글뷰로 포지션 이동
        cameraTargetChangeWithAnimation(eagleCamera, newTarget, newPosition);

        // 클릭된 건물 또는 섹션 없음 처리
        clickedMesh = null;
      }

      // 섹션 별 라벨 노출 처리
      for (var i = 0; i < buttonList.length; i++){ 
        buttonList[i].alpha = 1;
      }

      // 현재 이글뷰 카메라
      eagleView = true;
  } else {
    if (clickedMesh != null) {
      // 건물 포인터 미노출 
      pointer_256.setEnabled(false);
      pointer_vkc.setEnabled(false);
      pointer_paris.setEnabled(false);
      pointer_tencent.setEnabled(false);
      pointer_apple.setEnabled(false);
      
      // 선택된 건물 또는 섹션 없음 처리
      clickedMesh = null;

      // 카메라 변경 완료
      cameraChanging = false;

      // 카메라 컨트롤 불가로 처리
      scene.activeCamera.detachControl();

      // 카메라 변경
      toggleEagleView(true); 
    } else {
      // 카메라 줌인/아웃 최소/최대치 세팅
      camera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
      camera.upperRadiusLimit = window.innerWidth > 640 ? 410 : 410;
      eagleCamera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
      eagleCamera.upperRadiusLimit = window.innerWidth > 640 ? 410 : 410;

      // 기본 카메라로 리셋
      cameraTargetResetWithAnimation(eagleCamera, ground.position, camera.position);

      // 현재 이글뷰 카메라 아님으로 처리
      eagleView = false;

      // 프레임별 땅 회전시키기
      ground.rotation = new BABYLON.Vector3(0, deg, 0);

      // 동영상 전광판 보이기
      videoMesh.visibility = true; 

      // 라벨 버튼 숨기기
      for (var i = 0; i < buttonList.length; i++){ 
        buttonList[i].alpha = 0;
      }
    }
  }
}

// 카메라 애니메이션 함수
function cameraTargetChangeWithAnimation(cam, target, position) {
    // 카메라 애니메이션 초기화
    cam.animations = [];

    try {
      if (camera != null) camera.lowerRadiusLimit = 100;
    } catch(e) {
      console.log(e);
    }
    
    try {
      if (eagleCamera != null) eagleCamera.lowerRadiusLimit = 100;
    } catch(e) {
      console.log(e);
    }
    
    var callBack = function(test) {
        cameraChanging = false;
    }

    // ]----] 카메라 포지션 변경 애니메이션 시작 [-----[
    var positionAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "position", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
        frame: 0,
        value: cam.position,
        inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    keys.push({
        frame: 100,
        value: position,
        outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    positionAnimation.setKeys(keys);
    cam.animations.push(positionAnimation);
    // ]----] 카메라 포지션 변경 애니메이션 끝 [-----[

    
    // ]----] 카메라 타겟 변경 애니메이션 시작 [-----[
    var targetAnimation = new BABYLON.Animation(
      "myAnimationcamera", 
      "target", 
      80, 
      BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
      BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
      frame: 0,
      value: cam.target,
      inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    keys.push({
      frame: 100,
      value: target,
      outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    targetAnimation.setKeys(keys);
    cam.animations.push(targetAnimation);

    var frameRate = 30;
    var radiusAnimation = new BABYLON.Animation("radiusAnimation", "radius", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    keys = []
    keys.push({
        value: 1000,
        frame: 0
    })

    keys.push({
        value: 100,
        frame: 1 * frameRate,
    })
    radiusAnimation.setKeys(keys);
    // ]----] 카메라 타겟 변경 애니메이션 끝 [-----[

    scene.beginAnimation(cam, 0, 100, false, 1, callBack);
}

// 카메라 애니메이션 함수
function cameraBuildingTargetChangeWithAnimation(cam, target, position) {
    // 카메라 애니메이션 초기화
    cam.animations = [];
    
    var callBack = function(test) {
      cameraChanging = false;
    }

    try {
      if (camera != null) camera.lowerRadiusLimit = 100;
    } catch(e) {
      console.log(e);
    }
    
    try {
      if (eagleCamera != null) eagleCamera.lowerRadiusLimit = 100;
    } catch(e) {
      console.log(e);
    }

    // ]----] 카메라 포지션 변경 애니메이션 시작 [-----[
    var positionAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "position", 
        80, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
        frame: 0,
        value: cam.position,
        inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    keys.push({
        frame: 100,
        value: position,
        outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    positionAnimation.setKeys(keys);
    cam.animations.push(positionAnimation);
    // ]----] 카메라 포지션 변경 애니메이션 끝 [-----[

    
    // ]----] 카메라 타겟 변경 애니메이션 시작 [-----[
    var targetAnimation = new BABYLON.Animation(
      "myAnimationcamera", 
      "target", 
      80, 
      BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
      BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
      frame: 0,
      value: cam.target,
      inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    keys.push({
      frame: 100,
      value: target,
      outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    targetAnimation.setKeys(keys);
    cam.animations.push(targetAnimation);

    var frameRate = 30;
    var radiusAnimation = new BABYLON.Animation("radiusAnimation", "radius", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    keys = []
    keys.push({
        value: 1000,
        frame: 0
    })

    keys.push({
        value: 100,
        frame: 1 * frameRate,
    })
    radiusAnimation.setKeys(keys);
    cam.animations.push(targetAnimation);
    // ]----] 카메라 타겟 변경 애니메이션 끝 [-----[

    scene.beginAnimation(cam, 0, 100, false, 1, callBack);
}

// 카메라 리셋 애니메이션 함수
function cameraTargetResetWithAnimation(cam, target, position) {
    cam.animations = [];
    
    var callBack = function(test) {
        camera.target = ground.position;
        scene.activeCamera = camera;
        scene.activeCamera.attachControl(canvas, true);
        cameraChanging = false;
    }

    // 카메라 타겟 변경 애니메이션
    var targetAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "target", 
        speed, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

     var keys = []; 

     keys.push({
         frame: 0,
         value: eagleCamera.target,
         inTangent: new BABYLON.Vector3(.2, 0, 0),
     });

     keys.push({
         frame: 100,
         value: target,
         outTangent: new BABYLON.Vector3(0, 0, 0)
     });

     targetAnimation.setKeys(keys);
     cam.animations.push(targetAnimation);
    
    // 카메라 포지션 변경 애니메이션
    var positionAnimation = new BABYLON.Animation(
        "myAnimationcamera", 
        "position", 
        speed, 
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
    );

    keys = []; 

    keys.push({
        frame: 0,
        value: eagleCamera.position,
        inTangent: new BABYLON.Vector3(.2, 0, 0),
    });

    keys.push({
        frame: 100,
        value: position,
        outTangent: new BABYLON.Vector3(0, 0, 0)
    });

    positionAnimation.setKeys(keys);
    cam.animations.push(positionAnimation);

    scene.beginAnimation(cam, 0, 100, false, 1, callBack);
}

// 월드접속 애니메이션
function openingAnimation() {
    var frameRate = 30;
    var radiusAnimation = new BABYLON.Animation("radiusAnimation", "radius", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    var radiusKeyFrames = []
    radiusKeyFrames.push({
        value: 1000,
        frame: 0
    })

    radiusKeyFrames.push({
        value: 410,
        frame: 1 * frameRate,
    })
    radiusAnimation.setKeys(radiusKeyFrames)

    scene.beginDirectAnimation(camera, [radiusAnimation], 0, 1 * frameRate, false, 1,() => {
      camera.lowerRadiusLimit = window.innerWidth > 640 ? 300 : 410;
      camera.upperRadiusLimit = window.innerWidth > 640 ? 410 : 410;
      $("#dimLayer").fadeIn();
      $(".box-intro-modal").fadeIn();
    });
}

// 섹션 호버 시 섹션 보더 추가
function hoverSection(hoverMesh, hoverBorder) {
  let actionManager = new BABYLON.ActionManager(scene);
  hoverMesh.actionManager = actionManager;

  actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOverTrigger, function(ev){
    if (clickedMesh == null) {
      hoverBorder.setEnabled(true);  
    }
  }));

  actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOutTrigger, function(ev){
    hoverBorder.setEnabled(false);
  }));
}

// 건물 호버 시 포인터 추가
function hoverBuilding(building, pointer) {
  let actionManager = new BABYLON.ActionManager(scene);
  building.actionManager = actionManager;

  actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOverTrigger, function(ev){
    pointer.setEnabled(true);  
  }));

  actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOutTrigger, function(ev){
    pointer.setEnabled(false);
  }));
}

// 특정 섹션 줌인 함수
function zoomInSection(mesh) {
  clickedMesh = mesh;

  // 라벨 숨기기
  for (var i = 0; i < buttonList.length; i++){ 
    buttonList[i].alpha = 0;
  }

  // 버튼 없애기
  closeAllButton();

  // 카메라 포지션 보정값
  var _x = 0;
  var _y = 0;
  var _z = 0;

  switch (mesh.name){
    case "BRAND_ZONE":
      _x = 20;
      _y = 300; 
      _z = 100;
      pointer_256.setEnabled(true);
      pointer_vkc.setEnabled(true);
      pointer_paris.setEnabled(true);
      pointer_tencent.setEnabled(true);
      pointer_apple.setEnabled(true);
      attachButton(mesh);
      break;
    case "NFT_ZONE":
      _x = -90;
      _y = 60; 
      _z = -30;
      attachButton(mesh);
      break;
    case "ROKET_ZONE":
      _x = 30;
      _y = 200; 
      _z = 0;
      attachButton(mesh);
      break;
    case "SWAP_ZONE":
      _x = 0;
      _y = 150; 
      _z = -30;
      attachButton(mesh);
      break;
    case "VIDEO_ZONE":
      _x = -10;
      _y = 120; 
      _z = -10;
      attachButton(mesh);
      break;
  }

  // 클릭 시 카메라 애니메이션 함수 추가
  cameraTargetChangeWithAnimation(
    // 줌 조정될 카메라
    eagleView ? eagleCamera : camera, 

    // 타겟의 포지션
    new BABYLON.Vector3( 
      mesh._absolutePosition._x, 
      mesh._absolutePosition._y, 
      mesh._absolutePosition._z), 

    // 카메라의 포지션
    new BABYLON.Vector3( 
      mesh._absolutePosition._x + _x, 
      mesh._absolutePosition._y + _y, 
      mesh._absolutePosition._z + _z)
    );
  
  scene.activeCamera.attachControl(canvas, true);
}

// 건물 별 버튼 추가
function attachButton(building) {
  var section = true;
  switch(building.name) {
    case "256_shop":
      // addButtonEventEachMesh(building, "20220425/hidden_button.png");
      section = false;
      break;
    case "VKC_shop":
      addButtonEventEachMesh(building, "20220413/gui_btn_promotion.png");
      section = false;
      break;
    case "PB_shop":
      addButtonEventEachMesh(building, "20220413/gui_btn_promotion.png");
      section = false;
      break;
    case "tencent_shop":
      addButtonEventEachMesh(building, "20220413/gui_btn_promotion.png");
      section = false;
      break;
    case "Apple_shop":
      addButtonEventEachMesh(building, "20220413/gui_btn_promotion.png");
      section = false;
      break;
    case "ROKET_ZONE":
      addButtonEventEachMesh(building, "20220413/gui_btn_world.png");
      addButtonEventEachMesh(building, "20220425/btn_housing.png");
      break;
    case "SWAP_ZONE":
      addButtonEventEachMesh(building, "20220425/aggregator.png");
      addButtonEventEachMesh(building, "20220425/de-fi.png");
      addButtonEventEachMesh(building, "20220425/governance_treasury.png");
      break;
    case "NFT_ZONE":
      addButtonEventEachMesh(building, "20220425/btn_nft.png");
      break;
    case "VIDEO_ZONE":
      addButtonEventEachMesh(building, "20220425/btn_about_launchpad.png");
      addButtonEventEachMesh(building, "20220425/btn_dao.png");
      break;
  }
  
  // 닫기 버튼 추가
  var closeButton = BABYLON.GUI.Button.CreateImageOnlyButton(
    "but",
    "assets/img/20220413/gui_btn_close.png"
  );

  closeButton.parent = building;
  closeButton.width = window.innerWidth > 640 ? "80px" : "60px";
  closeButton.height = window.innerWidth > 640 ? "80px" : "60px";
  closeButton.thickness = 0;
  closeButton.linkOffsetX = 50;
  closeButton.linkOffsetY = section ? 100 : 250;
  closeButton.linkOffsetZ = 0;
  advancedTexture.addControl(closeButton); 
  closeButton.linkWithMesh(building);

  closeButton.actionManager = new BABYLON.ActionManager(scene);

  // ]-----] 메시 클릭시 이벤트 [-----[
  closeButton.onPointerUpObservable.add(function(){
    toggleEagleView(false);
    if(building.promotionButton != null) building.promotionButton.dispose();
    if(building.promotionButton2 != null) building.promotionButton2.dispose();
    if(building.promotionButton3 != null) building.promotionButton3.dispose();
    if(building.promotionButton4 != null) building.promotionButton4.dispose();
    if(building.promotionButton5 != null) building.promotionButton5.dispose();
    if(building.closeButton != null) building.closeButton.dispose();
  });    

  building.closeButton = closeButton;
}

// 빌딩 클릭시 줌인 및 버튼 노출
function zoomInBuilding(building) {
  // 버튼 없애기
  closeAllButton();

  clickedMesh = building;

  // 건물 포인터 노출 
  pointer_256.setEnabled(true);
  pointer_vkc.setEnabled(true);
  pointer_paris.setEnabled(true);
  pointer_tencent.setEnabled(true);
  pointer_apple.setEnabled(true);        

  // 카메라 포지션 보정값
  var _x = 0;
  var _y = 0;
  var _z = 0;

  switch (building.name){
    case "256_shop":
      _x = -100;
      _y = 0; 
      _z = 30;
      // 프로모션 버튼 추가
      attachButton(shop_256);
      break;
    case "VKC_shop":
      _x = 100;
      _y = 0; 
      _z = 100;
      // 프로모션 버튼 추가
      attachButton(shop_VKC);
      break;
    case "PB_shop":
      _x = -100;
      _y = 0; 
      _z = 100;
      // 프로모션 버튼 추가
      attachButton(shop_PB);
      break;
    case "tencent_shop":
      _x = 0;
      _y = 0; 
      _z = 100;
      // 프로모션 버튼 추가
      attachButton(shop_tencent);
      break;
    case "Apple_shop":
      _x = 50;
      _y = 0; 
      _z = 100;
      // 프로모션 버튼 추가
      attachButton(shop_Apple);
      break;
  }

  // 클릭 시 카메라 애니메이션 함수 추가
  cameraBuildingTargetChangeWithAnimation(
    // 줌 조정될 카메라
    eagleView ? eagleCamera : camera, 

    // 타겟의 포지션
    new BABYLON.Vector3( 
      building._absolutePosition._x, 
      building._absolutePosition._y, 
      building._absolutePosition._z), 

    // 카메라의 포지션
    new BABYLON.Vector3( 
      building._absolutePosition._x + _x, 
      building._absolutePosition._y + _y, 
      building._absolutePosition._z + _z)
    );
}


// 모든 건물/영역 버튼 없애기
function closeAllButton() {
  try {
    if (shop_256.promotionButton != null) shop_256.promotionButton.dispose();
    if (shop_256.closeButton != null) shop_256.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (shop_VKC.promotionButton != null) shop_VKC.promotionButton.dispose();
    if (shop_VKC.closeButton != null) shop_VKC.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (shop_PB.promotionButton != null) shop_PB.promotionButton.dispose();
    if (shop_PB.closeButton != null) shop_PB.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (shop_tencent.promotionButton != null) shop_tencent.promotionButton.dispose();
    if (shop_tencent.closeButton != null) shop_tencent.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (shop_Apple.promotionButton != null) shop_Apple.promotionButton.dispose();
    if (shop_Apple.closeButton != null) shop_Apple.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (_brandZone.promotionButton != null) _brandZone.promotionButton.dispose();
    if (_brandZone.promotionButton2 != null) _brandZone.promotionButton2.dispose();
    if (_brandZone.promotionButton3 != null) _brandZone.promotionButton3.dispose();
    if (_brandZone.promotionButton4 != null) _brandZone.promotionButton4.dispose();
    if (_brandZone.promotionButton5 != null) _brandZone.promotionButton5.dispose();
    if (_brandZone.closeButton != null) _brandZone.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (_nftZone.promotionButton != null) _nftZone.promotionButton.dispose();
    if (_nftZone.promotionButton2 != null) _nftZone.promotionButton2.dispose();
    if (_nftZone.promotionButton3 != null) _nftZone.promotionButton3.dispose();
    if (_nftZone.promotionButton4 != null) _nftZone.promotionButton4.dispose();
    if (_nftZone.promotionButton5 != null) _nftZone.promotionButton5.dispose();
    if (_nftZone.closeButton != null) _nftZone.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (_rocketZone.promotionButton != null) _rocketZone.promotionButton.dispose();
    if (_rocketZone.promotionButton2 != null) _rocketZone.promotionButton2.dispose();
    if (_rocketZone.promotionButton3 != null) _rocketZone.promotionButton3.dispose();
    if (_rocketZone.promotionButton4 != null) _rocketZone.promotionButton4.dispose();
    if (_rocketZone.promotionButton5 != null) _rocketZone.promotionButton5.dispose();
    if (_rocketZone.closeButton != null) _rocketZone.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (_swapZone.promotionButton != null) _swapZone.promotionButton.dispose();
    if (_swapZone.promotionButton2 != null) _swapZone.promotionButton2.dispose();
    if (_swapZone.promotionButton3 != null) _swapZone.promotionButton3.dispose();
    if (_swapZone.promotionButton4 != null) _swapZone.promotionButton4.dispose();
    if (_swapZone.promotionButton5 != null) _swapZone.promotionButton5.dispose();
    if (_swapZone.closeButton != null) _swapZone.closeButton.dispose();
  } catch(e) {

  }

  try {
    if (_videoZone.promotionButton != null) _videoZone.promotionButton.dispose();
    if (_videoZone.promotionButton2 != null) _videoZone.promotionButton2.dispose();
    if (_videoZone.promotionButton3 != null) _videoZone.promotionButton3.dispose();
    if (_videoZone.promotionButton4 != null) _videoZone.promotionButton4.dispose();
    if (_videoZone.promotionButton5 != null) _videoZone.promotionButton5.dispose();
    if (_videoZone.closeButton != null) _videoZone.closeButton.dispose();
  } catch(e) {

  }
}

// 프로모션 버튼 및 이벤트 추가
function addButtonEventEachMesh(building, src) {
  var section = false;
  switch(building.name) {
    case "BRAND_ZONE":
    case "NFT_ZONE":
    case "ROKET_ZONE":
    case "SWAP_ZONE":
    case "VIDEO_ZONE":
      section = true;
      break;
  }

  var promotionButton = BABYLON.GUI.Button.CreateImageOnlyButton(
    "but",
    "assets/img/"+src
  );
  promotionButton.parent = building;
  promotionButton.width = window.innerWidth > 640 ? "80px" : "60px";
  promotionButton.height = window.innerWidth > 640 ? "80px" : "60px";
  promotionButton.thickness = 0;
  if (src == "20220425/aggregator.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -50 : -12;
    promotionButton.linkOffsetY = section ? 100 : 250;
    promotionButton.linkOffsetZ = 0;
  } else if (src == "20220425/de-fi.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -50 : -12;
    promotionButton.linkOffsetY = section ? (window.innerWidth > 640 ? 0 : 35) : 150;
    promotionButton.linkOffsetZ = 0;
  } else if (src == "20220425/governance_treasury.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? 50 : 50;
    promotionButton.linkOffsetY = section ? (window.innerWidth > 640 ? 0 : 35) : 150;
    promotionButton.linkOffsetZ = 0;
  // } else if (src == "20220425/btn_partnership.png") {
  //   promotionButton.linkOffsetX = window.innerWidth > 640 ? -150 : -75;
  //   promotionButton.linkOffsetY = 100;
  //   promotionButton.linkOffsetZ = 0;
  } else if (src == "20220425/btn_about_launchpad.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -50 : -12;
    promotionButton.linkOffsetY = section ? 100 : 250;
    promotionButton.linkOffsetZ = 0;  
  } else if (src == "20220425/btn_dao.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -150 : -75;
    promotionButton.linkOffsetY = 100;
    promotionButton.linkOffsetZ = 0;
  } else if (src == "20220425/btn_housing.png") {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -150 : -75;
    promotionButton.linkOffsetY = 100;
    promotionButton.linkOffsetZ = 0;
  } else {
    promotionButton.linkOffsetX = window.innerWidth > 640 ? -50 : -12;
    promotionButton.linkOffsetY = section ? 100 : 250;
    promotionButton.linkOffsetZ = 0;  
  }
  
  advancedTexture.addControl(promotionButton); 
  promotionButton.linkWithMesh(building);

  promotionButton.actionManager = new BABYLON.ActionManager(scene);

  // ]-----] 메시 클릭시 이벤트 [-----[
  promotionButton.onPointerUpObservable.add(function(){
    switch(building.name) {
      case "256_shop":
        $("#dimLayer").fadeIn();
        $(".box-hiddenmission-modal").fadeIn();
        break;
      case "VKC_shop":
        $("#dimLayer").fadeIn();
        $("#preparingModal").fadeIn();
        break;
      case "PB_shop":
        $("#dimLayer").fadeIn();
        $("#preparingModal").fadeIn();
        break;
      case "tencent_shop":
        $("#dimLayer").fadeIn();
        $("#preparingModal").fadeIn();
        break;
      case "Apple_shop":
        $("#dimLayer").fadeIn();
        $("#preparingModal").fadeIn();
        break;
      case "ROKET_ZONE":
        if (src == "20220413/gui_btn_world.png") {
          $("#dimLayer").fadeIn();
          $(".box-world-modal").fadeIn();
        } else if (src == "20220425/btn_housing.png") {
          $("#dimLayer").fadeIn();
          $(".box-housing-modal").fadeIn();
        }
        
        break;
      case "SWAP_ZONE":
        $("#dimLayer").fadeIn();
        if (src == "20220425/aggregator.png") {
          $(".box-aggregator-modal").fadeIn();
        } else if (src == "20220425/de-fi.png") {
          $(".box-defi-modal").fadeIn();
        } else if (src == "20220425/governance_treasury.png") {
          $(".box-synthetics-modal").fadeIn();
        }
        break;
      case "NFT_ZONE":
        $("#dimLayer").fadeIn();
        $(".box-nft-modal").fadeIn();
        $(".box-nft-modal img").hide();
        $(".box-nft-modal .p1").addClass("show").show();
        $(".box-nft-modal .p5").removeClass("show");
        $(".box-nft-modal .box-action-type1").show();
        break;
      case "VIDEO_ZONE":
        $("#dimLayer").show();
        if (src == "20220425/btn_about_launchpad.png") {
          $("#dimLayer").fadeIn();
          $(".box-launchpad-modal").fadeIn();

        } else {
          if (window.innerWidth > 640) {
            $("#governanceModal").show();
          } else {
            $("#governanceMobileModal").show();
          }
        }
        
        break;
    }
  });

  console.log(src);
  building.promotionButton = promotionButton;
  if (src == "20220425/de-fi.png") {
    building.promotionButton2 = promotionButton;
  } else if (src == "20220425/governance_treasury.png") {
    building.promotionButton3 = promotionButton;
  } else if (src == "20220425/aggregator.png") {
    building.promotionButton4 = promotionButton;
  // } else if (src == "20220425/btn_partnership.png") {
  //   building.promotionButton5 = promotionButton;
  } else if (src == "20220425/btn_about_launchpad.png") { 
    building.promotionButton2 = promotionButton;
  } else if (src == "20220425/btn_housing.png") {
    building.promotionButton2 = promotionButton;
  } else if (src == "20220413/gui_btn_world.png") {
    building.promotionButton3 = promotionButton;
  }
}

// 준비중 모달 클릭시 숨기기
$(document).on("click", "#preparingModal", function() {
  $("#preparingModal").fadeOut();
  $("#dimLayer").fadeOut();
});


// 로고 클릭시 새로고침
$(document).on("click", ".pc-logo", function() {
  location.reload();
});


// 모바일 스테이킹 모달 스크롤 이벤트 리스너
$("#mobileStakingBody").scroll(function() {
    $("#mobileStakingHeader").scrollLeft($(this).scrollLeft());
});

// 인트로 모달 닫기
$(".box-intro-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-intro-modal").fadeOut();
  return false;
});

// 루니버스 모달 열기
$(".box-intro-modal .btn-description").click(function() {
  $(".box-intro-modal").hide();
  $(".box-luniverse-modal").show();
  return false;
});

// 루니버스 모달 닫기
$(".box-luniverse-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-luniverse-modal").fadeOut();
  return false;
});

// 파트너십 모달 닫기
// $(".box-partnership-modal .btn-close, .box-partnership-modal .box-close-action").click(function() {
//   $("#dimLayer").fadeOut();
//   $(".box-partnership-modal").fadeOut();
//   return false;
// });

// 런치패드 모달 닫기
$(".box-launchpad-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-launchpad-modal").fadeOut();
  return false;
});

// 로드맵 모달 열기
$("#roadmap").click(function() {
  $("#dimLayer").fadeIn();
  $(".box-roadmap-modal").fadeIn();
  return false;
});

// 로드맵 모달 닫기
$(".box-roadmap-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-roadmap-modal").fadeOut();
  return false;
});

// 하우징 모달 닫기
$(".box-housing-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-housing-modal").fadeOut();
  return false;
});

// 히든미션 코드 카피
$(".box-hiddenmission-modal .btn-copy").click(function() {
  var tempElem = document.createElement('textarea');
  tempElem.value = 'The Balance X METACONG';  
  document.body.appendChild(tempElem);

  tempElem.select();
  document.execCommand("copy");
  document.body.removeChild(tempElem);
  alert("복사되었습니다.")
  return false;
});

// 히든미션 모달 닫기
$(".box-hiddenmission-modal .btn-close").click(function() {
  $("#dimLayer").fadeOut();
  $(".box-hiddenmission-modal").fadeOut();
  return false;
});

// NFT 모달 다음 버튼
$(".box-nft-modal .box-action-type1 a").click(function() {
  if ($(this).hasClass("btn-close")) {
    $("#dimLayer").fadeOut();
    $(".box-nft-modal").fadeOut();
    $(".box-nft-modal .box-action-type1").show();
    $(".box-nft-modal .box-action-type2").hide();
    $(".box-nft-modal .box-action-type3").hide();

    if (window.innerWidth <= 640) {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p1").addClass("show");    
    }
  } else if ($(this).hasClass("btn-next")) {
    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type3").hide();

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p2").addClass("show");  
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p2").addClass("show");    
    }
  }
  return false;
});

// NFT 모달 다음 버튼
$(".box-nft-modal .box-action-type2 .btn-next").click(function() {
  if ($(".box-nft-modal img.show").hasClass("p2") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p2")) {
    
    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p3").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p3").addClass("show");    
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type3").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p3") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p3")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p4").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p4").addClass("show");    
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type3").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p4") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p4")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p5").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p5").addClass("show");    
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type3").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p5") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p5")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p6").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p6").addClass("show");    
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type3").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p6") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p6")) {

    // if (window.innerWidth > 640) {
    //   $(".box-nft-modal img").removeClass("show");
    //   $(".box-nft-modal img.p7").addClass("show");
    // } else {
    //   $(".box-nft-modal .img-mobile > div").removeClass("show");
    //   $(".box-nft-modal .img-mobile > div.p7").addClass("show");    
    // }

    // $(".box-nft-modal .box-action-type1").hide();
    // $(".box-nft-modal .box-action-type2").hide();
    // $(".box-nft-modal .box-action-type3").show();

    // if (window.innerWidth > 640) {
    //   $(".box-nft-modal img").removeClass("show");
    //   $(".box-nft-modal img.p8").addClass("show");
    // } else {
    //   $(".box-nft-modal .img-mobile > div").removeClass("show");
    //   $(".box-nft-modal .img-mobile > div.p8").addClass("show");    
    // }

    // $(".box-nft-modal .box-action-type1").hide();
    // $(".box-nft-modal .box-action-type2").hide();
    // $(".box-nft-modal .box-action-type3").show();

    $(".box-nft-modal").fadeOut();
    $(".box-nft-modal img").removeClass("show");
    $(".box-nft-modal div").removeClass("show");
    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").hide();
    $(".box-nft-modal .box-action-type1").show();
    $("#dimLayer").fadeOut();
  } 
  // else if ($(".box-nft-modal img.show").hasClass("p7") || 
  //   $(".box-nft-modal .img-mobile > div.show").hasClass("p7")) {

  //   if (window.innerWidth > 640) {
  //     $(".box-nft-modal img").removeClass("show");
  //     $(".box-nft-modal img.p8").addClass("show");
  //   } else {
  //     $(".box-nft-modal .img-mobile > div").removeClass("show");
  //     $(".box-nft-modal .img-mobile > div.p8").addClass("show");    
  //   }

  //   $(".box-nft-modal .box-action-type1").hide();
  //   $(".box-nft-modal .box-action-type2").hide();
  //   $(".box-nft-modal .box-action-type3").show();
  // }
  
  return false;
});

// NFT 모달 이전 버튼
$(".box-nft-modal .box-action-type2 .btn-prev").click(function() {
  if ($(".box-nft-modal img.show").hasClass("p2") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p2")) {
    
    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p1").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p1").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").hide();
    $(".box-nft-modal .box-action-type1").show();
  } else if ($(".box-nft-modal img.show").hasClass("p3") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p3")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p2").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p2").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type1").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p4") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p4")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p3").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p3").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type1").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p5") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p5")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p4").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p4").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type1").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p6") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p6")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p5").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p5").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type1").hide();
  } else if ($(".box-nft-modal img.show").hasClass("p7") || 
    $(".box-nft-modal .img-mobile > div.show").hasClass("p7")) {
    if (window.innerWidth > 640) {
      $(".box-nft-modal img").removeClass("show");
      $(".box-nft-modal img.p6").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div").removeClass("show");
      $(".box-nft-modal .img-mobile > div.p6").addClass("show");    
    }

    $(".box-nft-modal .box-action-type3").hide();
    $(".box-nft-modal .box-action-type2").show();
    $(".box-nft-modal .box-action-type1").hide();
  }
  
  return false;
});

// NFT 모달 닫기
$(".box-nft-modal .box-action-type3 a.btn-close").click(function() {
  $(".box-nft-modal").fadeOut();
  $(".box-nft-modal img").removeClass("show");
  $(".box-nft-modal div").removeClass("show");
  $(".box-nft-modal .box-action-type3").hide();
  $(".box-nft-modal .box-action-type2").hide();
  $(".box-nft-modal .box-action-type1").show();
  $("#dimLayer").fadeOut();

  return false;
});

// NFT 미션코드 복사
$(".box-nft-modal .box-action-type3 a.btn-copy").click(function() {
var tempElem = document.createElement('textarea');
  tempElem.value = 'METACONG Membership NFT';  
  document.body.appendChild(tempElem);

  tempElem.select();
  document.execCommand("copy");
  document.body.removeChild(tempElem);
  alert("복사되었습니다.")
  return false;
});

// NFT 모달 탭메뉴
$(".box-nft-modal .box-tab-menu a").click(function() {
  if (window.innerWidth > 640) {
    $(".box-nft-modal img").removeClass("show").hide();
  } else {
    $(".box-nft-modal .img-mobile > div").removeClass("show").hide();
  }
  
  if ($(this).hasClass("tab1")) {
    
    if (window.innerWidth > 640) {
      $(".box-nft-modal img.p1").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div.p1").addClass("show");
    }

    $(".box-nft-modal .box-action-type2").hide();
    $(".box-nft-modal .box-action-type1").show();
  } else if ($(this).hasClass("tab2")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img.p2").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div.p2").addClass("show");
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
  } else if ($(this).hasClass("tab3")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img.p5").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div.p5").addClass("show");
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
  } else if ($(this).hasClass("tab4")) {

    if (window.innerWidth > 640) {
      $(".box-nft-modal img.p6").addClass("show");
    } else {
      $(".box-nft-modal .img-mobile > div.p6").addClass("show");
    }

    $(".box-nft-modal .box-action-type1").hide();
    $(".box-nft-modal .box-action-type2").show();
  }

  return false;
});

// 디파이 관련 모달 닫기, 메타콩 월드 모달 닫기
$(".box-defi-modal .btn-close, .box-synthetics-modal .btn-close, .box-aggregator-modal .btn-close, .box-world-modal .btn-close").click(function(){
  $("#dimLayer").fadeOut();
  $(this).closest("div").fadeOut();
  return false;
});

// 어그리게이터 모달 페이지 이동
$(".box-aggregator-modal .box-action a").click(function() {
  if ($(this).hasClass("btn-prev")) {
    $(".box-aggregator-modal .p1").show();
    $(".box-aggregator-modal .p2").hide();
  } else {
    $(".box-aggregator-modal .p1").hide();
    $(".box-aggregator-modal .p2").show();
  }

  return false;
});

// 트레저리 모달 페이지 이동
$(".box-synthetics-modal .box-action a").click(function() {
  if ($(this).hasClass("btn-prev")) {
    $(".box-synthetics-modal .p1").show();
    $(".box-synthetics-modal .p2").hide();
  } else {
    $(".box-synthetics-modal .p1").hide();
    $(".box-synthetics-modal .p2").show();
  }

  return false;
});

// 월드 모달 페이지 이동
$(".box-world-modal .box-action a").click(function() {
  if ($(this).hasClass("btn-prev")) {
    if ($(".box-world-modal .p2").hasClass("show")) {
      $(".box-world-modal .p2").removeClass("show");
      $(".box-world-modal .p1").addClass("show");
    } else if ($(".box-world-modal .p3").hasClass("show")) {
      $(".box-world-modal .p3").removeClass("show");
      $(".box-world-modal .p2").addClass("show");
    } else if ($(".box-world-modal .p4").hasClass("show")) {
      $(".box-world-modal .p4").removeClass("show");
      $(".box-world-modal .p3").addClass("show");
    } else if ($(".box-world-modal .p5").hasClass("show")) {
      $(".box-world-modal .p5").removeClass("show");
      $(".box-world-modal .p4").addClass("show");
    } else if ($(".box-world-modal .p6").hasClass("show")) {
      $(".box-world-modal .p6").removeClass("show");
      $(".box-world-modal .p5").addClass("show");
    } 
    // else if ($(".box-world-modal .p7").hasClass("show")) {
    //   $(".box-world-modal .p7").removeClass("show");
    //   $(".box-world-modal .p6").addClass("show");
    // }

  } else {
    if ($(".box-world-modal .p1").hasClass("show")) {
      $(".box-world-modal .p1").removeClass("show");
      $(".box-world-modal .p2").addClass("show");
    } else if ($(".box-world-modal .p2").hasClass("show")) {
      $(".box-world-modal .p2").removeClass("show");
      $(".box-world-modal .p3").addClass("show");
    } else if ($(".box-world-modal .p3").hasClass("show")) {
      $(".box-world-modal .p3").removeClass("show");
      $(".box-world-modal .p4").addClass("show");
    } else if ($(".box-world-modal .p4").hasClass("show")) {
      $(".box-world-modal .p4").removeClass("show");
      $(".box-world-modal .p5").addClass("show");
    } else if ($(".box-world-modal .p5").hasClass("show")) {
      $(".box-world-modal .p5").removeClass("show");
      $(".box-world-modal .p6").addClass("show");
    } 
    // else if ($(".box-world-modal .p6").hasClass("show")) {
    //   $(".box-world-modal .p6").removeClass("show");
    //   $(".box-world-modal .p7").addClass("show");
    // }
  }

  return false;
});






