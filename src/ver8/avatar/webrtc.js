var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');
var localAudio = document.querySelector('#localAudio');

const audioInputSelect = document.querySelector('select#micList');
const audioOutputSelect = document.querySelector('select#audioList');
const videoSelect = document.querySelector('select#videoList');
const selectors = [audioInputSelect, audioOutputSelect, videoSelect];

var webcamState = false;

function start() {
    if (window.stream) {
        window.stream.getTracks().forEach(track => {
            track.stop();
        });
    }
    const audioSource = audioInputSelect.value;
    // const videoSource = videoSelect.value;
    const constraints = {
        audio: {
            deviceId: audioSource ? {
                exact: audioSource
            } : undefined
        },
        // video: {
        //     deviceId: videoSource ? {
        //         exact: videoSource
        //     } : undefined
        // }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(gotStream).then(gotDevices).catch(handleError);
}
var startMap = 0;

function gotStream(stream) {
    console.log('Adding local stream.');

    window.stream = stream;
    localStream = stream;
    screanStream = stream;
    // localVideo.srcObject = stream;

    msg = {
        'socketId': socket.id,
        'streamId': localStream.id,
        'displayName': '콩콩쓰',
        'dest': 'all'
    }
    sendMessage(msg);
    if (startMap > 0) {
        initPlayerCam();
    }

    if (startMap == 0) {
        if (localStream != null && localStream.getAudioTracks().length > 0) {
            var toggle = document.getElementById('toggle-mic');
            $('.toggle-mic-on').css('display', 'none');
            $('.toggle-mic-off').css('display', 'block');
            toggle.setAttribute('data', 'off');
            localStream.getAudioTracks()[0].enabled = false;
            mic_switch = false;
        }

        // 서버로 자신의 정보를 전송한다.
        data = {
            position_x: 3,
            position_z: 35
        }
        socket.emit("join", data);

        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰',
            'dest': 'all'
        }
        sendMessage(msg);
        webcamState = true;

        main_start();
        startMap = 1;
    }
    return navigator.mediaDevices.enumerateDevices();
}

function gotDevices(deviceInfos) {
    // Handles being called several times to update labels. Preserve values.
    const values = selectors.map(select => select.value);
    selectors.forEach(select => {
        while (select.firstChild) {
            select.removeChild(select.firstChild);
        }
    });

    for (let i = 0; i !== deviceInfos.length; ++i) {
        const deviceInfo = deviceInfos[i];
        const option = document.createElement('option');
        option.value = deviceInfo.deviceId;
        if (deviceInfo.kind === 'audioinput') {
            option.text = deviceInfo.label || `microphone ${audioInputSelect.length + 1}`;
            audioInputSelect.appendChild(option);
        } else if (deviceInfo.kind === 'audiooutput') {
            option.text = deviceInfo.label || `speaker ${audioOutputSelect.length + 1}`;
            audioOutputSelect.appendChild(option);
        } else if (deviceInfo.kind === 'videoinput') {
            option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
            videoSelect.appendChild(option);
        } else {
            console.log('Some other kind of source/device: ', deviceInfo);
        }
    }
    selectors.forEach((select, selectorIndex) => {
        if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex])) {
            select.value = values[selectorIndex];
        }
    });
}

function attachSinkId(element, sinkId) {
    if (typeof element.sinkId !== 'undefined') {
        element.setSinkId(sinkId)
            .then(() => {
                console.log(`Success, audio output device attached: ${sinkId}`);
            })
            .catch(error => {
                let errorMessage = error;
                if (error.name === 'SecurityError') {
                    errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
                }
                console.error(errorMessage);
                // Jump back to first output device in the list as it's the default.
                audioOutputSelect.selectedIndex = 0;
            });
    } else {
        console.warn('Browser does not support output device selection.');
    }
}

function changeAudioDestination() {
    const audioDestination = audioOutputSelect.value;
    attachSinkId(localVideo, audioDestination);
}

function handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

audioInputSelect.onchange = start;
audioOutputSelect.onchange = changeAudioDestination;

videoSelect.onchange = start;

start();

function shareScreen() {
    if ($('#stream_state').val() != 'stop') {
        alert('이미 사용중입니다.');
        return;
    }
    //try {
    navigator.mediaDevices.getDisplayMedia({
		video: true
	}).then(function(shareScreensStream){
        [videoTrack] = shareScreensStream.getVideoTracks();
        [audioTrack] = localStream.getAudioTracks();

        shareScreensStream.getTracks()[0].addEventListener('ended', () => {
            // 공유 종료 이벤트
            console.log("ended");
            localStopStreaming();
        })
        localStream = new MediaStream([videoTrack, audioTrack]);
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰',
            'dest': 'all'
        }
        sendMessage(msg);

        localVideoStream('screen');

        //  }).catch(function(e){ });
    })
}

function toggleSetting() {
    var display = document.getElementById("settingBox").style.display
    if (display == "none") {
        $('.name-box').show();
        $('.button-webcam').hide();
        $('.url-box').hide();
        $('#settingBox').show();
        $('.video-select').show();
        $("#dimLayer").show();
        document.getElementById('setting').innerText = "";
        console.log($("#name").val());
    } else {
        $('#settingBox').hide();
        $("#dimLayer").hide();
        document.getElementById('setting').innerText = "";
    }
}

const chatIcon = document.getElementById("chatIcon");
const chattingBox = document.getElementById("chattingBox");
const btnCloseChat = document.getElementById("btnCloseChattingBox");

chatIcon.addEventListener("click", function () {
    chattingBox.classList.add("open");
    this.style.display = "none";
})
btnCloseChat.addEventListener("click", function () {
    chattingBox.classList.remove("open");
    chatIcon.style.display = "block";
})

let mic_switch = true;
let video_switch = true;

function toggleVideo() {
    if (localStream != null && localStream.getVideoTracks().length > 0) {
        video_switch = !video_switch;
        localStream.getVideoTracks()[0].enabled = video_switch;
    }
}

function toggleMic() {
    console.log(localStream.getAudioTracks());
    console.log(localStream.getAudioTracks()[0].enabled)
    if (localStream != null && localStream.getAudioTracks().length > 0) {
        var toggle = document.getElementById('toggle-mic');
        if (toggle.getAttribute('data') == 'off') {
            $('.toggle-mic-on').css('display', 'block');
            $('.toggle-mic-off').css('display', 'none');
            $(".toggle-mic").addClass("on");
            $("#setMicStatus").addClass("on");
            toggle.setAttribute('data', 'on');
        } else {
            $('.toggle-mic-on').css('display', 'none');
            $('.toggle-mic-off').css('display', 'block');
            $("#setMicStatus").removeClass("on");
            toggle.setAttribute('data', 'off');
            $(".toggle-mic").removeClass("on");
        }
        mic_switch = !mic_switch;
        localStream.getAudioTracks()[0].enabled = mic_switch;
    }
}

function toggleAvatar() {
    if (player_info.avatarState == "avatar") {
        player_info.avatarState = "webCam"
    } else {
        player_info.avatarState = "avatar"
    }

    msg = {
        socketId: player_info.socketId,
        avatarState: player_info.avatarState
    }
    socket.emit("userAvatarChange", msg);

    var toggle = document.getElementById('toggle-video');
    if (toggle.getAttribute('data') == 'off') {
        $('.toggle-video-on').css('display', 'block');
        $('.toggle-video-off').css('display', 'none');
        $(".toggle-video").addClass("on");
        $("#setCameraStauts").addClass("on");
        $("#displayCameraOff").hide();
        $("#localVideoContainer").show();
        toggle.setAttribute('data', 'on');
    } else {
        $('.toggle-video-on').css('display', 'none');
        $('.toggle-video-off').css('display', 'block');
        $(".toggle-video").removeClass("on");
        $("#setCameraStauts").removeClass("on");
        $("#displayCameraOff").show();
        $("#localVideoContainer").hide();
        toggle.setAttribute('data', 'off');
    }
}

function fullScreen() {
    var stream_state = $('#stream_state').val();
    if (stream_state == 'url') {
        let node = document.getElementById("iframe-video").src;
        var newWindow = window.open("about:blank");
        newWindow.location.href = node;
    } else if (stream_state == 'webCam' || stream_state == 'screen') {
        if (player_info.streamer == player_info.socketId) {
            // 팝업 전체화면
            $('#remoteVideos').css('height', '80%');
        } else {
            // 팝업 전체화면
            $('#remoteVideos').css('height', '0');
            $("#remoteVideo_" + player_info.streamer).addClass("show");
        }
        $("#popup").fadeIn();
    }
}

function intro_close() {
    $('#intro').attr('class', 'fadeout');

    setTimeout(function () {
        $('#intro').css('display', 'none');
    }, 2000);
}

function send() {
    var chat_msg = $('#chatMsg').val();
    if (chat_msg != '' && chat_msg != ' ') {
        // 서버로 메시지를 전송한다.
        socket.emit("chat", {
            userName: player_info.name,
            msg: chat_msg
        });
        let today = new Date();
        let hours = ('0' + today.getHours()).slice(-2);
        let minutes = ('0' + today.getMinutes()).slice(-2);
        var seconds = ('0' + today.getSeconds()).slice(-2);
        $("#chatContent").append("<li class='my-chat'><strong>[" + $('#name').val() + "]</strong><span class='moment'>" + hours + ":" + minutes + ":" + seconds + "</span><p class='speak'> " + chat_msg + "</p></li>");
        $('#chatContent').scrollTop($('#chatContent')[0].scrollHeight);
        $('#chatMsg').val("");
    }
}
$(document).ready(function () {

    $("#popup .exit").click(function () {
        $("#remotePlayerVideos video").removeClass("show");
        $("#popup").fadeOut();
    });
    $("#btnCloseHz").click(function () {
        $("#healingZone").hide();
        $("#dimLayer").hide();
        $("#healingZone .page1").show();
        $("#healingZone .page2").hide();
    });
    $("#btnCloseCz, .btnCloseCz").click(function () {
        $("#commerceZone").hide();
        $("#dimLayer").hide();
        $("#commerceZone .page1").show();
        $("#commerceZone .page2").hide();
    });
    $(document).bind('remotePlayerVideo', function (event, data) {
        socket.emit("playerInfo", {
            socketId: data.socketId
        });

        if (data.socketId == player_info.streamer) {
            socket.emit("streamingInfo", {});
        }
    });

});

window.onbeforeunload = function () {
    sendMessage('bye');
};