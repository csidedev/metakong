// socket.io 서버에 접속한다
var socket = io("wss://metaforming.com:3000");
//var socket = io('http://localhost:5010');

var players = [];

$(document).ready(function () {
   
    $('#chat_msg').keypress(function (evt) {
        if((evt.keyCode || evt.which) == 13){
            evt.preventDefault();
            send();
        }
    });

    // 서버로 자신의 정보를 전송한다.
    socket.emit("login", new BABYLON.Vector3(3, 0.07, 35));
});
// 총 접속자 수
socket.on("clientsCount", function(data) {
  $("#clients_count").html("접속자 수 : "+data);
});

// 유저 입장
socket.on("login", function(playerList, newPlayer) {
  $('#name').val(newPlayer.userName);
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + newPlayer.userName + "</strong>님 입장!</div>");
  setNameLabel(newPlayer.id);
  var i =0;
  for(i=0 ; i<playerList.length; i++){
    createPlayer(playerList[i].player);
  }
});

// 다른 유저 입장
socket.on("createPlayer", function(data) {
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + data.userName + "</strong>님 입장!</div>");
  createPlayer(data);
});

// 유저 나감
socket.on("disconnectPlayer", function(data) {
  var state = true;
  for (var i = 0; i < players.length; i++) {
    for (var j = 0; j< data.length; j++) {
      if (data[j].player.id == players[i].id) {
        state = false;
        break;
      }
    }
    if(!state){
      removeAvatar(players[i]);
      players.splice(i, i);
    }
  }
});

socket.on("positionUpdate", function(positionData) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].id == positionData.socketId) {
      index = i;
      break;
    }
  }
  
  if(index > -1){
    let player = players[index];
    player.player.position = positionData.position;
    player.player.rotation =new BABYLON.Vector3(0,positionData.rotation._y,0);
    player.nameTag.position = new BABYLON.Vector3(positionData.position._x, positionData.position._y + 2, positionData.position._z);
  }
});

socket.on("animationUpdate", function(data) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].id == data.socketId) {
      index = i;
      break;
    }
  }
  if(index > -1){
    let player = players[index].player;

    // 아바타 애니메이션 설정
    player.skeleton.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
    player.skeleton.animationPropertiesOverride.enableBlending = true;
    player.skeleton.animationPropertiesOverride.blendingSpeed = 1;
    player.skeleton.animationPropertiesOverride.loopMode = 1;

    var idleRange = skeleton.getAnimationRange("YBot_Idle");
    var walkRange = skeleton.getAnimationRange("YBot_Walk");
    var runRange = skeleton.getAnimationRange("YBot_Run");

    if(data.animating){
      scene.beginAnimation(player.skeleton, walkRange.from, walkRange.to, true);
    } else {
      scene.beginAnimation(player.skeleton, idleRange.from, idleRange.to, true);
    }
  }
});
// 메시지
socket.on("chat", function(data) {
  $("#chat_content").append("<div>" + data.from.name + " : " + data.msg + "</div>");
});

function send() {
  var chat_msg =$('#chat_msg').val();
  if(chat_msg != '' && chat_msg != ' ' ){
    // 서버로 메시지를 전송한다.
    socket.emit("chat", { msg: chat_msg });
    $("#chat_content").append("<div><strong>" + $('#name').val() + " : " + chat_msg + "</strong></div>");
    $('#chat_msg').val("");
  }
}

function chat_user() {

    // Socket 통신
}
