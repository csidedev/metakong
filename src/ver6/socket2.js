// socket.io 서버에 접속한다
var socket = io("wss://metaforming.com:3000");
//var socket = io('http://localhost:5010');

var players = [];

var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var screanStream;
var pc;
var remoteStream;
var turnReady;
var peerConnections = {}; // key is uuid, values are peer connection object and user defined display name string

var streamPeer = null;
const peers = {};
var conn = null;

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  },
  {
    url: 'turn:numb.viagenie.ca',
    credential: 'muazkh',
    username: 'webrtc@live.com'
  }
  ]
};

var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

$(document).ready(function () {

  $('#chat_msg').keypress(function (evt) {
    if ((evt.keyCode || evt.which) == 13) {
      evt.preventDefault();
      send();
    }
  });
  // 서버로 자신의 정보를 전송한다.
  socket.emit("join", new BABYLON.Vector3(3, 0.07, 35));
});

// 총 접속자 수
socket.on("clientsCount", function (data) {
  $("#clients_count").html("현재 접속자 수 : " + data);
});

// 유저 닉네임 변경
socket.on("userNameUpdate", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      updatePlayerNameLabel(i, data.userName);
      break;
    }
  }
});

// 유저 아바타 변경
socket.on("userAvatarUpdate", function (data) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      players[i].avatarState=data.state;
      userAvatarUpdate(i, data);
      break;
    }
  }
});

// 유저 입장
socket.on("join", function (playerList, newPlayer) {
  
  $('#name').val(newPlayer.userName);
  player_info.name = newPlayer.userName;
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + newPlayer.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  setNameLabel(newPlayer.socketId);
  console.log(playerList.length);
  if (playerList.length == 0){
    isInitiator = true;
  }

  // 기존 유저 생성
  for (var i = 0; i < playerList.length; i++) {
    //createPlayer(playerList[i].player);
  }
});

// 다른 유저 입장
socket.on("createPlayer", function (data) {
  $("#chat_content").append("<div>[안내] 두둥! <strong>" + data.userName + "</strong>님 등장!</div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
  //createPlayer(data);
});

// 유저 나감
socket.on("disconnectPlayer", function (data) {
  var state = true;
  for (var i = 0; i < players.length; i++) {
    for (var j = 0; j < data.length; j++) {
      if (data[j].player.socketId == players[i].socketId) {
        state = false;
        break;
      }
    }
    if (!state) {
      $("#chat_content").append("<div>[안내] <strong>" + data[j].player.userName + "</strong>님 나감!</div>");
      $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
      removeAvatar(players[i]);
      players.splice(i, i);
    }
  }
});

// 유저 이동
socket.on("positionUpdate", function (positionData) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == positionData.socketId) {
      index = i;
      break;
    }
  }
  if (index > -1) {

    let player = players[index];
    player.player.position = positionData.position;
    if(player.avatarState != "webcam"){
      player.player.rotation = new BABYLON.Vector3(0, positionData.rotation._y, 0);
    }
    player.nameTag.position = new BABYLON.Vector3(positionData.position._x, 2, positionData.position._z);
  }
});

// 유저 액션 변경
socket.on("animationUpdate", function (data) {
  var index = -1;
  for (var i = 0; i < players.length; i++) {
    if (players[i].socketId == data.socketId) {
      index = i;
      break;
    }
  }
  if (index > -1) {
    let playerA = players[index].player;
    if (players[index].avatarState == "avatar"){
      // 아바타 애니메이션 설정
      playerA.skeleton.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
      playerA.skeleton.animationPropertiesOverride.enableBlending = true;
      playerA.skeleton.animationPropertiesOverride.blendingSpeed = 1;
      playerA.skeleton.animationPropertiesOverride.loopMode = 1;

      var idleRange = skeleton.getAnimationRange("YBot_Idle");
      var walkRange = skeleton.getAnimationRange("YBot_Walk");
      var runRange = skeleton.getAnimationRange("YBot_Run");

      if (data.action == "walk") {
        scene.beginAnimation(playerA.skeleton, walkRange.from, walkRange.to, true);
      } else if (data.action == "run") {
        scene.beginAnimation(playerA.skeleton, runRange.from, runRange.to, true);
      } else {
        scene.beginAnimation(playerA.skeleton, idleRange.from, idleRange.to, true);
      }
    }
  }
});

function sendMessage(message) {
  //console.log('Client sending message: ', message);
  socket.emit('message', message);
}

// 메시지
socket.on("message", function (signal) {
  var socketId = signal.socketId;
  var localUuid = socket.id;

  // Ignore messages that are not for us or from ourselves
  if (socketId == localUuid || (signal.dest != localUuid && signal.dest != 'all')) return;

  if (signal.displayName && signal.dest == 'all') {
    // 새로 들어온 peer 연결 요청하는
    // set up peer connection object for a newcomer peer
    setUpPeer(socketId, signal.displayName);
    //serverConnection.send(JSON.stringify({ 'displayName': localDisplayName, 'uuid': localUuid, 'dest': peerUuid }));
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'displayName': '콩콩쓰', 
      'dest': socketId
    }
    // 요청 허가 답변 보낸다
    sendMessage(msg)
  } else if (signal.displayName && signal.dest == localUuid) {
    // initiate call if we are the newcomer peer
    // 요청 허가 답변 받는다 
    setUpPeer(socketId, signal.displayName, true);

  } else if (signal.sdp) {
    peerConnections[socketId].pc.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function () {
      // Only create answers in response to offers
      if (signal.sdp.type == 'offer') {
        peerConnections[socketId].pc.createAnswer().then(description => createdDescription(description, socketId)).catch(errorHandler);
      }
    }).catch(errorHandler);

  } else if (signal.ice) {
    peerConnections[socketId].pc.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
  }
});
socket.on('log', function (array) {
  console.log.apply(console, array);
});

// 메시지
socket.on("chat", function (data) {
  let today = new Date();
  let hours = ('0' + today.getHours()).slice(-2);
  let minutes = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2);
  $("#chat_content").append("<div><strong>" + data.from.name + " </strong>" + hours + ":" + minutes + ":" + seconds + "<p> " + data.msg + "</p></div>");
  $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
});

socket.on('streamWebcam', function (data) {
  removeVideoStream();

  if (! document.getElementById('remoteVideo_' + data.socketId)){
    var vidElement = document.createElement('video');
    vidElement.setAttribute('autoplay', '');
    vidElement.setAttribute('id', 'remoteVideo_' + data.socketId);
    vidElement.setAttribute('width', 345);
    vidElement.setAttribute('height', 200);
    vidElement.setAttribute('src', "https://bitbucket.org/csidedev/metakong/raw/master/src/ver6/assets/img/metakong.mp4");

    document.getElementById('remotePlayerVideos').appendChild(vidElement);
  }
  const video =  document.getElementById("remoteVideo_"+data.socketId);
  mainScreenStream(video);
  mainScreenStreamer(data.streamer);
});
socket.on('streamURL', function (data) {
  addVideoStream('url',data.url,data.streamer);
});
socket.on('streamScreen', function (data) {
  removeVideoStream();

  if (! document.getElementById('remoteVideo_' + data.socketId)){
    var vidElement = document.createElement('video');
    vidElement.setAttribute('autoplay', '');
    vidElement.setAttribute('id', 'remoteVideo_' + data.socketId);
    vidElement.setAttribute('width', 345);
    vidElement.setAttribute('height', 200);
    vidElement.setAttribute('src', "https://bitbucket.org/csidedev/metakong/raw/master/src/ver6/assets/img/metakong.mp4");

    document.getElementById('remotePlayerVideos').appendChild(vidElement);
  }
  const video =  document.getElementById("remoteVideo_"+data.socketId);
  mainScreenStream(video);
  mainScreenStreamer(data.streamer);
});
socket.on('stopStreaming', function (data) {
  stopStreaming();
});

function send() {
  var chat_msg = $('#chat_msg').val();
  if (chat_msg != '' && chat_msg != ' ') {
    // 서버로 메시지를 전송한다.
    socket.emit("chat", { msg: chat_msg });
    let today = new Date();
    let hours = ('0' + today.getHours()).slice(-2);
    let minutes = ('0' + today.getMinutes()).slice(-2);
    var seconds = ('0' + today.getSeconds()).slice(-2);
    $("#chat_content").append("<div><strong>" + $('#name').val() + " </strong>" + hours + ":" + minutes + ":" + seconds + "<p> " + chat_msg + "</p></div>");
    $('#chat_content').scrollTop($('#chat_content')[0].scrollHeight);
    $('#chat_msg').val("");
  }
}

function setUpPeer(peerUuid, displayName, initCall = false) {
  peerConnections[peerUuid] = { 'displayName': displayName, 'pc': new RTCPeerConnection(pcConfig) };
  peerConnections[peerUuid].pc.ontrack = event => gotRemoteStream(event, peerUuid);
  peerConnections[peerUuid].pc.onicecandidate = event => gotIceCandidate(event, peerUuid);
  peerConnections[peerUuid].pc.oniceconnectionstatechange = event => checkPeerDisconnect(event, peerUuid);
  peerConnections[peerUuid].pc.addStream(localStream);
  if (initCall) {
    peerConnections[peerUuid].pc.createOffer().then(description => createdDescription(description, peerUuid)).catch(errorHandler);
  }
}

function gotIceCandidate(event, peerUuid) {
  if (event.candidate != null) {
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'dest': peerUuid,
      'ice': event.candidate
    }
    sendMessage(msg)
    //serverConnection.send(JSON.stringify({ 'ice': event.candidate, 'uuid': localUuid, 'dest': peerUuid }));
  }
}

function createdDescription(description, peerUuid) {
  console.log(`got description, peer ${peerUuid}`);
  console.log(description)
  peerConnections[peerUuid].pc.setLocalDescription(description).then(function () {
    
    msg = {
      'socketId': socket.id,
      'streamId': localStream.id,
      'dest': peerUuid,
      'sdp': peerConnections[peerUuid].pc.localDescription
    }
    sendMessage(msg)
    //serverConnection.send(JSON.stringify({ 'sdp': peerConnections[peerUuid].pc.localDescription, 'uuid': localUuid, 'dest': peerUuid }));
  }).catch(errorHandler);
}

function gotRemoteStream(event, peerUuid) {
  console.log(`got remote stream, peer ${peerUuid}`);
  console.log(event);
  if (!document.getElementById('remoteVideo_' + peerUuid)) {
    //assign stream to new HTML video element
    var vidElement = document.createElement('video');
    vidElement.setAttribute('autoplay', '');
    vidElement.setAttribute('id', 'remoteVideo_' + peerUuid);
    vidElement.setAttribute('width', 345);
    vidElement.setAttribute('height', 200);
    vidElement.srcObject = event.streams[0];

    document.getElementById('remotePlayerVideos').appendChild(vidElement);
  } else {
    document.getElementById('remoteVideo_' + peerUuid).srcObject = event.streams[0];
  }
  updateLayout();
}

function checkPeerDisconnect(event, peerUuid) {
  var state = peerConnections[peerUuid].pc.iceConnectionState;
  console.log(`connection with peer ${peerUuid} ${state}`);
  if (state === "failed" || state === "closed" || state === "disconnected") {
    delete peerConnections[peerUuid];
    let node = document.getElementById('remoteVideo_' + peerUuid);
    if (node && node.parentNode) {
      node.parentNode.removeChild(node);
    }
    for (var j = 0; j < players.length; j++) {
      if (peerUuid == players[j].socketId) {
        removeAvatar(players[j]);
        players.splice(j, j);
        break;
      }
    }
    updateLayout();
  }
}

function updateLayout() {
  // update CSS grid based on number of diplayed videos
  var rowHeight = '98vh';
  var colWidth = '98vw';

  var numVideos = Object.keys(peerConnections).length + 1; // add one to include local video

  if (numVideos > 1 && numVideos <= 4) { // 2x2 grid
    rowHeight = '48vh';
    colWidth = '48vw';
  } else if (numVideos > 4) { // 3x3 grid
    rowHeight = '32vh';
    colWidth = '32vw';
  }

  document.documentElement.style.setProperty(`--rowHeight`, rowHeight);
  document.documentElement.style.setProperty(`--colWidth`, colWidth);
}

function makeLabel(label) {
  var vidLabel = document.createElement('div');
  vidLabel.appendChild(document.createTextNode(label));
  vidLabel.setAttribute('class', 'videoLabel');
  return vidLabel;
}

function errorHandler(error) {
  console.log(error);
}