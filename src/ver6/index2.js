window.onload = function () {
    main();
}

var inputMap = {}; // 키보드 이벤트 

// 아바타 정보
var player_info = {
    position: new BABYLON.Vector3(3, 1.2, 35), // 위치
    //position: new BABYLON.Vector3(36, 3, -32), // 위치
    y: 0.077, // 아바타-캠 기본 높이
    alpha: Math.PI, // 시각
    speed: 0.018, // 속도
    name: "",
    avatarState: "avatar",
    player: null
}

// babylon 엔진 세팅
var canvas = document.getElementById("renderCanvas");
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
var css3DRenderer;
var css3DRenderer_yn=true;
var diffuseTexture;

function main() {
    scene.enablePhysics(); // 물리 엔진 활성화
    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0); 
    scene.debugLayer.show({ embedMode: true }); // babylon 디버그

    // 필수 Lights - 햇빛
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.6;
    light.specular = BABYLON.Color3.Black();

    // 그라운드 생성
    initGround();

    // 그라운드 이벤트 
//    initGroundEvent();

    // 내 아바타 생성
    initPlayer();

    // 아바타 무빙 이벤트 선언
    initKeyboardEvent();

    // 카메라 생성
    initCameraSetting();

    metakongTexture = new BABYLON.Texture("https://bitbucket.org/csidedev/metakong/raw/master/src/ver5/assets/img/metakong.png", scene);
    
    // 캠프 스테이지 메인 스크린 생성
    initMainScreen('');

    // 캠프 스테이지 버튼 생성
    initMainScreenBtn();

    // GUI - 화면 아래 안내문구
    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("GUI");
    var instructions = new BABYLON.GUI.TextBlock("instructions");
    instructions.text = "Move wasd keys, look with the mouse / Dance q / Sit c";
    instructions.color = "white";
    instructions.fontSize = 16;
    instructions.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
    instructions.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM
    advancedTexture.addControl(instructions);

    engine.runRenderLoop(function () {
        scene.render();
    });

}

// ----------------- init set -------------------
// 그라운드 생성
function initGround() {

    // Skybox 
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = BABYLON.Color3.Black()
    skyboxMaterial.specularColor = BABYLON.Color3.Black();
    skybox.material = skyboxMaterial;

    // Ground
    BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/csidedev/metakong/raw/master/src/ver6/assets/map/", "metakong_map.gltf", scene, function (newMeshes) {
        var ground = newMeshes[0];
        ground.id = "ground";
        ground.name = "ground";
        ground.scaling = new BABYLON.Vector3(-0.03, 0.03, -0.03);
        ground.rotation = new BABYLON.Vector3(0, Math.PI, 0);
        ground.checkCollisions = true;
    });

    // ----------------- 투명 객체 만들기 -------------------
    // 투명 설정
    const clearMaterial = new BABYLON.StandardMaterial("clearMaterial", scene);
    clearMaterial.alpha = 0;

    // 벽 바닥 
    const rise = 2;
    const diamInner = 112;
    const iWidth = diamInner * .15
    const diamOuter = diamInner + iWidth / 2

    const iFloor = BABYLON.MeshBuilder.CreateDisc("floor_", { radius: diamOuter / 2 - iWidth / 4 }, scene)
    const mFloor = new BABYLON.StandardMaterial("ifloor", scene)
    iFloor.rotation.x = Math.PI / 2;
    mFloor.material = clearMaterial;
    iFloor.material = clearMaterial;

    // 벽  
    const iOuter = BABYLON.MeshBuilder.CreateCylinder("iOuter", { diameter: diamOuter, height: rise }, scene)
    iOuter.position.y = rise / 2
    const iInner = BABYLON.MeshBuilder.CreateTube(
        "inner",
        {
            path: [new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, rise, 0)],
            radius: diamInner / 2,
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
        },
        scene
    )
    const outerCSG = BABYLON.CSG.FromMesh(iOuter)
    const innerCSG = BABYLON.CSG.FromMesh(iInner)
    const iRingCSG = outerCSG.subtract(innerCSG)
    const iRing = iRingCSG.toMesh("wall_", null, scene)
    iInner.dispose()
    iOuter.dispose()
    scene.removeMesh(iInner)
    scene.removeMesh(iOuter)
    scene.removeMesh(iRingCSG)

    iRing.checkCollisions = true;
    iRing.material = clearMaterial;

    // 중앙 화단 
    const cylinder = BABYLON.MeshBuilder.CreateCylinder("campfire_", { diameter: 34, height: 4 });
    cylinder.checkCollisions = true;
    cylinder.material = clearMaterial;

    // 나무 더미1
    const treebox1 = BABYLON.MeshBuilder.CreateBox("treebox1_", { width: 2, depth: 2.2, height: 4 });
    treebox1.checkCollisions = true;
    treebox1.position.set(-2.8, 0, 0);
    treebox1.rotation = new BABYLON.Vector3(0, -Math.PI / 8, 0);
    treebox1.material = clearMaterial;

}
// 그라운드 이벤트 생성
function initGroundEvent() {
    var npc1 = scene.getMeshByName("NPC SIZE.1");
    npc1.actionManager = new BABYLON.ActionManager(scene);
    npc1.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                console.log("ㅡㄹ릭");
            })
    );
}
// 내 아바타 생성
function initPlayer() {

    const rand = Math.random();
    const rand2 = Math.random();
    BABYLON.SceneLoader.ImportMesh("girl", "https://bitbucket.org/csidedev/metakong/raw/master/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons) => {
        
        player = meshes[0];
        player.scaling.scaleInPlace(0.5);
        //player.name = "mixamorig:Skin_" + socket.id;
        player.position = player_info.position;
        player.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);

        //skeleton.name = "mixamorig:Skin_" + socket.id;
        //player.skeleton = skeleton;
        //player.parent = scene.getMeshByName("YBot");
        //player.parent.name = "YBot_" + socket.id;

        player_info.player=player;

        // 아바타 애니메이션 설정
        
        var idleRange = scene.getAnimationGroupByName("stand");
        var walkRange = scene.getAnimationGroupByName("walk");
        var runRange = scene.getAnimationGroupByName("run");
        var sitRange = scene.getAnimationGroupByName("sit");
        var danceRange = scene.getAnimationGroupByName("dance");

        if (idleRange) idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);

        var animating = true;
        // 아바타 무빙
        scene.onBeforeRenderObservable.add(() => {
            player.position.y = player_info.y;

            var keydown = false;
            player_info.alpha = Math.PI - (camera.alpha - Math.PI / 2);
            if (inputMap["w"] && inputMap["a"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha - (Math.PI / 3), 0);
                keydown = true;
            } else if (inputMap["w"] && inputMap["d"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha + (Math.PI / 3), 0);
                keydown = true;
            } else if (inputMap["s"] && inputMap["a"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha - (Math.PI / 1.5), 0);
                keydown = true;
            } else if (inputMap["s"] && inputMap["d"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha + (Math.PI / 1.5), 0);
                keydown = true;
            } else if (inputMap["w"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha, 0);
                keydown = true;
            } else if (inputMap["s"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha - Math.PI, 0);
                keydown = true;
            } else if (inputMap["a"]) {
                player.rotation = new BABYLON.Vector3(0, Math.PI + player_info.alpha - (Math.PI / 2), 0);
                keydown = true;
            } else if (inputMap["d"]) {
                player.rotation = new BABYLON.Vector3(0,Math.PI +  player_info.alpha + (Math.PI / 2), 0);
                keydown = true;
            } else if (inputMap["q"] || inputMap["c"]) {
                keydown = true;
            }

            if (keydown) {
                if (!(inputMap["q"] || inputMap["c"])) {
                 player.moveWithCollisions(player.forward.scaleInPlace(player_info.speed));
                }
                // 소켓에 아바타 위치 전송
                socket.emit("positionUpdate", {
                    socketId: socket.id,
                    position: player.position,
                    rotation: player.rotation
                });
                if (!animating) {
                        animating = true;
                        if (inputMap["Shift"]) {
                            player_info.speed = 0.055;
                            runRange.start(true, 1.0, runRange.from, runRange.to, false);
                            //scene.beginAnimation(skeleton, runRange.from, runRange.to, true);
                            // 소켓에 아바타 애니메이션 전송- 뛰는중!
                            socket.emit("animationUpdate", {
                                socketId: socket.id,
                                action: "run"
                            });
                        } else if (inputMap["q"]) {
                            danceRange.start(true, 0.5, runRange.from, runRange.to, false);
                            socket.emit("animationUpdate", {
                                socketId: socket.id,
                                action: "dance"
                            });
                        } else if (inputMap["c"]) {
                            sitRange.start(true, 1.0, sitRange.from, sitRange.to, false);
                            socket.emit("animationUpdate", {
                                socketId: socket.id,
                                action: "dance"
                            });
                        } else {
                            walkRange.start(true, 1.0, walkRange.from, walkRange.to, false);
                            socket.emit("animationUpdate", {
                                socketId: socket.id,
                                  action: "walk"
                            });
                        }
                    player_info.position = player.position;
                }
            } else {
                player_info.speed = 0.018;
                
                if (animating) {
                    idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);
                    //scene.beginAnimation(skeleton, idleRange, idleRange, true);
                    sitRange.stop();
                    danceRange.stop();
                    runRange.stop();
                    walkRange.stop();

                    animating = false;
                    // 소켓에 아바타 애니메이션 전송- 쉬는중!
                    socket.emit("animationUpdate", {
                        socketId: socket.id,
                        action: "idle"
                    });
                }
            }
        });
        
    });
}

// 내 아바타 닉네임 태그 생성
function setNameLabel() {

    var name = document.getElementById('name').value;
    player_info.name=name;
    
    // 아바타 네임태그
    namePlane = BABYLON.MeshBuilder.CreatePlane('namePlane_' + socket.id, { width: 0.5, height: 0.5 }, scene);
    namePlane.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    
    nameText = new BABYLON.GUI.TextBlock('nameText');
    nameText.text = name;
    nameText.color = "yellow";
    nameText.fontSize = 60;
    
    advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlane);
    advancedTexture.addControl(nameText);
    advancedTexture.name = "nameText_" + socket.id;
    advancedTexture.renderScale = 0.3;
    namePlane.material.name = 'namePlane_' + socket.id;

    scene.registerBeforeRender(function () {
        namePlane.position.x = player_info.position.x;
        namePlane.position.y = 2;
        namePlane.position.z = player_info.position.z;
    });

    // 설정창 닫기
    $('.modal').hide();
    document.getElementById('setting').innerText = "설정";
}

// 멤버 아바타 생성
// 기존 접속 아바타 생성 + 새로 접속한 아바타 생성
// 네임태그도 함께 생성
function createPlayer(data) {
    console.log('createPlayer');

    if (data.avatarState == "avatar"){
        // 아바타 생성
        BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/csidedev/metakong/raw/master/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons) => {

            var playerA = meshes[0];
            playerA.userId = data.socketId;
            playerA.position = data.position;
            //playerA.checkCollisions = true;
            playerA.name = "mixamorig:Skin_" + data.socketId;

            var skeletonA = skeletons[0];
            skeletonA.name = "mixamorig:Skin_" + data.socketId;

            playerA.skeleton = skeletonA;
            playerA.parent = scene.getMeshByName("YBot");
            playerA.parent.name = "YBot_" + data.socketId;


            // 아바타 애니메이션 설정
            skeletonA.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
            skeletonA.animationPropertiesOverride.enableBlending = true;
            skeletonA.animationPropertiesOverride.blendingSpeed = 1;
            skeletonA.animationPropertiesOverride.loopMode = 1;

            // 아바타 애니메이션 설정
            var idleRange = skeletonA.getAnimationRange("YBot_Idle");
            scene.beginAnimation(skeletonA, idleRange.from, idleRange.to, true);

            // 아바타 네임태그
            namePlaneA = BABYLON.MeshBuilder.CreatePlane('namePlane_' + data.socketId, { width: 0.5, height: 0.5 }, scene);
            namePlaneA.position = new BABYLON.Vector3(playerA.position._x, 2, playerA.position._z);
            namePlaneA.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

            nameText = new BABYLON.GUI.TextBlock('nameText');
            nameText.text = data.userName;
            nameText.color = "white";
            nameText.fontSize = 60;
            
            advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlaneA);
            advancedTexture.name = "nameText_" + data.socketId;        
            advancedTexture.renderScale = 0.3;
            advancedTexture.addControl(nameText);
            namePlaneA.material.name = "namePlane_" + data.socketId;

            players.push({
                socketId: data.socketId,
                streamId: data.streamId,
                player: playerA,
                nameTag: namePlaneA,
                avatarState : data.avatarState
            });
        });
    } else {
        
        // 아바타 웹캠
        if (! document.getElementById('remoteVideo_' + data.socketId)){
            var vidElement = document.createElement('video');
            vidElement.setAttribute('autoplay', '');
            vidElement.setAttribute('id', 'remoteVideo_' + data.socketId);
            vidElement.setAttribute('width', 345);
            vidElement.setAttribute('height', 200);
            vidElement.src = "https://bitbucket.org/csidedev/metakong/raw/master/src/ver6/assets/img/metakong.mp4";

            document.getElementById('remotePlayerVideos').appendChild(vidElement);
        }

        video = document.getElementById('remoteVideo_' + data.socketId);

        videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + data.socketId, video, scene, true, true);
        playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + data.socketId, scene);

        console.log(videoTexture);

        // 원형 plane 생성
        player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + data.socketId, {});
        player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
        player_disc.position = data.position;
        player_disc.position._y = 1.4;

        // plane에 캠 영상 적용
        playerCamMat.backFaceCulling = false;
        playerCamMat.diffuseTexture = videoTexture;
        playerCamMat.emissiveColor = BABYLON.Color3.White();
        player_disc.material = playerCamMat;

        // 아바타 네임태그
        namePlaneA = BABYLON.MeshBuilder.CreatePlane('namePlane_' + data.socketId, { width: 0.5, height: 0.5 }, scene);
        namePlaneA.position = new BABYLON.Vector3(player_disc.position._x, 2, player_disc.position._z);
        namePlaneA.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

        nameText = new BABYLON.GUI.TextBlock('nameText');
        nameText.text = data.userName;
        nameText.color = "white";
        nameText.fontSize = 60;
        
        advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlaneA);
        advancedTexture.name = "nameText_" + data.socketId;        
        advancedTexture.renderScale = 0.3;
        advancedTexture.addControl(nameText);
        namePlaneA.material.name = "namePlane_" + data.socketId;

        players.push({
            socketId: data.socketId,
            streamId: data.streamId,
            player: player_disc,
            nameTag: namePlaneA,
            avatarState : data.avatarState
        });
    }
}

// 카메라 생성
function initCameraSetting() {

    var alpha = player_info.position.y + Math.PI / 8;
    var beta = Math.PI;
    var target = new BABYLON.Vector3(player_info.position.x, player_info.position.y + 1.5, player_info.position.z);

    camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", alpha, beta, 5, target, scene);
    camera.checkCollisions = true;
    camera.wheelPrecision = 15;
    camera.lowerRadiusLimit = 2;
    camera.upperRadiusLimit = 80;
    camera.lowerBetaLimit = -0.1;
    camera.upperBetaLimit = (Math.PI / 2) * 0.95;
    camera.wheelDeltaPercentage = 0.01;
    camera.inputs.remove(camera.inputs.attached.keyboard);

    scene.activeCamera = camera;
    scene.activeCamera.attachControl(canvas, true);
    scene.registerBeforeRender(function () {
        // 카메라가 아바타 따라다니도록 세팅
        camera.target.copyFrom(player_info.position);
        camera.target.y = 1.5;
    });
}

// 메인 스크린 생성
function initMainScreen(screenType) {
    $('.button-webcam').show();
    var planeOpts = {
        height: 4.2,
        width: 8.2,
        sideOrientation: BABYLON.Mesh.DOUBLESIDE
    };
    
    var ANote0Video = new BABYLON.MeshBuilder.CreatePlane("mainScreen", planeOpts, scene);
    ANote0Video.position = new BABYLON.Vector3(36.07, 3.15, -31.961);
    ANote0Video.rotation.y = ANote0Video.rotation.y + 2.88;
    ANote0Video.actionManager = new BABYLON.ActionManager(scene);
    ANote0Video.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                fullScreen();
            })
    );
    var mat = new BABYLON.StandardMaterial("mainScreenMat", scene);
    mat.emissiveColor = BABYLON.Color3.White();
    mat.diffuseTexture = metakongTexture;
    mat.emissiveColor = BABYLON.Color3.White();
    ANote0Video.material = mat;

    if(screenType == 'screen' || screenType == 'webCam'){
        mat.diffuseTexture.vScale =-1;   
        ANote0Video.scaling= new BABYLON.Vector3(1, -1, 1);
    } else {
        mat.diffuseTexture.vScale =1;   
        ANote0Video.scaling= new BABYLON.Vector3(1, 1, 1);
    }
}    
// 메인 스크린 버튼 생성
function initMainScreenBtn() {
   
    // 메인 스크린 발표자 라벨 생성
    streamerPlane = BABYLON.MeshBuilder.CreatePlane('mainScreenStreamer', { height: 2,width: 5 }, scene);
    streamerPlane.position = new BABYLON.Vector3(36.07,5, -31.941);
    streamerPlane.rotation.y = streamerPlane.rotation.y + 2.9;
    
    streamerText = new BABYLON.GUI.TextBlock('streamerText');
    streamerTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(streamerPlane);
    streamerTexture.name = "mainScreenStreamer";
    streamerTexture.renderScale = 0.4;
    streamerPlane.material.name = 'mainScreenStreamer';
    
    var label = new BABYLON.GUI.Rectangle("mainScreenLabel");
    label.background = "white"
    label.height = "80px";
    label.width = "200px";
    label.alpha = 0.8;
    label.cornerRadius = 20;
    label.thickness = 1;

    streamerTexture.addControl(label);
    label.addControl(streamerText);

    // 캠프 스테이지 스크린 공유 버튼
    mainScreenBtnMat = new BABYLON.StandardMaterial('mainScreenBtn', scene);
    mainScreenBtnMat.diffuseTexture = metakongTexture;
    mainScreenBtnMat.emissiveColor = BABYLON.Color3.White();
    mainScreenBtn = BABYLON.MeshBuilder.CreateDisc("mainScreenBtn", {radius: 0.4 });
    mainScreenBtn.position = new BABYLON.Vector3(31.5, 2.1, -33.5);  
    mainScreenBtn.rotation.z = mainScreenBtn.rotation.z - 3.14;
    mainScreenBtn.rotation.y = mainScreenBtn.rotation.y - 0.22;
    mainScreenBtn.scaling= new BABYLON.Vector3(1, 1, -1);
    mainScreenBtn.material = mainScreenBtnMat;
    mainScreenBtn.actionManager = new BABYLON.ActionManager(scene);
    mainScreenBtn.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                // 메인 스크린 선택- 설청 모달 띄움
                document.getElementById('setting').innerText="닫기";
                $('.name-box').hide();
                //$('.button-video').show();
                $('.url-box').show();
                $('.modal').show();
                $('.video-select').hide();
            })
    );
} 
// ----------------- init set end -------------------

// ----------------- event set ------------------
// 아바타 키보드 이벤트
function initKeyboardEvent() {

    var keysLeft = [37, 65]; // "ArrowLeft", "A", "a", "ㅁ"
    var keysRight = [39, 68]; // "ArrowRight", "D", "d", "ㅇ"
    var keysForwards = [38, 87]; // "ArrowUp", "W", "w", "ㅉ", "ㅈ"
    var keysBackwards = [40, 83]; // "ArrowDown", "S", "s", "ㄴ"
    var keysSpeedModifier = [16]; // "Shift"
    var keysSamba = [66]; // "b"
    var keysDance = [81]; // "q"
    var keysSit = [67]; // "c"

    scene.actionManager = new BABYLON.ActionManager(scene);
    scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
        var t = evt.sourceEvent.keyCode;
        if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t) || -1 !== keysDance.indexOf(t) || -1 !== keysSit.indexOf(t)) {
            var key;
            -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysDance.indexOf(t) ? key = "q" : -1 !== keysSit.indexOf(t) ? key = "c" : -1 !== keysSamba.indexOf(t) && (key = "b");
            inputMap[key] = evt.sourceEvent.type == "keydown";
        }
    }));
    scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {
        var t = evt.sourceEvent.keyCode;
        if (-1 !== keysLeft.indexOf(t) || -1 !== keysRight.indexOf(t) || -1 !== keysForwards.indexOf(t) || -1 !== keysBackwards.indexOf(t) || -1 !== keysSpeedModifier.indexOf(t) || -1 !== keysSamba.indexOf(t) || -1 !== keysDance.indexOf(t) || -1 !== keysSit.indexOf(t)) {
            var key;
            -1 !== keysLeft.indexOf(t) ? key = "a" : -1 !== keysRight.indexOf(t) ? key = "d" : -1 !== keysForwards.indexOf(t) ? key = "w" : -1 !== keysBackwards.indexOf(t) ? key = "s" : -1 !== keysSpeedModifier.indexOf(t) ? key = "Shift" : -1 !== keysDance.indexOf(t) ? key = "q" : -1 !== keysSit.indexOf(t) ? key = "c" : -1 !== keysSamba.indexOf(t) && (key = "b");
            inputMap[key] = evt.sourceEvent.type == "keydown";
        }
    }));
}

// 내 닉네임 변경
function updateNameLabel() {

    // 기존 닉네임 삭제
    scene.removeMesh(scene.getMeshByName("namePlane_" + socket.id));
    scene.getMaterialByName("namePlane_" + socket.id).dispose();
    scene.getTextureByName("nameText_" + socket.id).dispose();

    // 닉네임 재설정
    setNameLabel();

    // 소켓에 전달
    var name = document.getElementById('name').value;
    socket.emit("userNameUpdate", {
        socketId: socket.id,
        userName: name
    });
}

// 다른 아바타 닉네임 변경
function updatePlayerNameLabel(index, name) {
    let player = players[index];

    // 기존 닉네임 삭제
    scene.removeMesh(scene.getMeshByName("namePlane_" + player.socketId));
    scene.getMaterialByName("namePlane_" + player.socketId).dispose();
    scene.getTextureByName("nameText_" + player.socketId).dispose();

    // 네임태그 생성
    namePlaneA = BABYLON.MeshBuilder.CreatePlane('namePlane_' + player.socketId, { width: 0.5, height: 0.5 }, scene);
    namePlaneA.position = new BABYLON.Vector3(player.player.position._x, 2, player.player.position._z);
    namePlaneA.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

    nameText = new BABYLON.GUI.TextBlock('nameText');
    nameText.text = name;
    nameText.color = "white";
    nameText.fontSize = 60;
    
    advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlaneA);
    advancedTexture.name = "nameText_" + player.socketId;
    advancedTexture.renderScale = 0.3;
    advancedTexture.addControl(nameText);
    namePlaneA.material.name = "namePlane_" + player.socketId;

    players[index].nameTag = namePlaneA;
}

// 메인 스크린 스트리밍 이벤트
function videoStream(type) {

    if(type == "url"){
        // 영상 url 공유
        var url = document.getElementById('url').value;
        if(url == ""){
            alert("URL 입력하시길");
        } else {
            addVideoStream("url",url, player_info.name);
            $('.button-stop').css('display','block');
            socket.emit("streamURL", { socketId: socket.id, streamer : player_info.name, url: url });
            $('#stream_state').val(type)
        }
    } else if(type == "webCam"){
        addVideoStream("webCam",localStream, player_info.name);
        $('.button-stop').css('display','block');
        socket.emit("streamWebcam", { socketId: socket.id, streamer : player_info.name });
        $('#stream_state').val(type)
    } else if(type == "screen"){
        addVideoStream("screen",localStream, player_info.name);
        $('.button-stop').css('display','block');
        socket.emit("streamScreen", { socketId: socket.id, streamer : player_info.name });
        $('#stream_state').val(type)
    }
    // 설정창 닫기
    $('.modal').hide(); 
    document.getElementById('setting').innerText="설정";
}

// 아바타-캠 스위치 액션
function userAvatarUpdate(i, data) {
    userPlayer = null;
    if(i == null){
        userPlayer =  player_info.player
    } else {
        userPlayer =  players[i].player
    } 
        
    if (data.state == "avatar") {
        
        // 아바타 웹캠
        video = document.getElementById('localVideo');
        if (data.socketId != socket.id){
            video = document.getElementById('remoteVideo_' + data.socketId);
        }
        videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + data.socketId, video, scene, true, true);
        playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + data.socketId, scene);

        // 아바타 삭제
        scene.removeMesh(scene.getMeshByName("YBot_" + data.socketId));
        scene.removeMesh(scene.getMeshByName("mixamorig:Skin_" + data.socketId));
        userPlayer.subMeshes[0].getMaterial().dispose();
        userPlayer.subMeshes[1].getMaterial().dispose();
        userPlayer.material.dispose();
        userPlayer.skeleton.dispose();

        // 원형 plane 생성
        player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + data.socketId, {});
        player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
        player_disc.position = userPlayer.position;
        player_disc.position._y = 1.4;

        // plane에 캠 영상 적용
        playerCamMat.backFaceCulling = false;
        playerCamMat.diffuseTexture = videoTexture;
        playerCamMat.emissiveColor = BABYLON.Color3.White();
        player_disc.material = playerCamMat;

        if (data.socketId == socket.id){
            player_info.player = player_disc ; 
            player_info.y = 1.4;
            player_info.avatarState = "webcam";
        } else {
            players[i].player = player_disc ; 
            players[i].avatarState = "webcam" ; 
        }
    } else {
        // 캠 plane 삭제
        scene.removeMesh(scene.getMeshByName("webCam_" + data.socketId));
        scene.getMaterialByName("playerCamMat_" + data.socketId).dispose();
        scene.getTextureByName("playerCamVideo_" + data.socketId).dispose();

        // 아바타 생성
        BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/csidedev/metakong/raw/master/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons) => {
            var playerA = meshes[0];
            playerA.name = "mixamorig:Skin_" + data.socketId;
            playerA.userId = data.socketId;
            playerA.position = userPlayer.position;
            playerA.position._y = 0.077;
            playerA.rotation = new BABYLON.Vector3(0, player_info.alpha - Math.PI, 0);
            //playerA.checkCollisions = true;

            var skeletonA = skeletons[0];
            skeletonA.name = "mixamorig:Skin_" + data.socketId;
            playerA.skeleton = skeletonA;
            playerA.parent = scene.getMeshByName("YBot");
            playerA.parent.name = "YBot_" + data.socketId;
            
            // 아바타 애니메이션 설정
            skeletonA.animationPropertiesOverride = new BABYLON.AnimationPropertiesOverride();
            skeletonA.animationPropertiesOverride.enableBlending = true;
            skeletonA.animationPropertiesOverride.blendingSpeed = 1;
            skeletonA.animationPropertiesOverride.loopMode = 1;

            var idleRange = skeletonA.getAnimationRange("YBot_Idle");
            scene.beginAnimation(skeletonA, idleRange.from, idleRange.to, true);

            if (data.socketId == socket.id){
                player_info.player = playerA ;
                player = playerA ;
                skeleton = skeletonA ;
                player_info.y = 0.077;
                player_info.avatarState = "avatar";
            } else {
                players[i].player = playerA
                players[i].player.skeleton = skeletonA
                players[i].avatarState = "avatar" ; 
            }
        });
    }
}

// 멤버 접속 종료 이벤트
function removeAvatar(rmv_player) {
    if(rmv_player.avatarState == "avatar"){
        // 아바타 삭제
        try{
            scene.getTextureByName("nameText_" + rmv_player.socketId).dispose();
            scene.getMaterialByName("namePlane_" + rmv_player.socketId).dispose();
            scene.removeMesh(scene.getMeshByName("namePlane_" + rmv_player.socketId));
            scene.removeMesh(scene.getMeshByName("YBot_" + rmv_player.socketId));
            scene.removeMesh(scene.getMeshByName("mixamorig:Skin_" + rmv_player.socketId));
            rmv_player.player.subMeshes[0].getMaterial().dispose();
            rmv_player.player.subMeshes[1].getMaterial().dispose();
            rmv_player.player.material.dispose();
            rmv_player.player.skeleton.dispose();
        } catch{
            
        }
    } else {
        try{
            scene.getTextureByName("nameText_" + rmv_player.socketId).dispose();
            scene.getMaterialByName("namePlane_" + rmv_player.socketId).dispose();
            scene.removeMesh(scene.getMeshByName("namePlane_" + rmv_player.socketId));
            // 캠 plane 삭제
            scene.removeMesh(scene.getMeshByName("webCam_" + rmv_player.socketId));
            scene.getMaterialByName("playerCamMat_" + rmv_player.socketId).dispose();
            scene.getTextureByName("playerCamVideo_" + rmv_player.socketId).dispose();
        } catch{
            
        }
    }
}

// ----------------- event set end-------------------
// -------------- 영상 공유 func start----------------

// 영상 공유
function addVideoStream(type, stream, streamer) {
    removeVideoStream();
    $('#stream_state').val(type);
    initMainScreen(type);

    if(type == "webCam"){
        const video =  document.getElementById("remoteVideo");
        video.srcObject = stream;
        setTimeout(function() {
            mainScreenStream(video);
        },1000);
    } else if (type == "screen"){
        $('.button-webcam').hide();
        const video =  document.getElementById("remoteVideo");
        video.srcObject = stream;
        setTimeout(function() {
            mainScreenStream(video);
        },1000);
    } else if (type == "url"){
        plane = scene.getMeshByName("mainScreen");

        css3DRenderer = setupRenderer(plane, stream, scene);
        scene.onBeforeRenderObservable.add(() => {
            css3DRenderer.render(scene, scene.activeCamera)
        })
    }
    streamerText.text = streamer;
}

function mainScreenStream(video) {
    videoTexture = new BABYLON.VideoTexture('mainScreenVideo', video, scene, true, true);
    videoTexture.emissiveColor = new BABYLON.Color3.White()
    plane = scene.getMeshByName("mainScreen");

    mat = scene.getMaterialByName("mainScreenMat");
    mat.diffuseTexture = videoTexture; 
    plane.material = mat;
    //mat.diffuseTexture.vScale =-1;  
}

function removeDomNode(id) {
    let node = document.getElementById(id);
    if (node && node.parentNode) {
        node.parentNode.removeChild(node);
    }
}
let videoWidth = 800
let videoHeight = 480
let tvThickness = .2

// 영상 공유 정지
function stopStreaming(){
    removeVideoStream();
    initMainScreen('');
}
function removeVideoStream() {
    try{
        scene.removeMesh(scene.getMeshByName("mainScreen"));
        scene.getMaterialByName("mainScreenMat").dispose();
        type = $('#stream_state').val();
        if (type == "screen"){
            scene.getTextureByName("mainScreenVideo").dispose();

            localStream.getTracks().forEach(track => track.stop())
            localStream = screanStream;
            //localStream.getTracks().forEach(track => track.start())
            msg = {
                'socketId': socket.id,
                'streamId': localStream.id,
                'displayName': '콩콩쓰', 
                'dest': 'all'
            }
            sendMessage(msg);
        } else if(type == "webCam"){
            scene.getTextureByName("mainScreenVideo").dispose();
        } else if(type == "url"){
            // 기존 스트리밍 삭제
            removeDomNode("iframeContainer")
            removeDomNode("iframe-video")
            removeDomNode("cssContainer")
            removeDomNode("CSS3DRendererDom")
            scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
        }
        socket.emit("stopStreaming", socket.id);

        streamerText.text = '';
        $('.modal').css('display','none');
        document.getElementById('setting').innerText="설정";
        $('.button-stop').css('display','none');
        $('#stream_state').val('stop');

    } catch{
        
    }
}
// 메인 스크린에 URL 영상 스트리밍
function setupRenderer(videoViewMesh, videoURL, scene) {

    let webGLContainer = document.getElementById('canvasZone')

    // 컨테이너 생성
    let css3DContainer = document.createElement('div')
    css3DContainer.id = 'cssContainer'
    css3DContainer.style.position = 'absolute'
    css3DContainer.style.width = '100%'
    css3DContainer.style.height = '100%'
    css3DContainer.style.zIndex = '-1'
    webGLContainer.insertBefore(css3DContainer, webGLContainer.firstChild)

    let css3DRenderer = new CSS3DRenderer()
    console.log(css3DRenderer.domElement);
    css3DContainer.appendChild(css3DRenderer.domElement)
    css3DRenderer.setSize(webGLContainer.offsetWidth, webGLContainer.offsetHeight)

    // 영상 실행할 iFrame 컨테이너 생성
    var iframeContainer = document.createElement('div')
    iframeContainer.style.width = videoWidth + 'px'
    iframeContainer.style.height = videoHeight + 'px'
    iframeContainer.style.backgroundColor = '#000'
    iframeContainer.id = "iframeContainer"

    // CSS Object 생성
    var CSSobject = new CSS3DObject(iframeContainer, scene)
    CSSobject.position.copyFrom(videoViewMesh.getAbsolutePosition())
    CSSobject.rotation.y = -videoViewMesh.rotation.y
    CSSobject.scaling.copyFrom(videoViewMesh.scaling)
    CSSobject.name ="mainScreenUrl";

    // iFrame 생성 
    var iframe = document.createElement('iframe')
    iframe.id = 'iframe-video'
    iframe.style.width = videoWidth + 'px'
    iframe.style.height = videoHeight + 'px'
    iframe.style.border = '0px'
    iframe.allow = 'autoplay'
    iframe.src = [videoURL.replace('watch?v=','embed/').replace('youtu.be/','www.youtube.com/embed/'), null, '?rel=0&enablejsapi=1&disablekb=1&autoplay=1&controls=0&fs=0&modestbranding=1'].join('')
    iframeContainer.appendChild(iframe)

    // material 생성
    let depthMask = new BABYLON.StandardMaterial('VideoViewMaterial', scene)
    depthMask.backFaceCulling = true
    videoViewMesh.material = depthMask

    // Render Video the mesh
    videoViewMesh.onBeforeRenderObservable.add(() => engine.setColorWrite(false))
    videoViewMesh.onAfterRenderObservable.add(() => engine.setColorWrite(true))

    // swap meshes to put mask first
    var videoPlaneIndex = scene.meshes.indexOf(videoViewMesh)
    scene.meshes[videoPlaneIndex] = scene.meshes[0]
    scene.meshes[0] = videoViewMesh

    return css3DRenderer
}

class CSS3DObject extends BABYLON.Mesh {
    constructor(element, scene) {
        super()
        this.element = element
		this.element.style.position = 'absolute'
		this.element.style.pointerEvents = 'auto'
    }
}

class CSS3DRenderer {
    constructor() {
		var matrix = new BABYLON.Matrix()

		this.cache = {
			camera: { fov: 0, style: '' },
			objects: new WeakMap()
		}

		var domElement = document.createElement( 'div' )
		domElement.style.overflow = 'hidden'

		this.domElement = domElement
		this.cameraElement = document.createElement( 'div' )
		this.isIE = (!!document['documentMode'] || /Edge/.test(navigator.userAgent) || /Edg/.test(navigator.userAgent))

		if (!this.isIE) {
			this.cameraElement.style.webkitTransformStyle = 'preserve-3d'
			this.cameraElement.style.transformStyle = 'preserve-3d'
		}
		this.cameraElement.style.pointerEvents = 'none'
		domElement.appendChild(this.cameraElement)
    }

    getSize() {
		return {
			width: this.width,
			height: this.height
		}
    }

	setSize(width, height) {
		this.width = width
		this.height = height
		this.widthHalf = this.width / 2
		this.heightHalf = this.height / 2

		this.domElement.style.width = width + 'px'
		this.domElement.style.height = height + 'px'

		this.cameraElement.style.width = width + 'px'
		this.cameraElement.style.height = height + 'px'
	}    

	epsilon(value) {
		return Math.abs(value) < 1e-10 ? 0 : value
	}

	getCameraCSSMatrix(matrix) {
		var elements = matrix.m

		return 'matrix3d(' +
			this.epsilon( elements[ 0 ] ) + ',' +
			this.epsilon( - elements[ 1 ] ) + ',' +
			this.epsilon( elements[ 2 ] ) + ',' +
			this.epsilon( elements[ 3 ] ) + ',' +
			this.epsilon( elements[ 4 ] ) + ',' +
			this.epsilon( - elements[ 5 ] ) + ',' +
			this.epsilon( elements[ 6 ] ) + ',' +
			this.epsilon( elements[ 7 ] ) + ',' +
			this.epsilon( elements[ 8 ] ) + ',' +
			this.epsilon( - elements[ 9 ] ) + ',' +
			this.epsilon( elements[ 10 ] ) + ',' +
			this.epsilon( elements[ 11 ] ) + ',' +
			this.epsilon( elements[ 12 ] ) + ',' +
			this.epsilon( - elements[ 13 ] ) + ',' +
			this.epsilon( elements[ 14 ] ) + ',' +
			this.epsilon( elements[ 15 ] ) +
		')'
	}    

	getObjectCSSMatrix(matrix, cameraCSSMatrix) {
		var elements = matrix.m;
		var matrix3d = 'matrix3d(' +
			this.epsilon( elements[ 0 ] ) + ',' +
			this.epsilon( elements[ 1 ] ) + ',' +
			this.epsilon( elements[ 2 ] ) + ',' +
			this.epsilon( elements[ 3 ] ) + ',' +
			this.epsilon( - elements[ 4 ] ) + ',' +
			this.epsilon( - elements[ 5 ] ) + ',' +
			this.epsilon( - elements[ 6 ] ) + ',' +
			this.epsilon( - elements[ 7 ] ) + ',' +
			this.epsilon( elements[ 8 ] ) + ',' +
			this.epsilon( elements[ 9 ] ) + ',' +
			this.epsilon( elements[ 10 ] ) + ',' +
			this.epsilon( elements[ 11 ] ) + ',' +
			this.epsilon( elements[ 12 ] ) + ',' +
			this.epsilon( elements[ 13 ] ) + ',' +
			this.epsilon( elements[ 14 ] ) + ',' +
			this.epsilon( elements[ 15 ] ) +
		')'

		if (this.isIE) {
			return 'translate(-50%,-50%)' +
				'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)' +
				cameraCSSMatrix +
				matrix3d;
		}
		return 'translate(-50%,-50%)' + matrix3d
	}    

	renderObject(object, scene, camera, cameraCSSMatrix ) {
        if (object instanceof CSS3DObject) {
            var style
			var objectMatrixWorld = object.getWorldMatrix().clone()
			var camMatrix = camera.getWorldMatrix()
			var innerMatrix = objectMatrixWorld.m

			// Set scaling
			const youtubeVideoWidth = 1.6
			const youtubeVideoHeight = 1.2

			innerMatrix[0] *= 0.01 / youtubeVideoWidth
			innerMatrix[2] *= 0.01 / youtubeVideoWidth
			innerMatrix[5] *= 0.01 / youtubeVideoHeight

			// Set position from camera
			innerMatrix[12] = -camMatrix.m[12] + object.position.x
			innerMatrix[13] = -camMatrix.m[13] + object.position.y
			innerMatrix[14] = camMatrix.m[14] - object.position.z
			innerMatrix[15] = camMatrix.m[15] * 0.00001

			objectMatrixWorld = BABYLON.Matrix.FromArray(innerMatrix)
            objectMatrixWorld = objectMatrixWorld.scale(100)
			style = this.getObjectCSSMatrix( objectMatrixWorld, cameraCSSMatrix)
            var element = object.element
            var cachedObject = this.cache.objects.get( object )

            if ( cachedObject === undefined || cachedObject.style !== style ) {

                element.style.webkitTransform = style
                element.style.transform = style

                var objectData = { style: style }

                this.cache.objects.set( object, objectData )
            }
            if ( element.parentNode !== this.cameraElement ) {
                this.cameraElement.appendChild( element )
            }

        } else if ( object instanceof BABYLON.Scene ) {
            for ( var i = 0, l = object.meshes.length; i < l; i ++ ) {
                this.renderObject( object.meshes[ i ], scene, camera, cameraCSSMatrix )
            }
        }
	}    

	render(scene, camera) {
        var projectionMatrix = camera.getProjectionMatrix()
		var fov = projectionMatrix.m[5] * this.heightHalf

		if (this.cache.camera.fov !== fov) {

			if (camera.mode == BABYLON.Camera.PERSPECTIVE_CAMERA ) {
				this.domElement.style.webkitPerspective = fov + 'px'
				this.domElement.style.perspective = fov + 'px'
			} else {
				this.domElement.style.webkitPerspective = ''
				this.domElement.style.perspective = ''
			}
			this.cache.camera.fov = fov
		}

		if ( camera.parent === null ) camera.computeWorldMatrix()

		var matrixWorld = camera.getWorldMatrix().clone()
		var rotation = matrixWorld.clone().getRotationMatrix().transpose()
		var innerMatrix = matrixWorld.m

		innerMatrix[1] = rotation.m[1]
		innerMatrix[2] = -rotation.m[2]
		innerMatrix[4] = -rotation.m[4]
		innerMatrix[6] = -rotation.m[6]
		innerMatrix[8] = -rotation.m[8]
		innerMatrix[9] = -rotation.m[9]

		matrixWorld = BABYLON.Matrix.FromArray(innerMatrix)

		var cameraCSSMatrix = 'translateZ(' + fov + 'px)' + this.getCameraCSSMatrix( matrixWorld )

		var style = cameraCSSMatrix + 'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)'

		if (this.cache.camera.style !== style && !this.isIE ) {
			this.cameraElement.style.webkitTransform = style
			this.cameraElement.style.transform = style
			this.cache.camera.style = style
		}

        this.renderObject(scene, scene, camera, cameraCSSMatrix )
	}  
}
// -------------- 영상 공유 func end----------------


/**
 * 
 * The service worker navigation preload request was cancelled before 'preloadResponse' settled. If you intend to use 'preloadResponse', use waitUntil() or respondWith() to wait for the promise to settle.
 */