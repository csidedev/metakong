var inputMap = {}; // 키보드 이벤트 
let eagleView = false; // 카메라 이글뷰 여부
let eagleCamera; // 이글뷰용 카메라 인스턴스
let cameraChanging = false; // 카메라 변경중

// 아바타 정보
var player_info = {
    position: new BABYLON.Vector3(-8.8, 1.2, 21.67), // 기본 생성 위치
    //position: new BABYLON.Vector3(36, 1.2, -32), // 캠핑장 스크린 위치
    y: 0.077, // 아바타-캠 기본 높이
    alpha: Math.PI * Math.random(), // 시각
    speed: 0.05, // 속도
    name: "",
    nameTag: null,
    avatarState: "avatar",
    player: null,
    player_cam: null,
}

// 맵
var ground;

// babylon 엔진 세팅
var canvas = document.getElementById("renderCanvas");
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
var css3DRenderer;
var css3DRenderer_yn = true;
var diffuseTexture;

$(document).ready(function () {

    window.addEventListener("resize", function () {
        engine.resize();
    });
    window.onblur = function () {
        inputMap = {};
    }

    var keysLeft = [65]; // "ArrowLeft", "A", "a", "ㅁ"
    var keysRight = [68]; // "ArrowRight", "D", "d", "ㅇ"
    var keysForwards = [87]; // "ArrowUp", "W", "w", "ㅉ", "ㅈ"
    var keysBackwards = [83]; // "ArrowDown", "S", "s", "ㄴ"
    var keysSpeedModifier = [16]; // "Shift"
    var keysSamba = [66]; // "b"
    var keysDance = [81]; // "q"
    var keysSit = [67]; // "c"

    const openIglooSwap = document.getElementById("openIglooSwap");
    const iglooSwap = document.getElementById("iglooSwap");
    const btnCloseIgloo = document.getElementById("btnCloseIgloo");
    const btnQuest = document.getElementById("quest");
    const btnShop = document.getElementById("shop");
    const btnGovernance = document.getElementById("governance");
    const btnNews = document.getElementById("news");
    const btnRecruit = document.getElementById("recruit");
    const btnUpdateNick = document.getElementById("btnUpdateNick");
    const confirmUpdateNick = document.getElementById("confirmUpdateNick");
    let iglooTabs = document.querySelectorAll("#iglooSwap .tab-btn");
    let iglooContent = document.querySelectorAll("#iglooSwap .tab-content");

    for (i = 0; i < iglooTabs.length; i++) {
        iglooTabs[i].addEventListener("click", function (e) {
            var num = Array.from(iglooTabs).indexOf(e.currentTarget);
            for (j = 0; j < iglooContent.length; j++) {
                iglooContent[j].style.display = "none";
            }
            iglooContent[num].style.display = "block";
        })
    }

    // 메타콩소개
    btnNews.addEventListener("click", function () {
        window.open("https://fascinated-carol-305.notion.site/f43263b387ee46e2b39e4653b037cf31");
    })

    // 채용
    btnRecruit.addEventListener("click", function () {
        window.open("https://fascinated-carol-305.notion.site/973e1546f43d46988d2aadb28b730900");
    })

    // 이글루스왑 메뉴 열기,닫기
    openIglooSwap.addEventListener("click", function () {
        $("#dimLayer").show();
        iglooSwap.style.display = "block";
    })
    btnCloseIgloo.addEventListener("click", function () {
        $("#dimLayer").hide();
        iglooSwap.style.display = "none";
    })

    // 퀘스트
    btnQuest.addEventListener("click", function () {
        $("#dimLayer").show();
        $("#questBox").show();
    })

    // 퀘스트 수락,창닫기
    $("#btnCloseQuest, #btnAgree").on("click", function () {
        $("#dimLayer").hide();
        $("#questBox").hide();
    })

    // 준비중 메뉴
    btnShop.addEventListener("click", function () {
        $("#dimLayer").show();
        $("#nftShop").show();
    })

    // 준비중 메뉴
    btnGovernance.addEventListener("click", function () {
        $("#dimLayer").show();
        $("#governanceModal").show();
    })

    $("#btnCloseShop").on("click", function () {
        $("#dimLayer").hide();
        $("#nftShop").hide();
    })

    $("#btnCloseGovernance").on("click", function () {
        $("#dimLayer").hide();
        $("#governanceModal").hide();
    })

    $("#btnCloseKeyGuideModal").on("click", function () {
        $("#dimLayer").hide();
        $("#keyGuideModal").hide();
    })

    // npc 대화창
    $(".btn-next-page").on("click", function () {
        $(this).parent().hide();
        $(this).parent().siblings(".page2").show();
    })

    // 닉네임 업데이트
    // input 특수문자,공백 방지
    $("#name").on("keyup keydown", function () {
        var nickVal = $(this).val();
        var regExp = /[ \{\}\[\]\/?.,;:|\)*~`!^\-_+┼<>@\#$%&\'\"\\\(\=]/gi;
        if (regExp.test(nickVal)) {
            $(this).focus();
            $("#alert").show();
            $("#name").addClass("invalid");
            $("#btnUpdateNick, #btnUpdateNickEN").removeClass("on");
            $("#btnUpdateNick, #btnUpdateNickEN").attr("disabled", "disabled");
            $("#btnCompleteSet").hide();
        } else if (nickVal === "" || 1 >= nickVal.length) {
            $("#alert").show();
            $("#name").addClass("invalid");
            $("#btnUpdateNick, #btnUpdateNickEN").removeClass("on");
            $("#btnUpdateNick, #btnUpdateNickEN").attr("disabled", "disabled");
            $("#btnCompleteSet").hide();
        } else {
            $("#alert").hide();
            $("#name").removeClass("invalid");
            $("#btnUpdateNick, #btnUpdateNickEN").addClass("on");
            $("#btnUpdateNick, #btnUpdateNickEN").removeAttr("disabled");
            $("#btnCompleteSet").hide();
        }
    })

    function makeInpuMap(e) {
        const t = e.keyCode;
        focusEle = document.activeElement;
        if (document.getElementById('chat_msg') != focusEle && document.getElementById('name') != focusEle) {
            var keyList = [
                keysLeft.indexOf(t), 
                keysRight.indexOf(t), 
                keysForwards.indexOf(t), 
                keysBackwards.indexOf(t),
                keysSpeedModifier.indexOf(t),
                keysDance.indexOf(t),
                keysSit.indexOf(t),
                keysSamba.indexOf(t),
            ];

            var _index = keyList.indexOf(0);
            
            switch(_index) {
                case 0:
                    key = "a";
                    break;
                case 1:
                    key = "d";
                    break;
                case 2:
                    key = "w";
                    break;
                case 3:
                    key = "s";
                    break;
                case 4:
                    key = "Shift";
                    break;
                case 5:
                    key = "q";
                    break;
                case 6:
                    key = "c";
                    break;
                case 7:
                    key = "b";
                    break;
            }
            inputMap[key] = e.type == "keydown";
        }
    }

    window.addEventListener("keydown", function (e) {
       makeInpuMap(e); 
    });

    window.addEventListener("keyup", function (e) {
        makeInpuMap(e); 
    });

    $("#emoticon").click(function () {
        $('#emoticonBox').toggle();
    });

    // 언어설정
    $(".btn-lang").click(function() {
        switch($(this).data("lang")) {
            case "kr":
                location.href="/avatar/kr"
                break;
            case "en":
                location.href="/avatar"
                break;
        }
        return false;
    });

    // 카메라 애니메이션 함수
    function cameraTargetChangeWithAnimation(cam, target, position) {
        cam.animations = [];
        
        var callBack = function(test) {
            cameraChanging = false;
        }

        // 카메라 타겟 변경 애니메이션
        var targetAnimation = new BABYLON.Animation(
            "myAnimationcamera", 
            "target", 
            80, 
            BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );
 
         var keys = []; 

         keys.push({
             frame: 0,
             value: camera.target,
             outTangent: new BABYLON.Vector3(0, 0, 0)
         });

         keys.push({
             frame: 100,
             inTangent: new BABYLON.Vector3(.2, 0, 0),
             value: target,
         });

         targetAnimation.setKeys(keys);
         cam.animations.push(targetAnimation);
        
        // 카메라 포지션 변경 애니메이션
        var positionAnimation = new BABYLON.Animation(
            "myAnimationcamera", 
            "position", 
            80, 
            BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );

        keys = []; 

        keys.push({
            frame: 0,
            value: camera.position,
            outTangent: new BABYLON.Vector3(0, 0, 0)
        });

        keys.push({
            frame: 100,
            inTangent: new BABYLON.Vector3(.2, 0, 0),
            value: position,
        });

        positionAnimation.setKeys(keys);
        cam.animations.push(positionAnimation);

        scene.beginAnimation(cam, 0, 100, false, 1, callBack);
    }

    // 카메라 리셋 애니메이션 함수
    function cameraTargetResetWithAnimation(cam, target, position) {
        cam.animations = [];
        
        var callBack = function(test) {
            camera.target = player_info.position;
            scene.activeCamera = camera;
            scene.activeCamera.attachControl(canvas, true);
            cameraChanging = false;
        }

        // 카메라 타겟 변경 애니메이션
        var targetAnimation = new BABYLON.Animation(
            "myAnimationcamera", 
            "target", 
            80, 
            BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );
 
         var keys = []; 

         keys.push({
             frame: 0,
             value: eagleCamera.target,
             outTangent: new BABYLON.Vector3(0, 0, 0)
         });

         keys.push({
             frame: 100,
             inTangent: new BABYLON.Vector3(.2, 0, 0),
             value: target,
         });

         targetAnimation.setKeys(keys);
         cam.animations.push(targetAnimation);
        
        // 카메라 포지션 변경 애니메이션
        var positionAnimation = new BABYLON.Animation(
            "myAnimationcamera", 
            "position", 
            80, 
            BABYLON.Animation.ANIMATIONTYPE_VECTOR3, 
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );

        keys = []; 

        keys.push({
            frame: 0,
            value: eagleCamera.position,
            outTangent: new BABYLON.Vector3(0, 0, 0)
        });

        keys.push({
            frame: 100,
            inTangent: new BABYLON.Vector3(.2, 0, 0),
            value: position,
        });

        positionAnimation.setKeys(keys);
        cam.animations.push(positionAnimation);

        scene.beginAnimation(cam, 0, 100, false, 1, callBack);
    }

    // 카메라 이글뷰로 토글
    $('#btn-map').click(function() {
        if (cameraChanging) return false;

        cameraChanging = true;
        if (eagleView) {
            // Step1 : 아바타 중심으로 포지션 및 타겟 변경
            cameraTargetResetWithAnimation(eagleCamera, player_info.position, camera.position);
            eagleView = false;
        } else {
            // 이글뷰로 변경
            // 현재 카메라 포지션 그대로 기본 카메라 정의
            eagleCamera = new BABYLON.UniversalCamera("UniversalCamera", camera.position, scene);

            // Step1 : 아바타 중심 카메라에서 기본 카메라로 타겟은 아바타 그대로 유형만 변경
            scene.activeCamera = eagleCamera;
            scene.activeCamera.attachControl(canvas, false);
            eagleCamera.target = player_info.position;
            eagleCamera.inputs.clear();

            // 맵 중심으로 타겟 정의
            let newTarget = new BABYLON.Vector3(-15, 30, 10);

            // 이글뷰로 포지션 변경
            let newPosition = new BABYLON.Vector3(-30, 75, 10);

            // Step2 : 아바타에서 맵 중심으로 카메라 타겟 변경, 이글뷰로 포지션 이동
            cameraTargetChangeWithAnimation(eagleCamera, newTarget, newPosition);
            eagleView = true;
        }
        return false;
    });
});

function main_start() {

    scene.enablePhysics(); // 물리 엔진 활성화
    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);
    // scene.debugLayer.show({
    //     embedMode: true
    // }); // babylon 디버그

    // 필수 Lights - 햇빛
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.6;
    light.specular = BABYLON.Color3.Black();

    // 그라운드 생성
    initGround();

    // NPC 생성 
    initNPC();

    // 내 아바타 생성
    initPlayer();

    // 카메라 생성
    initCameraSetting();

    metakongTexture = new BABYLON.Texture("https://bitbucket.org/csidedev/metakong/raw/master/src/ver5/assets/img/metakong.png", scene);

    // 캠프 스테이지 메인 스크린 생성
    initMainScreen('');

    // 캠프 스테이지 버튼 생성
    initMainScreenBtn();

    engine.runRenderLoop(function () {
        scene.render();
    });

}

// ----------------- init set -------------------
// 그라운드 생성
function initGround() {

    // Skybox 
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {
        size: 1000.0
    }, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = BABYLON.Color3.Black()
    skyboxMaterial.specularColor = BABYLON.Color3.Black();
    skybox.material = skyboxMaterial;

    // 투명 설정
    const clearMaterial = new BABYLON.StandardMaterial("clearMaterial", scene);
    clearMaterial.alpha = 0;

    // Ground
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "world.glb", scene, function (newMeshes) {
        ground = newMeshes[0];
        ground.id = "ground";
        ground.name = "ground";
        ground.scaling = new BABYLON.Vector3(-1.5, 1.5, 1.5);
        ground.position = new BABYLON.Vector3(0, 0, 10);
        ground.rotation = new BABYLON.Vector3(0, 0, 0);
        ground.checkCollisions = true;

        // in_wall
        in_wall = scene.getMeshByName("in_wall");
        in_wall.checkCollisions = true;
        in_wall.material = clearMaterial;
        // out_wall
        out_wall = scene.getMeshByName("out_wall");
        out_wall.checkCollisions = true;
        out_wall.material = clearMaterial;

        // 열기구
        ps = scene.getMeshByName("ps");
        const frameRate = 10;
        const psSlide = new BABYLON.Animation("psSlide", "position.z", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);

        const keyFrames = [];
        keyFrames.push({
            frame: 0,
            value: -280
        });
        keyFrames.push({
            frame: frameRate,
            value: -270
        });
        keyFrames.push({
            frame: 2 * frameRate,
            value: -275
        });
        keyFrames.push({
            frame: 3 * frameRate,
            value: -280
        });

        psSlide.setKeys(keyFrames);
        ps.animations.push(psSlide);

        scene.beginAnimation(ps, 0, 3 * frameRate, true);

        // 코인
        coin = scene.getMeshByName("coin");
        coin.rotation = new BABYLON.Vector3(0, 0, 0);
        scene.registerBeforeRender(coinRotationAnimation);

        // 스크린
        const screen1 = BABYLON.MeshBuilder.CreateBox("camp_sreen", {
            width: 1,
            depth: 6,
            height: 5
        });
        screen1.checkCollisions = true;
        screen1.position.set(-19.66, 1, -5.9);
        screen1.rotation = new BABYLON.Vector3(0, -4, 0);
        screen1.material = clearMaterial;

        screen1.actionManager = new BABYLON.ActionManager(scene);
        screen1.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                function (event) {
                    fullScreen();
                })
        );
        
        document.getElementById("gauge").classList.add("done");
        document.getElementsByClassName("box-guage")[0].style.display = "none";
        document.getElementById("status").style.display = "none";
        document.getElementById("btnEnter").style.display = "block";

        $("#introBtn").fadeIn();
        $(".intro_ment").fadeOut();
    });

    function coinRotationAnimation() {
        scene.getMeshByName("coin").rotation.z += 0.01;
    }

    // ----------------- 투명 객체 만들기 -------------------
    // 중앙 분수대 
    const cylinder1 = BABYLON.MeshBuilder.CreateCylinder("WM", {
        diameter: 5,
        height: 3
    });
    cylinder1.checkCollisions = true;
    cylinder1.position = new BABYLON.Vector3(0.2, 0, 27.3)
    cylinder1.material = clearMaterial;

    // 콘서트 무대
    const box1 = BABYLON.MeshBuilder.CreateBox("concertStage", {
        width: 3.5,
        depth: 7,
        height: 3
    });
    box1.checkCollisions = true;
    box1.position.set(-16, 0, 10.9);
    box1.material = clearMaterial;

    // 무대 사이드 1
    const box2 = BABYLON.MeshBuilder.CreateBox("concertStage_side1", {
        width: 1,
        depth: 3,
        height: 3
    });
    box2.checkCollisions = true;
    box2.position.set(-15.8, 0, 15.5);
    box2.rotation = new BABYLON.Vector3(0, -Math.PI / 8, 0);
    box2.material = clearMaterial;

    // 무대 사이드 2
    const box3 = BABYLON.MeshBuilder.CreateBox("concertStage_side2", {
        width: 1,
        depth: 3,
        height: 3
    });
    box3.checkCollisions = true;
    box3.position.set(-16, 0, 6.4);
    box3.rotation = new BABYLON.Vector3(0, Math.PI / 8, 0);
    box3.material = clearMaterial;

    // 무대 사이드 나무1
    const box4 = BABYLON.MeshBuilder.CreateBox("concertStage_tree1", {
        width: 1,
        depth: 3,
        height: 3
    });
    box4.checkCollisions = true;
    box4.position.set(-18, 0, 17.2);
    box4.rotation = new BABYLON.Vector3(0, Math.PI / 2, 0);
    box4.material = clearMaterial;

    // 무대 사이드 나무1
    const box5 = BABYLON.MeshBuilder.CreateBox("concertStage_tree2", {
        width: 1,
        depth: 3,
        height: 3
    });
    box5.checkCollisions = true;
    box5.position.set(-18.5, 0, 4.2);
    box5.rotation = new BABYLON.Vector3(0, Math.PI / 2, 0);
    box5.material = clearMaterial;

    // 캠핑존 텐트
    const box6 = BABYLON.MeshBuilder.CreateBox("concertStage_tent", {
        width: 3,
        depth: 5,
        height: 3
    });
    box6.checkCollisions = true;
    box6.position.set(-16.5, 0, 2.6);
    box6.rotation = new BABYLON.Vector3(0, Math.PI / 1.5, 0);
    box6.material = clearMaterial;

    // 캠핑존 캠프파이어
    const cylinder2 = BABYLON.MeshBuilder.CreateCylinder("campFire", {
        diameter: 1.5,
        height: 3
    });
    cylinder2.checkCollisions = true;
    cylinder2.position = new BABYLON.Vector3(-17.8, 0, -2.9)
    cylinder2.material = clearMaterial;

    // 쓰러진 펭귄
    const npc_box = BABYLON.MeshBuilder.CreateBox("npc_box", {
        width: 1,
        depth: 2,
        height: 2
    });
    npc_box.checkCollisions = true;
    npc_box.position.set(21, 0, -1.65);
    npc_box.rotation = new BABYLON.Vector3(0, Math.PI / 2, 0);
    npc_box.material = clearMaterial;

    // 스트리머
    streamerPlane = BABYLON.MeshBuilder.CreatePlane('mainScreenStreamer', {
        height: 2,
        width: 5
    }, scene);
    streamerText = new BABYLON.GUI.TextBlock('streamerText');
    streamerTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(streamerPlane);
    streamerPlane.material.name = 'mainScreenStreamer';
    streamerPlane.position = new BABYLON.Vector3(-19.86, 3.25, -6.15);
    streamerPlane.rotation.y = streamerPlane.rotation.y + 3.75;

    var label = new BABYLON.GUI.Rectangle("mainScreenLabel");
    label.background = "white"
    label.height = "80px";
    label.width = "200px";
    label.alpha = 0.8;
    label.cornerRadius = 20;
    label.thickness = 1;

    streamerText.text = '';
    streamerText.color = "black";
    streamerText.fontSize = 30;

    streamerTexture.addControl(label);
    streamerTexture.name = "mainScreenStreamer";
    streamerTexture.renderScale = 0.4;
    label.addControl(streamerText);
}

// NPC 생성
function initNPC() {
    // 아바타 생성
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "ikong.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc1 = meshes[0];
        npc1.position = new BABYLON.Vector3(-11.82, 1, -2.42)
        npc1.rotation = new BABYLON.Vector3(0, Math.PI, 0);
        npc1.scaling.scaleInPlace(0.5);
        npc1.checkCollisions = true;
        npc1.name = "npc1";

        head_gear = scene.getMeshByName("head_gear")
        head_gear.name = "head_gear_npc1"
        head_gear.actionManager = new BABYLON.ActionManager(scene);
        head_gear.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                function (event) {
                    $("#healingZone .page1").show();
                    $("#healingZone").fadeIn();
                    $("#dimLayer").fadeIn();
                })
        );

        head = scene.getMeshByName("head")
        head.name = "head_npc1"
        head.actionManager = new BABYLON.ActionManager(scene);
        head.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                function (event) {
                    $("#healingZone .page1").show();
                    $("#healingZone").fadeIn();
                    $("#dimLayer").fadeIn();
                })
        );

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc1";
        danceRange.name = "danceRange_npc1";

        standRange.start(true, 1.0, standRange.from, standRange.to, false);

    });

    // 아바타 생성
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "ikong.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc2 = meshes[0];
        npc2.position = new BABYLON.Vector3(8.9, 1, 28)
        npc2.rotation = new BABYLON.Vector3(0, Math.PI / 8, 0);
        npc2.scaling.scaleInPlace(0.5);
        npc2.checkCollisions = true;
        npc2.name = "npc2";

        head_gear = scene.getMeshByName("head_gear")
        head_gear.name = "head_gear_npc2"
        head_gear.actionManager = new BABYLON.ActionManager(scene);
        head_gear.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                function (event) {
                    $("#commerceZone .page1").show();
                    $("#commerceZone").fadeIn();
                    $("#dimLayer").fadeIn();
                })
        );


        head = scene.getMeshByName("head")
        head.name = "head_npc2"
        head.actionManager = new BABYLON.ActionManager(scene);
        head.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
                function (event) {
                    $("#commerceZone .page1").show();
                    $("#commerceZone").fadeIn();
                    $("#dimLayer").fadeIn();
                })
        );

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc2";
        danceRange.name = "danceRange_npc2";

        danceRange.start(true, 1.0, danceRange.from, danceRange.to, false);

    });
    // 춤추는 펭귄
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "penkongboy.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc_pg1 = meshes[0];
        npc_pg1.position = new BABYLON.Vector3(-5.78, 0.077, -12.73)
        npc_pg1.rotation = new BABYLON.Vector3(0, Math.PI, 0);
        npc_pg1.scaling.scaleInPlace(0.5);
        npc_pg1.checkCollisions = true;
        npc_pg1.name = "npc_pg1";

        npc_pg1skeleton = skeletons[0];
        npc_pg1skeleton.name = "skel_npc_pg1";

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc_pg1";
        danceRange.name = "danceRange_npc_pg1";

        danceRange.start(true, 1.0, danceRange.from, danceRange.to, false);
    });
    // 펭귄 1
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "penkongboy.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc_pg2 = meshes[0];
        npc_pg2.position = new BABYLON.Vector3(-6.46, 0.077, -9.33)
        npc_pg2.rotation = new BABYLON.Vector3(0, Math.PI, 0);
        npc_pg2.scaling.scaleInPlace(0.5);
        npc_pg2.checkCollisions = true;
        npc_pg2.name = "npc_pg2";

        npc_pg2skeleton = skeletons[0];
        npc_pg2skeleton.name = "skel_npc_pg2";

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc_pg2";
        danceRange.name = "danceRange_npc_pg2";

        standRange.start(true, 1.0, standRange.from, standRange.to, false);
    });
    // 펭귄 2
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "penkongboy.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc_pg3 = meshes[0];
        npc_pg3.position = new BABYLON.Vector3(3.38, 0.077, -2.4)
        npc_pg3.rotation = new BABYLON.Vector3(0, -Math.PI / 2.5, 0);
        npc_pg3.scaling.scaleInPlace(0.5);
        npc_pg3.checkCollisions = true;
        npc_pg3.name = "npc_pg3";

        npc_pg3skeleton = skeletons[0];
        npc_pg3skeleton.name = "skel_npc_pg3";

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc_pg3";
        danceRange.name = "danceRange_npc_pg3";

        standRange.start(true, 1.0, standRange.from, standRange.to, false);
    });

    // 펭귄 3
    BABYLON.SceneLoader.ImportMesh("", "./assets/meshes/metakong/", "penkongboy.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        npc_pg4 = meshes[0];
        npc_pg4.position = new BABYLON.Vector3(-9.42, 0.077, -5.94)
        npc_pg4.rotation = new BABYLON.Vector3(0, -Math.PI / 2.5, 0);
        npc_pg4.scaling.scaleInPlace(0.5);
        npc_pg4.checkCollisions = true;
        npc_pg4.name = "npc_pg4";

        npc_pg4skeleton = skeletons[0];
        npc_pg4skeleton.name = "skel_npc_pg4";

        var standRange = animationGroups[1];
        var danceRange = animationGroups[0];
        standRange.name = "standRange_npc_pg4";
        danceRange.name = "danceRange_npc_pg4";

        standRange.start(true, 1.0, standRange.from, standRange.to, false);
    });
}
// 내 아바타 생성
function initPlayer() {

    BABYLON.SceneLoader.ImportMesh("girl", "./assets/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {
        player = meshes[0];
        player.scaling.scaleInPlace(0.5);
        player.name = "girl_" + socket.id;
        player._children[0]._position._y = -2.25;
        player.position = player_info.position;
        player.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);

        skeleton = skeletons[0];
        skeleton.name = "girl_" + socket.id;

        Cylinder1 = scene.getMaterialByName("Cylinder");
        Cylinder1.name = "Cylinder_" + socket.id;
        Cylinder2 = scene.getTextureByName("Cylinder (Emissive)");
        Cylinder2.name = "Cylinder_" + socket.id;

        // 아바타 애니메이션 설정
        var idleRange = animationGroups[3];
        var walkRange = animationGroups[4];
        var runRange = animationGroups[1];
        var sitRange = animationGroups[2];
        var danceRange = animationGroups[0];

        idleRange.name = "idleRange_" + socket.id;
        walkRange.name = "walkRange_" + socket.id;
        runRange.name = "runRange_" + socket.id;
        sitRange.name = "sitRange_" + socket.id;
        danceRange.name = "danceRange_" + socket.id;
        if (idleRange) idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);

        player_info.player = player;
        initPlayerCam();
        var player_animation = null;
        var animating = true;
        // 아바타 무빙
        scene.onBeforeRenderObservable.add(() => {
            // 채팅창 포커스시 무빙x
            if (!$("#chatMsg").is(":focus")) {
                var keydown = false;
                player_info.alpha = Math.PI - (camera.alpha - Math.PI / 2);
                if (inputMap["w"] && inputMap["a"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 1.5), 0);
                    keydown = true;
                } else if (inputMap["w"] && inputMap["d"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 1.5), 0);
                    keydown = true;
                } else if (inputMap["s"] && inputMap["a"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 3), 0);
                    keydown = true;
                } else if (inputMap["s"] && inputMap["d"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 3), 0);
                    keydown = true;
                } else if (inputMap["w"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + Math.PI, 0);
                    keydown = true;
                } else if (inputMap["s"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha, 0);
                    keydown = true;
                } else if (inputMap["a"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha + (Math.PI / 2), 0);
                    keydown = true;
                } else if (inputMap["d"]) {
                    player_info.player.rotation = new BABYLON.Vector3(0, player_info.alpha - (Math.PI / 2), 0);
                    keydown = true;
                } else if (inputMap["q"] || inputMap["c"]) {
                    keydown = true;
                }

                // 캠핑장 의자에 앉기
                if (endPosition != null) {
                    scene.getAnimationGroupByName("sitRange_" + player_info.socketId).stop();
                    scene.getAnimationGroupByName("danceRange_" + player_info.socketId).stop();
                    scene.getAnimationGroupByName("runRange_" + player_info.socketId).stop();
                    scene.getAnimationGroupByName("idleRange_" + player_info.socketId).stop();

                    walkRange = scene.getAnimationGroupByName("walkRange_" + player_info.socketId);
                    walkRange.start(true, 1.0, walkRange.from, walkRange.to, false);

                    if (player_info.player.position.x.toFixed(2) < endPosition.x) {
                        player_info.player.rotation.y = -Math.PI / 2;
                        player_info.player.position.x = player_info.player.position.x + 0.005;
                        player_info.position.x = player_info.position.x + 0.005;
                    } else if (player_info.player.position.x.toFixed(2) > endPosition.x) {
                        player_info.player.rotation.y = Math.PI / 2;
                        player_info.player.position.x = player_info.player.position.x - 0.005;
                        player_info.position.x = player_info.position.x - 0.005;
                    }
                    if (player_info.player.position.z.toFixed(2) < endPosition.z) {
                        player_info.player.rotation.y = Math.PI;
                        player_info.player.position.z = player_info.player.position.z + 0.005;
                        player_info.position.z = player_info.position.z + 0.005;
                    } else if (player_info.player.position.z.toFixed(2) > endPosition.z) {
                        player_info.player.rotation.y = -0.3;
                        player_info.player.position.z = player_info.player.position.z - 0.005;
                        player_info.position.z = player_info.position.z - 0.005;
                    }

                    if (player_info.player.position.x.toFixed(2) == endPosition.x &&
                        player_info.player.position.z.toFixed(2) == endPosition.z) {
                        player_info.player.rotation.y = endRotation;
                        scene.getAnimationGroupByName("idleRange_" + player_info.socketId).stop();
                        scene.getAnimationGroupByName("danceRange_" + player_info.socketId).stop();
                        scene.getAnimationGroupByName("runRange_" + player_info.socketId).stop();
                        scene.getAnimationGroupByName("walkRange_" + player_info.socketId).stop();

                        sitRange = scene.getAnimationGroupByName("sitRange_" + player_info.socketId);
                        sitRange.start(true, 1.0, sitRange.from, sitRange.to, false);
                        endPosition = null;
                        endRotation = null;

                        // 소켓에 아바타 애니메이션 전송- 쉬는중!
                        socket.emit("animationUpdate", {
                            socketId: socket.id,
                            animation: 'sitRange',
                            speed: player_info.speed,
                            rotation: player_info.player.rotation.y,
                            position_x: player_info.player.position.x,
                            position_z: player_info.player.position.z
                        });
                    }
                }

                if (keydown) {
                    endPosition = null;
                    if (!(inputMap["q"] || inputMap["c"])) {
                        player_info.player.moveWithCollisions(player_info.player.forward.scaleInPlace(player_info.speed));
                        player_info.position = player_info.player.position;

                        if (player_animation != 'runRange' && inputMap["Shift"]) {
                            animating = false;
                            player_info.speed = 0.1;
                        }
                        if (player_animation == 'runRange' && !inputMap["Shift"]) {
                            animating = false;
                            player_info.speed = 0.05;
                        }
                    }

                    // 소켓에 아바타 위치 전송
                    /*socket.emit("positionUpdate", {
                        socketId: socket.id,
                        position_x: player.position.x,
                        position_z: player.position.z,
                        rotation: player.rotation.y
                    });*/
                    if (!animating) {
                        animating = true;
                        if (inputMap["q"]) {
                            danceRange.start(true, 0.5, danceRange.from, danceRange.to, false);
                            player_rotation_y = player_info.player.rotation.y;
                            player_animation = 'danceRange';
                            player_speed = player_info.speed;
                        } else if (inputMap["c"]) {
                            sitRange.start(true, 1.0, sitRange.from, sitRange.to, false);
                            player_rotation_y = player_info.player.rotation.y;
                            player_animation = 'sitRange';
                            player_speed = player_info.speed;
                        } else if (inputMap["Shift"]) {
                            player_info.speed = 0.1;
                            runRange.start(true, 1.0, runRange.from, runRange.to, false);
                            // 소켓에 아바타 애니메이션 전송- 뛰는중!
                            player_rotation_y = player_info.player.rotation.y;
                            player_animation = 'runRange';
                            player_speed = player_info.speed;
                        } else {
                            runRange.stop();
                            walkRange.start(true, 1.0, walkRange.from, walkRange.to, false);
                            player_rotation_y = player_info.player.rotation.y;
                            player_animation = 'walkRange';
                            player_speed = player_info.speed;
                        }

                        socket.emit("animationUpdate", {
                            socketId: socket.id,
                            animation: player_animation,
                            speed: player_speed,
                            rotation: player_rotation_y,
                            position_x: player_info.player.position.x,
                            position_z: player_info.player.position.z
                        });
                    } else {
                        if (player_info.player.rotation.y != player_rotation_y) {
                            player_rotation_y = player_info.player.rotation.y;
                            socket.emit("animationUpdate", {
                                socketId: socket.id,
                                animation: player_animation,
                                speed: player_speed,
                                rotation: player_rotation_y,
                                position_x: player_info.player.position.x,
                                position_z: player_info.player.position.z
                            });
                        }
                    }
                } else {
                    player_info.speed = 0.05;

                    if (animating) {
                        idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);
                        sitRange.stop();
                        danceRange.stop();
                        runRange.stop();
                        walkRange.stop();

                        animating = false;
                        // 소켓에 아바타 애니메이션 전송- 쉬는중!
                        socket.emit("animationUpdate", {
                            socketId: socket.id,
                            animation: 'idleRange',
                            speed: player_info.speed,
                            rotation: player_info.player.rotation.y,
                            position_x: player_info.player.position.x,
                            position_z: player_info.player.position.z
                        });
                    }
                }
                try {
                    player_info.nameTag.position.x = player.position.x;
                    player_info.nameTag.position.z = player.position.z;
                    player_info.nameTag.position.y = 1.8;
                } catch {}

                player_info.player.setEnabled(player_info.avatarState == "avatar");
                player_info.player_cam.setEnabled(player_info.avatarState != "avatar");
            }
        });

    });

    scene.onBeforeRenderObservable.add(() => {
        if (animationUpdate_index > -1) {
            if (players[animationUpdate_index] != null) {
                if (players[animationUpdate_index].rockName && players[animationUpdate_index].rockName != null) {
                    scene.getAnimationGroupByName("sitRange_" + players[animationUpdate_index].socketId).stop();
                    scene.getAnimationGroupByName("danceRange_" + players[animationUpdate_index].socketId).stop();
                    scene.getAnimationGroupByName("runRange_" + players[animationUpdate_index].socketId).stop();
                    scene.getAnimationGroupByName("idleRange_" + players[animationUpdate_index].socketId).stop();

                    walkRange_ = scene.getAnimationGroupByName("walkRange_" + players[animationUpdate_index].socketId);
                    walkRange_.start(true, 1.0, walkRange_.from, walkRange_.to, false);

                    rockName_ = players[animationUpdate_index].rockName;

                    if (rockName_ == 'rock') {
                        endPosition_ = new BABYLON.Vector3(-17.00, 1.2, -1.55);
                        endRotation_ = 0.5
                    } else if (rockName_ == 'rock_1') {
                        endPosition_ = new BABYLON.Vector3(-15.75, 1.2, -3.57);
                        endRotation_ = 0.5
                    } else if (rockName_ == 'rock_2') {
                        endPosition_ = new BABYLON.Vector3(-19.65, 1.2, -1.50);
                        endRotation_ = 0.5
                    } else if (rockName_ == 'rock_3') {
                        endPosition_ = new BABYLON.Vector3(-19.77, 1.2, -3.92);
                        endRotation_ = 0.5
                    } else if (rockName_ == 'rock_4') {
                        endPosition_ = new BABYLON.Vector3(-17.25, 1.2, -5.16);
                        endRotation_ = 0.5
                    } else if (rockName_ == 'rock_1_2') {
                        endPosition_ = new BABYLON.Vector3(-19.70, 1.2, 10.98);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_2_2') {
                        endPosition_ = new BABYLON.Vector3(-19.68, 1.2, 12.56);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_2_3') {
                        endPosition_ = new BABYLON.Vector3(-19.64, 1.2, 14.25);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_3_2') {
                        endPosition_ = new BABYLON.Vector3(-19.70, 1.2, 9.29);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_4_2') {
                        endPosition_ = new BABYLON.Vector3(-19.68, 1.2, 7.65);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_5_2') {
                        endPosition_ = new BABYLON.Vector3(-21.59, 1.2, 7.71);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_6') {
                        endPosition_ = new BABYLON.Vector3(-21.71, 1.2, 9.46);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_7') {
                        endPosition_ = new BABYLON.Vector3(-21.62, 1.2, 11.05);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_8') {
                        endPosition_ = new BABYLON.Vector3(-21.60, 1.2, 12.65);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_9') {
                        endPosition_ = new BABYLON.Vector3(-21.69, 1.2, 14.23);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_10') {
                        endPosition_ = new BABYLON.Vector3(-23.66, 1.2, 7.83);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_11') {
                        endPosition_ = new BABYLON.Vector3(-23.68, 1.2, 9.45);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_12') {
                        endPosition_ = new BABYLON.Vector3(-23.66, 1.2, 11.04);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_13') {
                        endPosition_ = new BABYLON.Vector3(-23.60, 1.2, 12.66);
                        endRotation_ = 4.5
                    } else if (rockName_ == 'rock_14') {
                        endPosition_ = new BABYLON.Vector3(-23.67, 1.2, 14.26);
                        endRotation_ = 4.5
                    }

                    if (players[animationUpdate_index].player.position.x.toFixed(2) < endPosition_.x) {
                        players[animationUpdate_index].player.rotation.y = -Math.PI / 2;
                        players[animationUpdate_index].player.position.x = players[animationUpdate_index].player.position.x + 0.005;
                        players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                        players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);

                    } else if (players[animationUpdate_index].player.position.x.toFixed(2) > endPosition_.x) {
                        players[animationUpdate_index].player.rotation.y = Math.PI / 2;
                        players[animationUpdate_index].player.position.x = players[animationUpdate_index].player.position.x - 0.005;
                        players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                        players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);
                    }
                    if (players[animationUpdate_index].player.position.z.toFixed(2) < endPosition_.z) {
                        players[animationUpdate_index].player.rotation.y = Math.PI;
                        players[animationUpdate_index].player.position.z = players[animationUpdate_index].player.position.z + 0.005;
                        players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                        players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);
                    } else if (players[animationUpdate_index].player.position.z.toFixed(2) > endPosition_.z) {
                        players[animationUpdate_index].player.rotation.y = -0.3;
                        players[animationUpdate_index].player.position.z = players[animationUpdate_index].player.position.z - 0.005;
                        players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                        players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);
                    }

                    if (players[animationUpdate_index].player.position.x.toFixed(2) == endPosition_.x &&
                        players[animationUpdate_index].player.position.z.toFixed(2) == endPosition_.z) {
                        players[animationUpdate_index].player.rotation.y = endRotation_;
                        scene.getAnimationGroupByName("idleRange_" + players[animationUpdate_index].socketId).stop();
                        scene.getAnimationGroupByName("danceRange_" + players[animationUpdate_index].socketId).stop();
                        scene.getAnimationGroupByName("runRange_" + players[animationUpdate_index].socketId).stop();
                        scene.getAnimationGroupByName("walkRange_" + players[animationUpdate_index].socketId).stop();

                        sitRange_ = scene.getAnimationGroupByName("sitRange_" + players[animationUpdate_index].socketId);
                        sitRange_.start(true, 1.0, sitRange_.from, sitRange_.to, false);
                        players[animationUpdate_index].rockName = null;
                    }
                } else {
                    if (players[animationUpdate_index].animation) {
                        if (players[animationUpdate_index].animation != 'idleRange' && players[animationUpdate_index].animation != 'sitRange' && players[animationUpdate_index].animation != 'danceRange') {
                            players[animationUpdate_index].player.moveWithCollisions(players[animationUpdate_index].player.forward.scaleInPlace(players[animationUpdate_index].speed));
                        }
                        players[animationUpdate_index].player_cam.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.2, players[animationUpdate_index].player.position.z);
                        players[animationUpdate_index].nameTag.position = new BABYLON.Vector3(players[animationUpdate_index].player.position.x, 1.8, players[animationUpdate_index].player.position.z);
                    }
                }
            }
        }
    });
}

function initPlayerCam() {

    try {
        scene.removeMesh(scene.getMeshByName("webCam_" + socket.id));
        scene.getMaterialByName("playerCamMat_" + socket.id).dispose();
        scene.getTextureByName("playerCamVideo_" + socket.id).dispose();
    } catch {

    }
    // 아바타 웹캠
    playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + socket.id, scene);
    video = document.getElementById('localVideo');
    videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + socket.id, video, scene, true, true);

    // 원형 plane 생성
    player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + socket.id, {});
    player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    player_disc.position = player_info.position;

    // plane에 캠 영상 적용
    playerCamMat.backFaceCulling = false;
    playerCamMat.diffuseTexture = videoTexture;
    playerCamMat.emissiveColor = BABYLON.Color3.White();
    player_disc.material = playerCamMat;

    player_info.player_cam = player_disc;

}

// 멤버 아바타 생성
// 기존 접속 아바타 생성 + 새로 접속한 아바타 생성
// 네임태그도 함께 생성
function createPlayer(data) {

    // 아바타 생성
    BABYLON.SceneLoader.ImportMesh("girl", "./assets/meshes/metakong/", "girl_rig.glb", scene, (meshes, particleSystems, skeletons, animationGroups) => {

        var playerA = meshes[0];
        playerA.scaling.scaleInPlace(0.5);
        playerA.name = "girl_" + data.socketId;
        playerA._children[0]._position._y = -2.25; // 캠 영상과 기준 좌표 맞추기 위해 추가
        playerA.rotation = new BABYLON.Vector3(0, data.rotation, 0);
        playerA.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);

        skeleton = skeletons[0];
        skeleton.name = "girl_" + data.socketId;

        Cylinder1 = scene.getMaterialByName("Cylinder");
        Cylinder1.name = "Cylinder_" + data.socketId;
        Cylinder2 = scene.getTextureByName("Cylinder (Emissive)");
        Cylinder2.name = "Cylinder_" + data.socketId;

        var idleRange = animationGroups[3];
        var walkRange = animationGroups[4];
        var runRange = animationGroups[1];
        var sitRange = animationGroups[2];
        var danceRange = animationGroups[0];
        idleRange.name = "idleRange_" + data.socketId;
        walkRange.name = "walkRange_" + data.socketId;
        runRange.name = "runRange_" + data.socketId;
        sitRange.name = "sitRange_" + data.socketId;
        danceRange.name = "danceRange_" + data.socketId;

        if (data.animation == 'idleRange') {
            idleRange.start(true, 1.0, idleRange.from, idleRange.to, false);
        } else if (data.animation == 'sitRange') {
            sitRange.start(true, 1.0, sitRange.from, sitRange.to, false);
        } else if (data.animation == 'danceRange') {
            danceRange.start(true, 1.0, danceRange.from, danceRange.to, false);
        }

        video = document.getElementById('remoteVideo_' + data.socketId);

        videoTexture = new BABYLON.VideoTexture('playerCamVideo_' + data.socketId, video, scene, false);
        playerCamMat = new BABYLON.StandardMaterial('playerCamMat_' + data.socketId, scene);

        // 원형 plane 생성
        player_disc = BABYLON.MeshBuilder.CreateDisc("webCam_" + data.socketId, {});
        player_disc.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
        player_disc.scaling = new BABYLON.Vector3(1, -1, 1);

        // plane에 캠 영상 적용
        playerCamMat.backFaceCulling = false;
        playerCamMat.diffuseTexture = videoTexture;
        playerCamMat.emissiveColor = BABYLON.Color3.White();
        player_disc.material = playerCamMat;

        player_disc.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);
        playerA.position = new BABYLON.Vector3(data.position_x, 1.2, data.position_z);

        // 아바타 네임태그
        namePlaneA = setNameLabel(data, data.userName);

        playerA.setEnabled(data.avatarState == "avatar");
        player_disc.setEnabled(data.avatarState != "avatar");

        players.push({
            socketId: data.socketId,
            avatarState: data.avatarState,
            animation: data.animation,
            speed: '0.01',
            userName: data.userName,
            position_x: data.position_x,
            position_z: data.position_z,
            player: playerA,
            player_cam: player_disc,
            nameTag: namePlaneA,
            rockName: null
        });
    });
}

// 아바타 닉네임 태그 생성
function setNameLabel(playerInfo, name) {

    try {
        // 기존 닉네임 삭제
        scene.removeMesh(scene.getMeshByName("namePlane_" + playerInfo.socketId));
        scene.getMaterialByName("namePlane_" + playerInfo.socketId).dispose();
        scene.getTextureByName("nameText_" + playerInfo.socketId).dispose();
    } catch {}

    playerInfo.name = name;

    // 아바타 네임태그
    namePlane = BABYLON.MeshBuilder.CreatePlane('namePlane_' + playerInfo.socketId, {
        width: 0.5,
        height: 0.4
    }, scene);
    namePlane.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

    nameText = new BABYLON.GUI.TextBlock('nameText');
    nameText.text = name;
    nameText.fontSize = 60;
    nameText.color = playerInfo.socketId == socket.id ? "yellow" : "white";
    namePlane.position = new BABYLON.Vector3(playerInfo.position_x, 1.8, playerInfo.position_z)

    advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(namePlane);
    advancedTexture.addControl(nameText);
    advancedTexture.name = "nameText_" + playerInfo.socketId;
    advancedTexture.renderScale = 0.3;
    namePlane.material.name = 'namePlane_' + playerInfo.socketId;

    if (playerInfo.socketId != socket.id) {
        playerInfo.nameTag = namePlane;
    }

    return namePlane;
}

// 월드접속 애니메이션
function openingAnimation() {
    var frameRate = 30;
    var radiusAnimation = new BABYLON.Animation("radiusAnimation", "radius", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    var radiusKeyFrames = []
    radiusKeyFrames.push({
        value: 1000,
        frame: 0
    })
    radiusKeyFrames.push({
        value: 15,
        frame: 2.5 * frameRate,
    })
    radiusAnimation.setKeys(radiusKeyFrames)

    scene.beginDirectAnimation(camera, [radiusAnimation], 0, 2.5 * frameRate, false, 1,() => {
        camera.upperRadiusLimit = 30;
        $("#dimLayer").fadeIn();
        $("#keyGuideModal").fadeIn();
    });
}

$("#introBtn").on("click", function () {
    openingAnimation();
    $("#fog").addClass("disapear");
})

// 카메라 생성
function initCameraSetting() {

    var alpha = player_info.position.y + Math.PI / 1.5;
    var beta = Math.PI / 3;
    var target = new BABYLON.Vector3(player_info.position.x, player_info.position.y, player_info.position.z);

    camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", alpha, beta, 15, target, scene);
    //camera.checkCollisions = true;
    camera.wheelPrecision = 15;
    camera.lowerRadiusLimit = 7;
    // camera.upperRadiusLimit = 130;
    camera.lowerBetaLimit = -0.1;
    camera.upperBetaLimit = (Math.PI / 2) * 0.95;
    camera.wheelDeltaPercentage = 0.01;
    camera.inputs.remove(camera.inputs.attached.keyboard);

    scene.activeCamera = camera;
    scene.activeCamera.attachControl(canvas, true);
    scene.registerBeforeRender(function () {
        // 카메라가 아바타 따라다니도록 세팅
        camera.target.copyFrom(player_info.position);
        camera.target.y = 1.2;
    });
}

// 메인 스크린 생성
function initMainScreen(screenType) {
    var planeOpts = {
        height: 3,
        width: 5.5,
        sideOrientation: BABYLON.Mesh.DOUBLESIDE
    };
    var ANote0Video = new BABYLON.MeshBuilder.CreatePlane("mainScreen", planeOpts, scene);
    ANote0Video.position = new BABYLON.Vector3(-19.881, 2.1, -6.185);
    ANote0Video.rotation.y = ANote0Video.rotation.y + 3.75;
    ANote0Video.actionManager = new BABYLON.ActionManager(scene);
    ANote0Video.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                fullScreen();
            })
    );
    var mat = new BABYLON.StandardMaterial("mainScreenMat", scene);
    mat.emissiveColor = BABYLON.Color3.White();
    mat.diffuseTexture = metakongTexture;
    mat.emissiveColor = BABYLON.Color3.White();
    ANote0Video.material = mat;

    if (screenType == 'screen' || screenType == 'webCam') {
        mat.diffuseTexture.vScale = -1;
        ANote0Video.scaling = new BABYLON.Vector3(1, -1, 1);
    } else {
        mat.diffuseTexture.vScale = 1;
        ANote0Video.scaling = new BABYLON.Vector3(1, 1, 1);
    }
}
// 메인 스크린 버튼 생성
function initMainScreenBtn() {

    // 캠프 스테이지 스크린 공유 버튼
    mainScreenBtnMat = new BABYLON.StandardMaterial('mainScreenBtn', scene);
    if ($('.btn-lang.on').data('lang') == 'kr') {
        mainScreenBtnMat.diffuseTexture = new BABYLON.Texture("../assets/img/ui/20220304/btn_share.png", scene);
    } else {
        mainScreenBtnMat.diffuseTexture = new BABYLON.Texture("./assets/img/20220309/share_btn_circle_off.png", scene);    
    }
    
    mainScreenBtnMat.emissiveColor = BABYLON.Color3.White();

    mainScreenBtn = BABYLON.MeshBuilder.CreateDisc("mainScreenBtn", {
        radius: 0.4
    });
    mainScreenBtn.position = new BABYLON.Vector3(-23.04, 3, -4);
    mainScreenBtn.rotation.z = mainScreenBtn.rotation.z - 3.14;
    mainScreenBtn.rotation.y = mainScreenBtn.rotation.y + 3.75;
    mainScreenBtn.scaling = new BABYLON.Vector3(-1, 1, 1);
    mainScreenBtn.material = mainScreenBtnMat;
    mainScreenBtn.actionManager = new BABYLON.ActionManager(scene);
    mainScreenBtn.actionManager.registerAction(
        new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger,
            function (event) {
                if ($('#stream_state').val() != 'stop' && (player_info.streamer != player_info.socketId)) {
                    alert('이미 사용중입니다.');
                    return;
                }
                // 메인 스크린 선택- 설청 모달 띄움
                document.getElementById('setting').innerText = "";
                $("#dimLayer").show();
                $('#shareScreen').show();
                var localVideo = $("#localVideo");
                $("#shareCam, #shareCamEN").prepend(localVideo);
                // var stream_state = document.getElementById('stream_state').value;
                // if (stream_state == 'screen') {
                    // $('.button-webcam').hide();
                // } else {
                    // $('.button-webcam').show();
                // }
            })
    );
}
// 화면공유 닫기
$("#btnCloseShare").on("click", function () {
    $("#dimLayer").hide();
    $("#shareScreen").hide();
    var localVideo = $("#localVideo");
    $("#localVideoContainer").prepend(localVideo);
})
// ----------------- init set end -------------------
// ----------------- event set ------------------


var endPosition;
var endRotation;
canvas.addEventListener('click', () => {
    // We try to pick an object
    const result = scene.pick(scene.pointerX, scene.pointerY)
    let pickedMesh

    // Then if a mesh is hit, highlight it
    if (result.hit) {
        // Then highlight the hit mesh
        pickedMesh = result.pickedMesh
        //console.log(pickedMesh);
        if (pickedMesh.name == "npc_box") {
            alert('???: 아파용....');
        }

        if (pickedMesh.name.startsWith('rock') && pickedMesh.name != 'rock_5') {
            if (pickedMesh.parent.name == 'CAMPING_ZONE') {
                // 캠핑존 안에 있는지 확인
                // if (player_info.player.position.x <= -9.8 && player_info.player.position.x >= -22.3) {
                //     if (player_info.player.position.z <= 3.35 && player_info.player.position.z >= -6.35) {

                        rock = new BABYLON.Vector3(-17.00, 1.2, -1.55)
                        rock_1 = new BABYLON.Vector3(-15.75, 1.2, -3.57)
                        rock_2 = new BABYLON.Vector3(-19.65, 1.2, -1.50)
                        rock_3 = new BABYLON.Vector3(-19.77, 1.2, -3.92)
                        rock_4 = new BABYLON.Vector3(-17.25, 1.2, -5.16)

                        if (pickedMesh.name == 'rock') endPosition = rock;
                        else if (pickedMesh.name == 'rock_1') endPosition = rock_1;
                        else if (pickedMesh.name == 'rock_2') endPosition = rock_2;
                        else if (pickedMesh.name == 'rock_3') endPosition = rock_3;
                        else if (pickedMesh.name == 'rock_4') endPosition = rock_4;
                        else endPosition = null;
                        startPosition = player_info.player.position;

                        endRotation = 0.5;

                        socket.emit('userSit', pickedMesh.name, socket.id);
                //     }
                // }
            } else if (pickedMesh.parent.name == 'rocks') {
                // 콘서트존 안에 있는지 확인
                // if (player_info.player.position.x <= -17.25 && player_info.player.position.x >= -26.77) {
                //     if (player_info.player.position.z <= 17.70 && player_info.player.position.z >= 3.55) {

                        rock_1_2 = new BABYLON.Vector3(-19.70, 1.2, 10.98)
                        rock_2_2 = new BABYLON.Vector3(-19.68, 1.2, 12.56)
                        rock_2_3 = new BABYLON.Vector3(-19.64, 1.2, 14.25)
                        rock_3_2 = new BABYLON.Vector3(-19.70, 1.2, 9.29)
                        rock_4_2 = new BABYLON.Vector3(-19.68, 1.2, 7.65)
                        rock_5_2 = new BABYLON.Vector3(-21.59, 1.2, 7.71)
                        rock_6 = new BABYLON.Vector3(-21.71, 1.2, 9.46)
                        rock_7 = new BABYLON.Vector3(-21.62, 1.2, 11.05)
                        rock_8 = new BABYLON.Vector3(-21.60, 1.2, 12.65)
                        rock_9 = new BABYLON.Vector3(-21.69, 1.2, 14.23)
                        rock_10 = new BABYLON.Vector3(-23.66, 1.2, 7.83)
                        rock_11 = new BABYLON.Vector3(-23.68, 1.2, 9.45)
                        rock_12 = new BABYLON.Vector3(-23.66, 1.2, 11.04)
                        rock_13 = new BABYLON.Vector3(-23.60, 1.2, 12.66)
                        rock_14 = new BABYLON.Vector3(-23.67, 1.2, 14.26)

                        if (pickedMesh.name == 'rock_1_2') endPosition = rock_1_2;
                        else if (pickedMesh.name == 'rock_2_2') endPosition = rock_2_2;
                        else if (pickedMesh.name == 'rock_2_3') endPosition = rock_2_3;
                        else if (pickedMesh.name == 'rock_3_2') endPosition = rock_3_2;
                        else if (pickedMesh.name == 'rock_4_2') endPosition = rock_4_2;
                        else if (pickedMesh.name == 'rock_5_2') endPosition = rock_5_2;
                        else if (pickedMesh.name == 'rock_6') endPosition = rock_6;
                        else if (pickedMesh.name == 'rock_7') endPosition = rock_7;
                        else if (pickedMesh.name == 'rock_8') endPosition = rock_8;
                        else if (pickedMesh.name == 'rock_9') endPosition = rock_9;
                        else if (pickedMesh.name == 'rock_10') endPosition = rock_10;
                        else if (pickedMesh.name == 'rock_11') endPosition = rock_11;
                        else if (pickedMesh.name == 'rock_12') endPosition = rock_12;
                        else if (pickedMesh.name == 'rock_13') endPosition = rock_13;
                        else if (pickedMesh.name == 'rock_14') endPosition = rock_14;
                        else endPosition = null;
                        startPosition = player_info.player.position;

                        endRotation = 4.5;

                        socket.emit('userSit', pickedMesh.name, socket.id);
                //     }
                // }
            }
        }
    }
})

// 내 닉네임 변경 이벤트
function updateNameLabel() {
    // 닉네임 재설정
    var name = document.getElementById('name').value;
    player_info.nameTag = setNameLabel(player_info, name);

    // 소켓에 전달
    socket.emit("userNameChange", {
        socketId: socket.id,
        userName: name,
        position_x: player_info.player.position.x,
        position_z: player_info.player.position.z,
    });

    $("#btnUpdateNick, #btnUpdateNickEN").removeClass("on");
    $("#btnCompleteSet").show();
}

// 아바타 의자에 앉기
function playerSit(rockName, index_sit) {
    players[index_sit].rockName = rockName;
    animationUpdate_index = index_sit;
}

// 이모티콘
function playerEmotion(data, i) {
    if (i == null) {
        target_socketId = player_info.socketId
        target_player = player_info.player;
        $('.emotionBox').css('display', 'none');
    } else {
        target_socketId = players[i].player.socketId
        target_player = players[i].player;
    }

    var emotionPlane = new BABYLON.MeshBuilder.CreatePlane('emotion_' + target_socketId, {
        width: 2,
        height: 1.5
    }, scene);

    var emotionText = new BABYLON.GUI.TextBlock('emotion_' + target_socketId);
    var emotionTexture = new BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(emotionPlane);
    emotionPlane.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
    emotionPlane.parentNode = target_player;

    var emotionlabel = new BABYLON.GUI.Ellipse("emotion_" + target_socketId);
    emotionlabel.background = "white"
    emotionlabel.height = "110px";
    emotionlabel.width = "110px";
    emotionlabel.alpha = 1;

    emotionText.text = data;
    emotionText.color = "black";
    emotionText.fontSize = 70;
    emotionText.lineSpacing = 0;

    emotionTexture.addControl(emotionlabel);
    emotionTexture.name = 'emotion_' + target_socketId;
    emotionTexture.renderScale = 0.4;
    emotionlabel.addControl(emotionText);

    scene.onBeforeRenderObservable.add(() => {
        emotionPlane.position.x = emotionPlane.parentNode.position.x;
        emotionPlane.position.z = emotionPlane.parentNode.position.z;
    });
    const frameRate = 10;
    const xSlide = new BABYLON.Animation("xSlide", "position.y", frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);

    const keyFrames = [];

    keyFrames.push({
        frame: 0,
        value: 1.5
    });
    keyFrames.push({
        frame: frameRate,
        value: 2.1
    });
    keyFrames.push({
        frame: 2 * frameRate,
        value: 2.1
    });
    keyFrames.push({
        frame: 3 * frameRate,
        value: 1.5
    });

    xSlide.setKeys(keyFrames);
    emotionPlane.animations.push(xSlide);

    const myAnimatable = scene.beginAnimation(emotionPlane, 0, 3 * frameRate, false);
    myAnimatable.onAnimationEnd = function () {
        scene.removeMesh(scene.getMeshByName(emotionPlane.name));
        scene.getTextureByName(emotionPlane.name).dispose();
    }
    if (i == null) {
        socket.emit("userEmotion", data, socket.id);
    }
    $("#emoticonBox").hide();
}

// 아바타-캠 스위치 액션
function userAvatarChange(index) {
    players[index].player.setEnabled(players[index].avatarState == "avatar");
    players[index].player_cam.setEnabled(players[index].avatarState != "avatar");
}

// 메인 스크린 스트리밍 이벤트(로컬 세팅)
function localVideoStream(type) {
    if ($('#stream_state').val() != 'stop') {
        alert('이미 사용중입니다.');
        return;
    }

    // console.log($('#stream_state').val());
    // 기존에 재생중이던 스크린 종료
    $('.button-webcam').show();
    scene.removeMesh(scene.getMeshByName("mainScreen"));
    scene.getMaterialByName("mainScreenMat").dispose();

    var mainScreenBtn = scene.getMaterialByName("mainScreenBtn");

    if ($('#stream_state').val() == 'url') {
        removeDomNode("iframeContainer")
        removeDomNode("iframe-video")
        removeDomNode("cssContainer")
        removeDomNode("CSS3DRendererDom")
        scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
    } else if ($('#stream_state').val() == 'webCam') {
        scene.getTextureByName("mainScreenVideo").dispose();
    } else if ($('#stream_state').val() == 'screen') {
        scene.getTextureByName("mainScreenVideo").dispose();
        localStream.getTracks().forEach(track => track.stop())
        localStream = screanStream;
        var toggle = document.getElementById('toggle-mic');
        $('.toggle-mic-on').css('display', 'none');
        $('.toggle-mic-off').css('display', 'block');
        toggle.setAttribute('data', 'off');
        localStream.getAudioTracks()[0].enabled = false;
        mic_switch = false;
        //localStream.getTracks().forEach(track => track.start())
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰',
            'dest': 'all'
        }
        sendMessage(msg);
    }
    // 스크린 초기화
    initMainScreen(type);

    // 스트리밍 타입 세팅하고 시작 
    $('#stream_state').val(type);

    if (type == "url") {
        // 영상 url 공유
        var url = document.getElementById('url').value;
        if (url == "") {
            alert("URL을 입력해주세요");
        } else {
            // url 스트리밍 시작
            plane = scene.getMeshByName("mainScreen");

            css3DRenderer = setupRenderer(plane, url, scene);
            scene.onBeforeRenderObservable.add(() => {
                css3DRenderer.render(scene, scene.activeCamera)
            })
            streamerText.text = player_info.name;

            $('#stream_state').val(type)

            // 공유 중지 버튼
            $('.button-stop').css('display', 'block');

            // 소켓 통신
            socket.emit("streamURL", {
                socketId: socket.id,
                streamer: player_info.name,
                url: url
            });
        }
    } else if (type == "webCam") {
        // 스트리밍 시작
        const video = document.getElementById("remoteVideo");
        video.srcObject = localStream;
        setTimeout(function () {
            mainScreenStream(video);
        }, 1000);
        streamerText.text = player_info.name;

        $('#stream_state').val(type)

        // 공유 중지 버튼
        $('.button-stop').css('display', 'block');

        // 소켓 통신
        socket.emit("streamWebcam", {
            socketId: socket.id,
            streamer: player_info.name
        });

    } else if (type == "screen") {
        // 웹캠 스트리밍 공유
        const video = document.getElementById("remoteVideo");
        video.srcObject = localStream;
        setTimeout(function () {
            mainScreenStream(video);
        }, 1000);
        $('.button-stop').css('display', 'block');
        $('#stream_state').val('screen')

        streamerText.text = player_info.name;

        // 소켓 통신
        socket.emit("streamScreen", {
            socketId: socket.id,
            streamer: player_info.name
        });

        // 설정창 닫기
        $('#shareScreen').hide();
        $('#dimLayer').hide();
        document.getElementById('setting').innerText = "";
    }
    player_info.streamer = player_info.socketId;
    // 설정창 닫기
    if ($('.btn-lang.on').data('lang') == 'kr') {
        mainScreenBtn.diffuseTexture = new BABYLON.Texture("../assets/img/ui/20220304/btn_share_stop.png", scene);
    } else {
        mainScreenBtn.diffuseTexture = new BABYLON.Texture("./assets/img/20220309/share_btn_circle_on.png", scene);
    }
    
    $('#shareScreen').hide();
    $('#dimLayer').hide();
    document.getElementById('setting').innerText = "";
}

// 멤버 삭제
function removeAvatar(rmv_player, nameTag) {
    // 아바타 삭제
    try {
        rmv_player.player.dispose();
        scene.getSkeletonByName("girl_" + rmv_player.socketId).dispose();

        // 캠 plane 삭제
        scene.removeMesh(scene.getMeshByName("webCam_" + rmv_player.socketId));
        scene.getMaterialByName("playerCamMat_" + rmv_player.socketId).dispose();
        scene.getTextureByName("playerCamVideo_" + rmv_player.socketId).dispose();

        scene.getAnimationGroupByName("danceRange_" + rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("idleRange_" + rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("runRange_" + rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("sitRange_" + rmv_player.socketId).dispose()
        scene.getAnimationGroupByName("walkRange_" + rmv_player.socketId).dispose()

        scene.getMaterialByName("Cylinder_" + rmv_player.socketId).dispose();
        scene.getTextureByName("Cylinder_" + rmv_player.socketId).dispose();

        // if (nameTag) {
        scene.getTextureByName("nameText_" + rmv_player.socketId).dispose();
        scene.getMaterialByName("namePlane_" + rmv_player.socketId).dispose();
        scene.removeMesh(scene.getMeshByName("namePlane_" + rmv_player.socketId));
        // }
    } catch {

    }
}

// ----------------- event set end-------------------
// -------------- 영상 공유 func start----------------

function mainScreenStream(video) {
    mainScreenVideoTexture = new BABYLON.VideoTexture('mainScreenVideo', video, scene, true, true);
    mainScreenVideoTexture.emissiveColor = new BABYLON.Color3.White()
    plane = scene.getMeshByName("mainScreen");

    mat = scene.getMaterialByName("mainScreenMat");
    mat.diffuseTexture = mainScreenVideoTexture;
    plane.material = mat;
    //mat.diffuseTexture.vScale =-1;  
}

function mainScreenStreamer(streamer) {

    // 발표자 이름 표시
    scene.removeMesh(scene.getMeshByName("mainScreenStreamer"));
    scene.getTextureByName("mainScreenStreamer").dispose();
    scene.getMaterialByName("mainScreenStreamer").dispose();

    streamerPlane = BABYLON.MeshBuilder.CreatePlane('mainScreenStreamer', {
        height: 2,
        width: 5
    }, scene);
    streamerText = new BABYLON.GUI.TextBlock('streamerText');
    streamerTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(streamerPlane);
    streamerPlane.material.name = 'mainScreenStreamer';
    streamerPlane.position = new BABYLON.Vector3(36.07, 5, -31.941);
    streamerPlane.rotation.y = streamerPlane.rotation.y + 2.9;

    var label = new BABYLON.GUI.Rectangle("mainScreenLabel");
    label.background = "white"
    label.height = "80px";
    label.width = "200px";
    label.alpha = 0.8;
    label.cornerRadius = 20;
    label.thickness = 1;

    streamerText.text = streamer;
    streamerText.color = "black";
    streamerText.fontSize = 30;

    streamerTexture.addControl(label);
    streamerTexture.name = "mainScreenStreamer";
    streamerTexture.renderScale = 0.4;
    label.addControl(streamerText);
}

function removeDomNode(id) {
    let node = document.getElementById(id);
    if (node && node.parentNode) {
        node.parentNode.removeChild(node);
    }
}
let videoWidth = 800
let videoHeight = 480
let tvThickness = .2

// 영상 공유 정지
function localStopStreaming() {
    scene.removeMesh(scene.getMeshByName("mainScreen"));
    scene.getMaterialByName("mainScreenMat").dispose();
    type = $('#stream_state').val();
    if (type == "screen") {
        scene.getTextureByName("mainScreenVideo").dispose();

        localStream.getTracks().forEach(track => track.stop())
        localStream = screanStream;
        var toggle = document.getElementById('toggle-mic');
        $('.toggle-mic-on').css('display', 'none');
        $('.toggle-mic-off').css('display', 'block');
        toggle.setAttribute('data', 'off');
        localStream.getAudioTracks()[0].enabled = false;
        mic_switch = false;
        //localStream.getTracks().forEach(track => track.start())
        msg = {
            'socketId': socket.id,
            'streamId': localStream.id,
            'displayName': '콩콩쓰',
            'dest': 'all'
        }
        sendMessage(msg);
    } else if (type == "webCam") {
        // console.log(localStream.getAudioTracks());
        scene.getTextureByName("mainScreenVideo").dispose();
    } else if (type == "url") {
        // 기존 스트리밍 삭제
        removeDomNode("iframeContainer")
        removeDomNode("iframe-video")
        removeDomNode("cssContainer")
        removeDomNode("CSS3DRendererDom")
        scene.removeMesh(scene.getMeshByName("mainScreenUrl"));
    }

    player_info.streamer = null;
    socket.emit("stopStreaming", socket.id);

    streamerText.text = '';
    $('#shareScreen').css('display', 'none');
    $('#dimLayer').hide();
    document.getElementById('setting').innerText = "";
    $('.button-stop').css('display', 'none');
    $('#stream_state').val('stop');
    var mainScreenBtn = scene.getMaterialByName("mainScreenBtn");
    if ($('.btn-lang.on').data('lang') == 'kr') {
        mainScreenBtnMat.diffuseTexture = new BABYLON.Texture("../assets/img/ui/20220304/btn_share.png", scene);
    } else {
        mainScreenBtnMat.diffuseTexture = new BABYLON.Texture("./assets/img/20220309/share_btn_circle_off.png", scene);    
    }
    initMainScreen('');
    $('#stream_state').val('stop');
}

// 메인 스크린에 URL 영상 스트리밍
function setupRenderer(videoViewMesh, videoURL, scene) {

    let webGLContainer = document.getElementById('canvasZone')

    // 컨테이너 생성
    let css3DContainer = document.createElement('div')
    css3DContainer.id = 'cssContainer'
    css3DContainer.style.position = 'absolute'
    css3DContainer.style.width = '100%'
    css3DContainer.style.height = '100%'
    css3DContainer.style.zIndex = '-1'
    webGLContainer.insertBefore(css3DContainer, webGLContainer.firstChild)

    let css3DRenderer = new CSS3DRenderer()
    css3DContainer.appendChild(css3DRenderer.domElement)
    css3DRenderer.setSize(webGLContainer.offsetWidth, webGLContainer.offsetHeight)

    // 영상 실행할 iFrame 컨테이너 생성
    var iframeContainer = document.createElement('div')
    iframeContainer.style.width = videoWidth + 'px'
    iframeContainer.style.height = videoHeight + 'px'
    iframeContainer.style.backgroundColor = '#000'
    iframeContainer.id = "iframeContainer"

    // CSS Object 생성
    var CSSobject = new CSS3DObject(iframeContainer, scene)
    CSSobject.position.copyFrom(videoViewMesh.getAbsolutePosition())
    CSSobject.rotation.y = -videoViewMesh.rotation.y
    CSSobject.scaling.copyFrom(videoViewMesh.scaling)
    CSSobject.name = "mainScreenUrl";

    // iFrame 생성 
    var iframe = document.createElement('iframe')
    iframe.id = 'iframe-video'
    iframe.style.width = videoWidth + 'px'
    iframe.style.height = videoHeight + 'px'
    iframe.style.border = '0px'
    iframe.allow = 'autoplay'
    iframe.src = [videoURL.replace('watch?v=', 'embed/').replace('youtu.be/', 'www.youtube.com/embed/'), null, '?rel=0&enablejsapi=1&disablekb=1&autoplay=1&controls=0&fs=0&modestbranding=1'].join('')
    iframeContainer.appendChild(iframe)

    // material 생성
    let depthMask = new BABYLON.StandardMaterial('VideoViewMaterial', scene)
    depthMask.backFaceCulling = true
    videoViewMesh.material = depthMask

    // Render Video the mesh
    // videoViewMesh.onBeforeRenderObservable.add(() => engine.setColorWrite(false))
    // videoViewMesh.onAfterRenderObservable.add(() => engine.setColorWrite(true))

    // swap meshes to put mask first
    var videoPlaneIndex = scene.meshes.indexOf(videoViewMesh)
    scene.meshes[videoPlaneIndex] = scene.meshes[0]
    scene.meshes[0] = videoViewMesh

    return css3DRenderer
}

class CSS3DObject extends BABYLON.Mesh {
    constructor(element, scene) {
        super()
        this.element = element
        this.element.style.position = 'absolute'
        this.element.style.pointerEvents = 'auto'
    }
}

class CSS3DRenderer {
    constructor() {
        var matrix = new BABYLON.Matrix()

        this.cache = {
            camera: {
                fov: 0,
                style: ''
            },
            objects: new WeakMap()
        }

        var domElement = document.createElement('div')
        domElement.style.overflow = 'hidden'

        this.domElement = domElement
        this.cameraElement = document.createElement('div')
        this.isIE = (!!document['documentMode'] || /Edge/.test(navigator.userAgent) || /Edg/.test(navigator.userAgent))

        if (!this.isIE) {
            this.cameraElement.style.webkitTransformStyle = 'preserve-3d'
            this.cameraElement.style.transformStyle = 'preserve-3d'
        }
        this.cameraElement.style.pointerEvents = 'none'
        domElement.appendChild(this.cameraElement)
    }

    getSize() {
        return {
            width: this.width,
            height: this.height
        }
    }

    setSize(width, height) {
        this.width = width
        this.height = height
        this.widthHalf = this.width / 2
        this.heightHalf = this.height / 2

        this.domElement.style.width = width + 'px'
        this.domElement.style.height = height + 'px'

        this.cameraElement.style.width = width + 'px'
        this.cameraElement.style.height = height + 'px'
    }

    epsilon(value) {
        return Math.abs(value) < 1e-10 ? 0 : value
    }

    getCameraCSSMatrix(matrix) {
        var elements = matrix.m

        return 'matrix3d(' +
            this.epsilon(elements[0]) + ',' +
            this.epsilon(-elements[1]) + ',' +
            this.epsilon(elements[2]) + ',' +
            this.epsilon(elements[3]) + ',' +
            this.epsilon(elements[4]) + ',' +
            this.epsilon(-elements[5]) + ',' +
            this.epsilon(elements[6]) + ',' +
            this.epsilon(elements[7]) + ',' +
            this.epsilon(elements[8]) + ',' +
            this.epsilon(-elements[9]) + ',' +
            this.epsilon(elements[10]) + ',' +
            this.epsilon(elements[11]) + ',' +
            this.epsilon(elements[12]) + ',' +
            this.epsilon(-elements[13]) + ',' +
            this.epsilon(elements[14]) + ',' +
            this.epsilon(elements[15]) +
            ')'
    }

    getObjectCSSMatrix(matrix, cameraCSSMatrix) {
        var elements = matrix.m;
        var matrix3d = 'matrix3d(' +
            this.epsilon(elements[0]) + ',' +
            this.epsilon(elements[1]) + ',' +
            this.epsilon(elements[2]) + ',' +
            this.epsilon(elements[3]) + ',' +
            this.epsilon(-elements[4]) + ',' +
            this.epsilon(-elements[5]) + ',' +
            this.epsilon(-elements[6]) + ',' +
            this.epsilon(-elements[7]) + ',' +
            this.epsilon(elements[8]) + ',' +
            this.epsilon(elements[9]) + ',' +
            this.epsilon(elements[10]) + ',' +
            this.epsilon(elements[11]) + ',' +
            this.epsilon(elements[12]) + ',' +
            this.epsilon(elements[13]) + ',' +
            this.epsilon(elements[14]) + ',' +
            this.epsilon(elements[15]) +
            ')'

        if (this.isIE) {
            return 'translate(-50%,-50%)' +
                'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)' +
                cameraCSSMatrix +
                matrix3d;
        }
        return 'translate(-50%,-50%)' + matrix3d
    }

    renderObject(object, scene, camera, cameraCSSMatrix) {
        if (object instanceof CSS3DObject) {
            var style
            var objectMatrixWorld = object.getWorldMatrix().clone()
            var camMatrix = camera.getWorldMatrix()
            var innerMatrix = objectMatrixWorld.m

            // Set scaling
            const youtubeVideoWidth = 1.6
            const youtubeVideoHeight = 1.2

            innerMatrix[0] *= 0.01 / youtubeVideoWidth
            innerMatrix[2] *= 0.01 / youtubeVideoWidth
            innerMatrix[5] *= 0.01 / youtubeVideoHeight

            // Set position from camera
            innerMatrix[12] = -camMatrix.m[12] + object.position.x
            innerMatrix[13] = -camMatrix.m[13] + object.position.y
            innerMatrix[14] = camMatrix.m[14] - object.position.z
            innerMatrix[15] = camMatrix.m[15] * 0.00001

            objectMatrixWorld = BABYLON.Matrix.FromArray(innerMatrix)
            objectMatrixWorld = objectMatrixWorld.scale(100)
            style = this.getObjectCSSMatrix(objectMatrixWorld, cameraCSSMatrix)
            var element = object.element
            var cachedObject = this.cache.objects.get(object)

            if (cachedObject === undefined || cachedObject.style !== style) {

                element.style.webkitTransform = style
                element.style.transform = style

                var objectData = {
                    style: style
                }

                this.cache.objects.set(object, objectData)
            }
            if (element.parentNode !== this.cameraElement) {
                this.cameraElement.appendChild(element)
            }

        } else if (object instanceof BABYLON.Scene) {
            for (var i = 0, l = object.meshes.length; i < l; i++) {
                this.renderObject(object.meshes[i], scene, camera, cameraCSSMatrix)
            }
        }
    }

    render(scene, camera) {
        var projectionMatrix = camera.getProjectionMatrix()
        var fov = projectionMatrix.m[5] * this.heightHalf

        if (this.cache.camera.fov !== fov) {

            if (camera.mode == BABYLON.Camera.PERSPECTIVE_CAMERA) {
                this.domElement.style.webkitPerspective = fov + 'px'
                this.domElement.style.perspective = fov + 'px'
            } else {
                this.domElement.style.webkitPerspective = ''
                this.domElement.style.perspective = ''
            }
            this.cache.camera.fov = fov
        }

        if (camera.parent === null) camera.computeWorldMatrix()

        var matrixWorld = camera.getWorldMatrix().clone()
        var rotation = matrixWorld.clone().getRotationMatrix().transpose()
        var innerMatrix = matrixWorld.m

        innerMatrix[1] = rotation.m[1]
        innerMatrix[2] = -rotation.m[2]
        innerMatrix[4] = -rotation.m[4]
        innerMatrix[6] = -rotation.m[6]
        innerMatrix[8] = -rotation.m[8]
        innerMatrix[9] = -rotation.m[9]

        matrixWorld = BABYLON.Matrix.FromArray(innerMatrix)

        var cameraCSSMatrix = 'translateZ(' + fov + 'px)' + this.getCameraCSSMatrix(matrixWorld)

        var style = cameraCSSMatrix + 'translate(' + this.widthHalf + 'px,' + this.heightHalf + 'px)'

        if (this.cache.camera.style !== style && !this.isIE) {
            this.cameraElement.style.webkitTransform = style
            this.cameraElement.style.transform = style
            this.cache.camera.style = style
        }

        this.renderObject(scene, scene, camera, cameraCSSMatrix)
    }
}
// -------------- 영상 공유 func end----------------


// 0309 영문버전 추가
var loadingScreenDiv = window.document.getElementById("loadingScreen");
var btnEnter = document.getElementById("btnEnter");
// Loading still in progress.
// To wait for it to complete, add "DOMContentLoaded" or "load" listeners.
window.addEventListener("DOMContentLoaded", () => {
  // DOM ready! Images, frames, and other subresources are still downloading.
  document.getElementById("gauge").classList.add("inter");
});

window.addEventListener("load", () => {
  // Fully loaded!
  setTimeout(function () {
    document.getElementById("gauge").classList.add("done");
    document.getElementsByClassName("box-guage")[0].style.display = "none";
    document.getElementById("status").style.display = "none";
    document.getElementById("btnEnter").style.display = "block";
  }, 8000)
  
  var swiper = new Swiper(".mySwiper", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
});


btnEnter.addEventListener("click", () => {
  loadingScreenDiv.classList.add("disapear");
  openingAnimation();
  $("#fog").addClass("disapear");
})

